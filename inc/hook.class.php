<?php
/*
	[Bidcms.Com!] (C)2009-2011 Bidcms.Com.
	This is NOT a freeware, use is subject to license terms

	$Id: model.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
abstract class hook extends controller
{
	//支付前判断
	//$info订单信息
	public function order_pay_before($info,$note_id){
	}
	//支付成功后返回
	//$info订单信息,$result支付信息
	public function order_pay_success($info,$result,$note_id){
	}
}
