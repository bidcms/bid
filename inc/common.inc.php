<?php
/*
	[Bidcms.com!] (C)2009-2014 Bidcms.com.
	This is NOT a freeware, use is subject to license terms
	$author: limengqi
	$Id: common.inc.php 2010-08-24 10:42 $
*/

if (isset($_REQUEST['GLOBALS']) OR isset($_FILES['GLOBALS'])) {
	exit('Request tainting attempted.');
}
date_default_timezone_set("PRC");
define('ROOT_PATH',str_replace('\\','/',substr(dirname(__FILE__),0,-3)));
define('IN_BIDCMS',1);
define('IN_PHPUP',1);
if(!is_file(ROOT_PATH.'/inc/config.inc.php'))
{
	exit('The File "config.inc.php" does not exist,Please <a href="install/index.php">install</a> it before using Bidcms.');
}

require(ROOT_PATH.'/inc/config.inc.php');

header("Content-type:text/html;charset=".$charset);
//初始化数据连接
require(ROOT_PATH.'/inc/classes/pdo.class.php');
//加载公共函数
require(ROOT_PATH.'/inc/global.func.php');
require(ROOT_PATH.'/inc/template.func.php');
//初始化redis
if(class_exists('Redis') && isset($redis_config)){
	require(ROOT_PATH.'/inc/classes/redis.class.php');
	$cache=new cacheRedis($redis_config['host'],$redis_config['port'],$redis_config['auth']);
} else {
	require(ROOT_PATH.'/inc/classes/cache.class.php');
	$cache=new cache(ROOT_PATH.'/data/cache/');
}
//加载cookie类
require(ROOT_PATH."/inc/classes/cookie.class.php");
$cookie=new cookies($cookie_info);
//加载session类
require(ROOT_PATH."/inc/classes/session.class.php");
$session=new session($session_info);
//Controller
require(ROOT_PATH.'/inc/controller.class.php');
$base=new controller();

//Model
require(ROOT_PATH.'/inc/classes/validate.class.php');
require(ROOT_PATH.'/inc/model.class.php');
require(ROOT_PATH.'/inc/remote.class.php');

//Curl
require(ROOT_PATH.'/inc/classes/curl.class.php');
$curl=new curl();

require(ROOT_PATH.'/inc/global.inc.php');




define('SITE_LANG_ID','2');
define('SITE_LANG','zh-cn');
define('SITE_TITLE','');
define('SEO_TITLE','');
define('SITE_THEME','default');
define('VERSION','BidCms_3.0');
define('BIDCMS_CLIENT_SESSION','bidcms');
define('SITE_CURRENCY','CNY');
