<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: config.inc.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
//网站编码
$charset="utf-8";
//session
$session_info=array('prefix'=>'bidcms_');
//cookie
$cookie_info=array('prefix'=>'bidcms_','path'=>'/','domain'=>'demo.bidcms.com');
//网站申请的KEY
$bidcms_key='';

//网站申请的SECRET
$bidcms_secret='fJcpBvRFrg506jpcuJeixezgPNyALm';

//认证的网址
$bidcms_site='';

$manager = array(
	'hash' => 'shop'
);

$db_config=array(
	'store'=>array('host'=>'127.0.0.1','user'=>'root','passwd'=>'root','name'=>'bidcms','table_prefix'=>'bidcms_','charset'=>'utf8'),
);
