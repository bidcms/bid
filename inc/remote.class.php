<?php
/*
	[Bidcms.Com!] (C)2009-2011 Bidcms.Com.
	This is NOT a freeware, use is subject to license terms
	$Id: model.class.php 2010-08-24 10:42 $
*/
if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
class remote
{
	public $table='';
	public $prefix='https://yun.bidcms.com/index.php';
    public $ext = array();
    public function fields($fields){
      $this->ext['fields'] = $fields;
      return $this;
    }
    public function api($data=array()){
		$url=$this->prefix;
		$content=array();
		if(!empty($url)){
			$data=bidcms_encode($data);
            $content=$GLOBALS['curl']->request($url,'PUT',$data);
            $res = bidcms_decode($content);
            if(is_array($res)){
              $content = $res;
            }
		}
		return $content;
	}
	public function get_page($container=array(),$showpage=array(),$order=''){
		$url=$this->prefix.'?con=remote&act=get_page';
		$content=array();
		if(!empty($this->table)){
			$data=bidcms_encode(array('model'=>$this->table,'container'=>$container,'showpage'=>$showpage,'order'=>$order,'manager'=>$GLOBALS['manager']['hash'],'ext'=>$this->ext));
            $content=$GLOBALS['curl']->request($url,'PUT',$data);
            $res = bidcms_decode($content);
            if(is_array($res)){
              $content = $res;
            }
		}
		return $content;
	}
	public function get_one($container=array(),$order='',$showpage=array()){
		$url=$this->prefix.'?con=remote&act=get_one';
		$content=array();
		if(!empty($this->table)){
			$data=bidcms_encode(array('model'=>$this->table,'container'=>$container,'order'=>$order,'manager'=>$GLOBALS['manager']['hash'],'ext'=>$this->ext));
            $content=$GLOBALS['curl']->request($url,'PUT',$data);
			$res = bidcms_decode($content);
            if(is_array($res)){
              $content = $res;
            }
		}
		return $content;
	}
	public function get_sum($field,$container=array(),$showpage=array()){
		$url=$this->prefix.'?con=remote&act=get_sum';
		$content=array();
		if(!empty($this->table)){
			$data=bidcms_encode(array('model'=>$this->table,'field'=>$field,'showpage'=>$showpage,'container'=>$container,'order'=>$order,'manager'=>$GLOBALS['manager']['hash'],'ext'=>$this->ext));
			$content=$GLOBALS['curl']->request($url,'PUT',$data);
			$res = bidcms_decode($content);
            if(is_array($res)){
              $content = $res;
            }
		}
		return $content;
	}
	public function get_count($container=array(),$order='',$showpage=array()){
		$url=$this->prefix.'?con=remote&act=get_count';
		$content=array();
		if(!empty($this->table)){
			$data=bidcms_encode(array('model'=>$this->table,'showpage'=>$showpage,'container'=>$container,'order'=>$order,'manager'=>$GLOBALS['manager']['hash'],'ext'=>$this->ext));
			$content=$GLOBALS['curl']->request($url,'PUT',$data);
			$res = bidcms_decode($content);
            if(is_array($res)){
              $content = $res;
            }
		}
		return $content;
	}
	public function get_sql($sql){
		$url=$this->prefix.'?con=remote&act=get_sql';
		$content=array();
		if(!empty($this->table)){
			$data=bidcms_encode(array('model'=>$this->table,'sql'=>$sql,'manager'=>$GLOBALS['manager']['hash'],'ext'=>$this->ext));
			$content=$GLOBALS['curl']->request($url,'PUT',$data);
			$res = bidcms_decode($content);
            if(is_array($res)){
              $content = $res;
            }
		}
		return $content;
	}
	public function update_data($container,$data){
		$data=!empty($data)?$data:$this->dataset;
		$url=$this->prefix.'?con=remote&act=update_data';
		$rows=0;
		if(!empty($this->table)){
			$data=bidcms_encode(array('model'=>$this->table,'container'=>$container,'data'=>$data,'manager'=>$GLOBALS['manager']['hash'],'ext'=>$this->ext));
			$rows=$GLOBALS['curl']->request($url,'PUT',$data);
		}
		return $rows;
	}
	public function insert_data($data = array()){
		$data=!empty($data)?$data:$this->dataset;
		$url=$this->prefix.'?con=remote&act=insert_data';
		$rows=0;
		if(!empty($this->table)){
			$data=bidcms_encode(array('model'=>$this->table,'data'=>$data,'manager'=>$GLOBALS['manager']['hash'],'ext'=>$this->ext));
			$rows=$GLOBALS['curl']->request($url,'PUT',$data);
		}
		return $rows;
	}
	public function delete_data($container){
		$url=$this->prefix.'?con=remote&act=delete_data';
		$rows=0;
		if(!empty($this->table)){
			$data=bidcms_encode(array('model'=>$this->table,'container'=>$container,'manager'=>$GLOBALS['manager']['hash'],'ext'=>$this->ext));
			$rows=$GLOBALS['curl']->request($url,'PUT',$data);
		}
		return $rows;
	}
	public function get_info_primary_id($id){
		$info=array();
		if($id>0 && !empty($this->definition['primary'])){
			$info=$this->get_one(array('and '.$this->definition['primary'].'=:id',array(':id'=>$id)));
		}
		return $info;
	}
	public function get_info_join_id($id){
		$info=array();
		if($id>0 && !empty($this->definition['join'])){
			$res=$this->get_one(array('and '.$this->definition['join'].'=:id',array(':id'=>$id)));
			if(is_array($res)){
				$info = $res;
			}
		}
		return $info;
	}
	function get_list_join_id($id,$is_mutil=false){
		$list=array();
		if(!empty($this->definition['join']) && !empty($id)){
			$container=array(' and '.$this->definition['join'].'=:id',array(':id'=>$id));
			$list=$this->get_page($container);
		}
		return $list;
	}
}
