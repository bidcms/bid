<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0">
    <title><?php echo isset($message['message'])?$message['message']:'请点击确定';?></title>
    <link rel="stylesheet" href="https://cdn.bootcss.com/weui/1.1.2/style/weui.min.css"/>
</head>
<body>
<div class="container" id="container">
  <div class="weui-msg">
        <div class="weui-msg__icon-area"><i class="weui-icon-<?php echo isset($message['type'])?$message['type']:'info';?> weui-icon_msg"></i></div>
        <div class="weui-msg__text-area">
            <h2 class="weui-msg__title">操作提示</h2>
            <p class="weui-msg__desc"><?php echo isset($message['message'])?$message['message']:'请点击确定';?></p>
        </div>
        <div class="weui-msg__opr-area">
            <p class="weui-btn-area">
                <a href="javascript:cBack();" class="weui-btn weui-btn_primary">确定</a>
            </p>
        </div>
        <div class="weui-msg__extra-area">
            <div class="weui-footer">
                <p class="weui-footer__links">
                    <a href="javascript:void(0);" class="weui-footer__link">必得CMS</a>
                </p>
                <p class="weui-footer__text">Copyright © 2008-2016 bidcms.com</p>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://res.wx.qq.com/open/js/jweixin-1.3.2.js"></script>
<script type="text/javascript">
<!--
var time="<?php echo $time;?>";
var url='<?php echo $url;?>';
var userAgent = navigator.userAgent.toLowerCase();

function GetQueryString(e,url) {
	url=url!=''?url:window.location.search.substr(1);
	var n = new RegExp("(^|&)" + e + "=([^&]*)(&|$)"),
		t = url.match(n);
	return null != t ? unescape(t[2]) : null
}
function cBack(){
	if(url!=''){
		if(userAgent.indexOf('miniprogram')>0 || userAgent.indexOf('wechatdevtools')>0){
			var wxUrl=GetQueryString('wxapp',url);
			if(wxUrl){
				wx.miniProgram.redirectTo({
				  url:unescape(wxUrl)
				});
				return false;
			}
		}
		window.location.replace(url);
	} else {
		if(userAgent.indexOf('miniprogram')>0 || userAgent.indexOf('wechatdevtools')>0){
			wx.miniProgram.navigateBack({
			  delta:1
			})
		} else {
			window.history.go(-1);
		}
	}
	
}
if(time>0){
	setTimeout(function(){cBack();},time*1000);
}
//-->
</script>
</body>
</html>
