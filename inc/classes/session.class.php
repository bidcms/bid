<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: session.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
class session
{
	function __construct($config = array())
	{
		$this->prefix=isset($config['prefix'])?$config['prefix']:'bidcms_';
	}
	function set($key,$val = '')
	{
      	if(is_array($key)){
          foreach($key as $k=>$v)
          {
              $_SESSION[$this->prefix.$k]=$v;
          }
        } else{
          $_SESSION[$this->prefix.$key] = $val;
        }
	}
	function get($key)
	{
		return isset($_SESSION[$this->prefix.$key])?$_SESSION[$this->prefix.$key]:'';
	}
	function get_id()
	{
		return session_id();
	}
	function destroy($key='')
	{
		if(empty($key))
		{
			session_destroy();
		}
		else
		{   if(is_array($key)){
                foreach($key as $k=>$v)
                {
                    unset($_SESSION[$this->prefix.$k]);
                }
            } else {
                unset($_SESSION[$this->prefix.$key]);
        	}
		}
	}
}