<?php
class image {
	//图片类型
	var $type;
	//实际宽度
	var $width;
	//实际高度
	var $height;
	//改变后的宽度
	var $resize_width;
	//改变后的高度
	var $resize_height;
	//是否裁图
	var $cut;
	//源图象
	var $srcimg;
	//临时创建的图象
	var $im;
	//生成的文件名
	var $filename;
	function __construct($img, $wid, $hei, $c=false) {
		$this->resize_width = $wid>0?$wid:100;
		$this->resize_height = $hei>0?$hei:100;
		$this->cut = $c;
		//初始化图象
		$this->init_img($img);
		//生成图象
		$this->newimg ();
		ImageDestroy ( $this->im );
	}
	function newimg() {
		//改变后的图象的比例
		$resize_ratio = ($this->resize_width) / ($this->resize_height);
		//实际图象的比例
		$ratio = ($this->width) / ($this->height);
		if ($this->cut) //裁图
		{
			$newimg = imagecreatetruecolor ( $this->resize_width, $this->resize_height );
			if ($ratio >= $resize_ratio) //高度优先
			{
				imagecopyresampled ( $newimg, $this->im, 0, 0, 0, 0, $this->resize_width, $this->resize_height, (($this->height) * $resize_ratio), $this->height );
			}
			if ($ratio < $resize_ratio) //宽度优先
			{
				imagecopyresampled ( $newimg, $this->im, 0, 0, 0, 0, $this->resize_width, $this->resize_height, $this->width, (($this->width) / $resize_ratio) );
			}
		} else {
			if ($ratio >= $resize_ratio) {
				$newimg = imagecreatetruecolor ( $this->resize_width, ($this->resize_width) / $ratio );
				imagecopyresampled ( $newimg, $this->im, 0, 0, 0, 0, $this->resize_width, ($this->resize_width) / $ratio, $this->width, $this->height );
			}
			if ($ratio < $resize_ratio) {
				$newimg = imagecreatetruecolor ( ($this->resize_height) * $ratio, $this->resize_height );
				imagecopyresampled ( $newimg, $this->im, 0, 0, 0, 0, ($this->resize_height) * $ratio, $this->resize_height, $this->width, $this->height );
			}
		}
		$this->dst_img=$newimg;
	}
	//初始化图象
	function init_img($path) {
		$imgArr=getimagesize($path);
		$type=$imgArr['mime'];
		if ($type == "image/jpeg") {
			$this->im = imagecreatefromjpeg ( $path);
		}
		if ($type == "image/gif") {
			$this->im = imagecreatefromgif ( $path );
		}
		if ($type == "image/png") {
			$this->im = imagecreatefrompng ( $path );
		}
		$this->width = $imgArr[0];
		$this->height = $imgArr[1];
	}
	
	/*
	时间：2017/4/27
	功能：合成海报
	作者：李孟琦
	参数：
	返回值：
	*/
	public function merge($bgImg,$data=array()){
		if(isset($bgImg['path']) && !empty($bgImg['path'])){
			$this->init_img($bgImg['path']);
			$bg_source=$this->im;
			$bgW=$this->width;
			$bgH=$this->height;
		} else {
			$bg_source = imagecreatetruecolor ( $bgImg['w'], $bgImg['h'] );
			$color = imagecolorallocate($bg_source, intval($bgImg['color'][0]), intval($bgImg['color'][1]), intval($bgImg['color'][2]));
			imagefill( $bg_source, 0, 0, $color );
			$bgW=$bgImg['w'];
			$bgH=$bgImg['h'];
		}
		foreach($data as $k=>$v){
			if($v['type']=='text'){
				//在背景图片上生成文字
				$color = imagecolorallocate($bg_source, intval($v['color'][0]), intval($v['color'][1]), intval($v['color'][2]));
				if(isset($v['x'])){
					$x=$v['x']>1?$v['x']:floatval($v['x']*$bgW);
					$y=$v['y']>1?$v['y']:floatval($v['y']*$bgH);
				} else {
					$x=$y=0;
				}
				$font_file=!empty($v['font'])?$v['font']:"/statics/fonts/wryh.ttf";
				if(isset($v['len'])){
					$len=!empty($v['len'])?$v['len']:20;
					$strlen=mb_strlen($v['content'],'utf-8');
					$place=0;
					for($i=0;$i<$strlen;$i+=$len){
						$content=mb_substr($v['content'],$i,$len,'utf-8');
						imagefttext($bg_source, $v['size'], 0, $x, $y+($v['size']*1.5)+$place, $color,$font_file,$content);
						$place+=20;
					}
				} else {
					imagefttext($bg_source, $v['size'], 0, $x, $y+($v['size']*1.5), $color,$font_file,$v['content']);
				}
				//imagestring($bg_source,15,$x,$y,$v['content'],$color);
			} elseif($v['type']=='image'){
				if($v['w']>1 && $v['h']>1){
					$this->resizeimage($v['content'],$v['w'],$v['h'],true);
					$img_source=$this->dst_img;
					$w=$v['w'];
					$h=$v['h'];
				} else {
					$this->init_img($v['content']);
					$img_source=$this->im;
					$w=$this->width;
					$h=$this->height;
				}
				if(isset($v['x'])){
					$x=$v['x']>1?$v['x']:floatval($v['x']*$bgW-($w*$v['x']));
					$y=$v['y']>1?$v['y']:floatval($v['y']*$bgH-($h*$v['y']));
				} else {
					$x=floatval(($bgW-$w)/2);
					$y=floatval(($bgH-$h)/2);
				}
				imagecopy($bg_source,$img_source,$x,$y,0,0,$w,$h);
			}
			
		}
		if(!empty($this->filename)){
			imagejpeg($bg_source,$this->filename);
		} else {
			imagejpeg($bg_source);
		}
		imagedestroy($bg_source);	
	}
}

?>