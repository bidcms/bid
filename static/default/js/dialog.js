//结束
function Complete_Dialog()
{
	$('#dialog').html($('#dialog_complete').html());
	$('#blurdiv').show();
	$('#blurdiv').height($(document).height());
	
	$('#dialog').dialog({
		title:'竞拍结束',
		width: 400,
		buttons: {},
		close:function(){$('#blurdiv').hide();$('#dialog').html('');$('#oids').val($('#ids').val());}
	});
}
//登录
function Login_Dialog()
{ 
	$.post(site_root+"/index.php?act=dialog&type=login",{},
	function(data){
		$('#dialog').html(data);
	});
	
	$('#blurdiv').show();
	$('#blurdiv').height($(document).height());
	
	$('#dialog').dialog({
		title:'会员登录',
		width: 400,
		height:300,
		buttons: {},
		close:function(){$('#blurdiv').hide();$('#dialog').html('');}
	});
}
//聊天扣钱
function Warn_Dialog(obj)
{
	if($.cookie('phpup_setwarn')==1)
	{
		return true;
	}
	else
	{
		$.cookie('phpup_setwarn',1,{expires: 7});
		if($(obj).attr('data')!=1)
		{
			$('#dialog').html($('#warnchat').html());
			$('#blurdiv').show();
			$('#blurdiv').height($(document).height());
			
			$('#dialog').dialog({
				title:'温馨提示',
				width: 400,
				height:250,
				buttons: {},
				close:function(){$('#blurdiv').hide();}
			});
		}
	}
}
//自动出价
function AutoBuy_Dialog(gid)
{
	$.post(site_root+"/index.php?act=autobuy&goodsid="+gid,{},
	function(data){
		$('#dialog').html(data);
	});
	
	$('#blurdiv').show();
	$('#blurdiv').height($(document).height());
	
	$('#dialog').dialog({
		title:'自动出价',
		width: 400,
		height:350,
		buttons: {},
		close:function(){$('#blurdiv').hide();$('#dialog').html('');}
	});
}
//关闭自动出价
function AutoBuy_Close(gid)
{
	$.post(site_root+"/index.php?act=closeauto&goodsid="+gid,{},
	function(data){
		eval('var dataobj='+data);
		if(dataobj.datastatus=='success')
		{
			alert('关闭成功');
			$('#to_cancel').attr('src',$('#to_cancel').attr('src').replace('over','down'));
			$('#to_start').attr('src',$('#to_start').attr('src').replace('down','over'));
		}
		else if(dataobj.datastatus=='nologin')
		{
			Login_Dialog();
		}
		else if(dataobj.datastatus=='nogoods')
		{
			alert('产品参数为空');
		}
		else
		{
			alert('关闭失败，请联系管理员');
		}
	});
}
// 充值
function Charge_Dialog()
{
	$.post(site_root+"/index.php?act=dialog&type=charge",{},
	function(data){
		$('#dialog').html(data);
	});
	$('#blurdiv').show();
	$('#blurdiv').height($(document).height());
	
	$('#dialog').dialog({
		title:'充值',
		width: 600,
		height:400,
		buttons: {},
		close:function(){$('#blurdiv').hide();$('#dialog').html('');}
	});
}
//充值等待
function btnChargeConfirm_click()
{
	$.post(site_root+"/index.php?act=dialog&type=chargewait",{},
	function(data){
		$('#dialog').html(data);
	});
	$('#blurdiv').show();
	$('#blurdiv').height($(document).height());
	
	$('#dialog').dialog({
		title:'充值',
		width: 600,
		buttons: {},
		close:function(){$('#blurdiv').hide();$('#dialog').html('');}
	});
}
//下订单
function Order_Dialog(gid)
{
	$.post(site_root+"/index.php?con=order&act=index&goodsid="+gid,{rand:Math.random()},
	function(data){
		$('#dialog').html(data);
	});
	$('#blurdiv').show();
	$('#blurdiv').height($(document).height());
	
	$('#dialog').dialog({
		title:'下订单',
		width: 500,
		height:300,
		buttons: {},
		close:function(){$('#blurdiv').hide();$('#dialog').html('');}
	});
}
//积分充值
function Scorecharge_Dialog(gid)
{
	$.post(site_root+"/index.php?con=order&act=scorecharge&id="+gid,{},
	function(data){
		$('#dialog').html(data);
	});
	$('#blurdiv').show();
	$('#blurdiv').height($(document).height());
	
	$('#dialog').dialog({
		title:'充值',
		width: 500,
		buttons: {},
		close:function(){$('#blurdiv').hide();$('#dialog').html('');}
	});
}
//积分兑换订单
function Score_Dialog(gid)
{
	$.post(site_root+"/index.php?con=order&act=scorebuy&id="+gid,{},
	function(data){
		$('#dialog').html(data);
	});
	$('#blurdiv').show();
	$('#blurdiv').height($(document).height());
	
	$('#dialog').dialog({
		title:'下订单',
		width: 500,
		buttons: {},
		close:function(){$('#blurdiv').hide();$('#dialog').html('');}
	});
}
//订单付款
function OrderPay_Dialog(oid)
{
	$.post(site_root+"/index.php?con=order&act=payment&orderid="+oid,{},
	function(data){
		$('#dialog').html(data);
	});
	$('#blurdiv').show();
	$('#blurdiv').height($(document).height());
	
	$('#dialog').dialog({
		title:'订单付款',
		width: 600,
		buttons: {},
		close:function(){$('#blurdiv').hide();$('#dialog').html('');}
	});
}
//订单付款等待
function btnOrderConfirm_click()
{
	$.post(site_root+"/index.php?act=dialog&type=chargewait",{},
	function(data){
		$('#dialog').html(data);
	});
	$('#blurdiv').show();
	$('#blurdiv').height($(document).height());
	
	$('#dialog').dialog({
		title:'订单付款',
		width: 600,
		height: 500,
		buttons: {},
		close:function(){$('#blurdiv').hide();$('#dialog').html('');}
	});
}
//查看订单
function OrderView_Dialog(oid)
{
	$.post(site_root+"/index.php?con=order&act=view&orderid="+oid,{},
	function(data){
		$('#dialog').html(data);
	});
	$('#blurdiv').show();
	$('#blurdiv').height($(document).height());
	
	$('#dialog').dialog({
		title:'查看订单',
		width: 600,
		height:400,
		buttons: {},
		close:function(){$('#blurdiv').hide();$('#dialog').html('');}
	});
}
//超时提示
function time_wran(title,content)
{
	$('#chargeForm').submit();
	$('#dialog').html(content);
	$('#blurdiv').show();
	$('#blurdiv').height($(document).height());
	
	$('#dialog').dialog({
		title:title,
		width: 400,
		height:200,
		buttons: {},
		close:function(){$('#blurdiv').hide();continue_alert();}
	});
}

//修改手机
function ChangeMobile_Dialog(mobile)
{
	$('#dialog').html('<table><tr><td><INPUT TYPE="text" id="changenew" NAME="newmobile" value="'+mobile+'" class="login_input"></td><td><input type="button" id="btnEditMobile" value="" class="btn_validate1" onclick="ChangeMobileConfirm();"/></td></tr></table>');
	$('#blurdiv').show();
	$('#blurdiv').height($(document).height());
	
	$('#dialog').dialog({
		title:'修改手机',
		width: 350,
		height:100,
		buttons: {},
		close:function(){$('#blurdiv').hide();$('#dialog').html('');}
	});
}
//修改手机操作
function ChangeMobileConfirm()
{
	$.post(site_root+"/tools/changemobile.php",{'newmobile':$('#changenew').val()},
	function(dataobj){
		eval('var data='+dataobj);
		if(data.datastatus=='success')
		{
			$('#newmobile').html($('#changenew').val());
			Dialog_close();
		}
		else if(data.datastatus=='nologin')
		{
			alert('用户未登录');
		}
		else if(data.datastatus=='errormobile')
		{
			alert('手机格式错误');
		}
		else if(data.datastatus=='likemobile')
		{
			alert('已存在此手机号');
		}
	});
}
function time_continue()
{
	Dialog_close();
	continue_alert();
}
function Dialog_close()
{
	$('#dialog').html('');
	$('#blurdiv').hide();
	$('#dialog').dialog('close');
}
function parseLogin(obj)
{
	if(obj.datastatus=='success')
	{
		window.open(window.location,'_self','');
	}
	else if(obj.datastatus=='usererror')
	{
		alert('用户名或密码错误');
	}
	else if(obj.datastatus=='codenull')
	{
		alert('验证码不能为空');
	}
	else if(obj.datastatus=='codeerror')
	{
		alert('验证码错误');
	}
}
//我要投币
function SetGuess_Dialog(gid,dotype)
{
	$.post(site_root+"/index.php?act=setguess",{'gid':gid,'dotype':dotype},
	function(data){
		$('#dialog').html(data);
	});
	$('#blurdiv').show();
	$('#blurdiv').height($(document).height());
	
	$('#dialog').dialog({
		title:'我要投币',
		width: 400,
		height:300,
		buttons: {},
		close:function(){$('#blurdiv').hide();}
	});
}
	//投币详情
function GetGuess_Dialog(gid)
{
	$.post(site_root+"/index.php?act=getguess",{'gid':gid},
	function(data){
		$('#dialog').html(data);
	});
	$('#blurdiv').show();
	$('#blurdiv').height($(document).height());
	
	$('#dialog').dialog({
		title:'投币详情',
		width: 600,
		height:600,
		buttons: {},
		close:function(){$('#blurdiv').hide();}
	});
}
//验证码
function getCode(){
    $.ajax({
      url:"/tools/showimgcode.php?r"+Math.random(),
      data:{},
      dataType:'json',
      success:function(res){
        if(res.errcode=='0'){
          $("#txt_code").attr('src',res.data.base64);
        }
      }
    });
}