<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: index.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
class front_controller extends controller
{
	public function init(){
      global $cache;
      $GLOBALS['setting'] = $this->bidcms_model('system_setting')->get_cache_list();
      $GLOBALS['cate'] = $this->bidcms_model('goods_cate')->get_cache_list();
      $GLOBALS['successinfo'] = $this->bidcms_model('goods_complete')->get_cache_success();
      $GLOBALS['show'] = $this->bidcms_model('user_show')->get_cache_list();
      $GLOBALS['link'] = $this->bidcms_model('system_link')->get_cache_list();
      $GLOBALS['userinfo'] = array('uid'=>0,'countbuy'=>0,'pms'=>0,'verification'=>0,'money'=>0,'success'=>0);
      if($GLOBALS['session']->get('uid') > 0){
        $uid = $GLOBALS['session']->get('uid');
        $key = 'user'.($uid%10).':'.$uid;
        $userinfo = $cache->get($key);
        if(empty($userinfo)){
          $userinfo = $this->bidcms_model('user_base')->fields('uid,username,mobile,email,verification,money,score,lastlogin,lastip,usertype,ip,address')->where('uid',$uid)->get_one();
          if(!empty($userinfo)){
            $success = $this->bidcms_model('goods_complete')->where('currentuid', $uid)->get_count('goods_id');
            $nopay = $this->bidcms_model('order_base')->where(['uid' => $uid, 'order_status' => 0])->get_count('id');
            $scorebuy = $this->bidcms_model('order_base')->where(['uid' => $uid, 'order_status' => 1])->get_count('id');
            $countbuy = $this->bidcms_model('goods_bidlog')->where(['uid' => $uid])->get_sum('count');
            $pms = $this->bidcms_model('user_pm')->where(['to_uid' => $uid])->get_count('id');
            $userinfo['scorebuy'] = intval($scorebuy);
            $userinfo['nopay'] = intval($nopay);
            $userinfo['success'] = intval($success);
            $userinfo['countbuy'] = intval($countbuy);
            $userinfo['pms'] = intval($pms);
            $cache->set($key,$userinfo,86400);
          }
        }
        $GLOBALS['userinfo'] = $userinfo;
      }
    }
    public function post($val=null,$default=''){
		return $this->bidcms_request($val,$default,'post');
	}
	public function input($val=null,$default=''){
		return $this->bidcms_request($val,$default,'input');
	}
	public function get($val=null,$default=''){
		return $this->bidcms_request($val,$default,'get');
	}
     //编辑器
	public function edit($content='',$textareaid='content',$textareaname='content',$textwidth='621px',$textheight='457px',$autosave=0,$showtextarea='none')
	{
		$str='<textarea name="'.$textareaname.'" id="'.$textareaid.'" style="display:'.$showtextarea.';">'.$content.'</textarea><iframe src="'.SITE_ROOT.'/editor/editor.htm?id='.$textareaid.'&ReadCookie='.$autosave.'" frameBorder="0" marginHeight="0" marginWidth="0" scrolling="No" width="'.$textwidth.'" height="'.$textheight.'"></iframe>';
		return $str;
	}
    //检查按钮状态
    public function checkbutton($goods)
    {
        if($GLOBALS['userinfo']['uid'] == 0)
        {
            return 'nologin';
        }
        if($GLOBALS['userinfo']['verification']==0)
        {
            return 'needconfirm';
        }
        $money=$goods['diffmoney']>0?floatval($goods['diffmoney']):floatval($GLOBALS['setting']['site_diffmoney']>0?$GLOBALS['setting']['site_diffmoney']:1);
        
        if($GLOBALS['userinfo']['money'] < $money)
        {
            return 'moneyerror';
        }
      	if($goods['needsign'] && $GLOBALS['userinfo']['uid']>0){
          //判断是否要报名
          $sign = $this->bidcms_model('sign')->fields('uid,gid')->where(['uid'=>$GLOBALS['userinfo']['uid'],'gid'=>$goods['goods_id']])->get_one();
          if($goods['needsign'] && !$sign)
          {
              return 'needsign';
          }
        }
        return 'ok';
    }
    function check_sms($mobile,$code){
       if(!empty($code)){
        $info = $this->bidcms_model('sms')->where(['mobile'=>$mobile,'code'=>$code])->get_one();
        if($info && $info['updatetime'] > time()-300){
          return true;
        }
       }
        return false;
    }
    //发送验证码
    function sms($mobile,$content,$code = ''){
      if(!empty($mobile)){
        if(!empty($code)){
        	$this->bidcms_model('sms')->insert_data(array('mobile'=>$mobile,'code'=>$code,'updatetime'=>time()));
        }
        $user = $GLOBALS['setting']['sms_user'];
        $api='http://gateway.woxp.cn:6630/utf8/web_api/';
        $password=md5($GLOBALS['setting']['sms_password']);
        $sms_kind=10783;
        $url=$GLOBALS['setting']['sms_api'].'?x_eid='.$sms_kind.'&x_uid='.$user.'&x_pwd_md5='.$password.'&x_ac=10&x_gate_id=300&x_target_no='.$mobile.'&x_memo='.str_replace(' ','',$content);
        return $GLOBALS['curl']->get($url);
      }
      return false;
    }
    //检查验证码
	function _checkcode($code)
	{
		include_once(ROOT_PATH."inc/classes/captcha.class.php");
        $captcha = new captcha();
        return $captcha->verify($code);
	}
}
