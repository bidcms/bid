<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: index.class.php 2010-08-24 10:42 $
*/
if (!defined('IN_BIDCMS')) {
    exit('Access Denied');
}
class index_controller extends front_controller {
    function index_action() {
        global $cache,$session;
        //邀请人
        $friend_uid = $this->get('uid',0);
        if($friend_uid > 0){
          $session->set('friendid',$friend_uid);
        }
        $status = $cache->get('status');
        //缓存会员动态
        if (empty($status)) {
            $status = $this->bidcms_model('user_status')->fields('uid,username,title')->get_page();
            if (!empty($status)) {
                $cache->set('status', $status, 3600);
            }
        }
        //正在进行
        $goods = $this->bidcms_model('goods_base')->fields(array('goods_name', 'goods_id', 'thumb', 'marketprice', 'nowprice', 'starttime', 'ishot', 'diffmoney', 'needsign', 'currentuser', 'currentuid', 'lasttime'))->where(array('ispassed' => 1,'lasttime'=>array('egt',time())))->get_page(array(), 'order by lasttime asc');
        $j = 0;
        $hot_list = $normal_list = $goods_nostart = $goods_list = array();
        $ids = array();
        foreach ($goods as $val) {
            $val['button_status'] = $this->checkbutton($val);
            if ($val['starttime'] > time()) {
                $goods_nostart[] = $val;
            } elseif ($val['starttime'] <= time()) {
                $ids[] = $val['goods_id'];
                $goods_list[] = array('id'=>$val['goods_id'],'nowprice'=>$val['nowprice'],'currentuser'=>$val['currentuser'],'lasttime'=>$val['lasttime']-time(),'currentuid'=>$val['currentuid']);
                if ($val['ishot']) {
                    $j++;
                    if ($j < 7) {
                        $hot_list[] = $val;
                    }
                } else {
                    $normal_list[] = $val;
                }
            }
        }
        //网站公告
        $article = $this->bidcms_model('article_base')->fields(array('title', 'id', 'cateid', 'updatetime'))->wherein(array('cateid' => '1,4'))->get_page(array('isshow' => 0, 'current_page' => 0, 'page_size' => 10, 'index' => 'cateid', 'list' => true), 'order by id desc');
        //晒单
        $show = $this->bidcms_model('user_show')->get_page(10);
        //已结束
        $goods_success = $this->bidcms_model('goods_complete')->fields(array('goods_name', 'goods_id', 'thumb', 'currentuser', 'currentuid', 'marketprice', 'nowprice', 'goods_status', 'content'))->where(array('goods_status' => array('lt', 3)))->get_page(5, 'order by lasttime desc');
        $pagetitle = $GLOBALS['setting']['site_title'];
        require $this->bidcms_template('index');
    }
    function itemlist_action() {
        global $cache;
        $cateid = intval($this->get('cateid'));
        if ($cateid > 0) {
            $goods_list = array();
            //正在进行
            $goods = $this->bidcms_model('goods_base')->where(array('ispassed' => 1, 'isfree' => array('lt', 1), 'cateid' => $cateid))->fields(array('goods_name', 'goods_id', 'thumb', 'marketprice', 'nowprice', 'starttime', 'ishot', 'diffmoney', 'needsign', 'lasttime','currentuser', 'currentuid'))->get_page(array(), 'order by lasttime asc');
            foreach ($goods as $k=>$val) {
                $val['button_status'] = $this->checkbutton($val);
                $goods[$k]['button_status'] = $this->checkbutton($val);
                $ids[] = $val['goods_id'];
                $goods_list[] = array('id'=>$val['goods_id'],'nowprice'=>$val['nowprice'],'currentuser'=>$val['currentuser'],'lasttime'=>$val['lasttime']-time(),'currentuid'=>$val['currentuid']);
            }
            //已结束
            $showpage = array( 'current_page' => 0, 'page_size' => 10);
            $complete = $this->bidcms_model('goods_complete')->fields(array('goods_name', 'goods_id', 'thumb', 'currentuser', 'currentuid', 'marketprice', 'nowprice', 'goods_status', 'content'))->where(array('goods_status' => array('lt', 3), 'cateid' => $cateid))->get_page($showpage, 'order by goods_id desc');
            //本区获胜者
            if (empty($cache->get('catewin_' . $cateid))) {
                $win = $this->bidcms_model('goods_complete')->fields('currentuid,goods_name,goods_id,nowprice,currentuser')->where('cateid', $cateid)->get_page(10, 'order by goods_id desc');
                $cache->set('catewin_' . $cateid, $win, 86400);
            } else {
                $win = $cache->get('catewin_' . $cateid);
            }
            $pagetitle = $GLOBALS['cate'][$cateid]['catename'];
            include $this->bidcms_template('itemlist');
        } else {
            sheader(url('index', 'index'), 3, '分类为空,查看更多商品...');
        }
    }
    function free_action() {
        global $cache;
        $container = array('ispassed' => 1, 'isfree' => 1);
        //正在进行
        $goods = $this->bidcms_model('goods_base')->where($container)->fields(array('goods_name', 'goods_id', 'thumb', 'marketprice', 'nowprice', 'starttime', 'ishot', 'diffmoney', 'needsign','currentuid','currentuser','lasttime'))->get_page(12, 'order by lasttime asc');
        foreach ($goods as $k=>$val) {
            $ids[]= $val['goods_id'];
            $goods[$k]['button_status'] = $this->checkbutton($val);
            $goods_list[] = array('id'=>$val['goods_id'],'nowprice'=>$val['nowprice'],'currentuser'=>$val['currentuser'],'lasttime'=>$val['lasttime']-time(),'currentuid'=>$val['currentuid']);
        }
        //本区获胜者
        if (empty($cache->get('catewin_free'))) {
            $win = $this->bidcms_model('goods_complete')->where('isfree', 1)->get_page(10, 'order by goods_id desc');
            $cache->set('catewin_free', $win, 86400);
        } else {
            $win = $cache->get('catewin_free');
        }
        $pagetitle = '新手体验';
        include $this->bidcms_template('free');
    }
    //晒单
    function show_action() {
        global $get;
        $show_mod = $this->bidcms_model('user_show');
        $showpage = array('isshow' => 1, 'current_page' => intval(isset($get['page']) ? $get['page'] : 0), 'page_size' => 20, 'url' => SITE_ROOT . '/index.php?con=index&act=show');
        $showlist = $show_mod->where('ispassed', 1)->get_page($showpage, 'order by id desc');
        $count = 0;
        if ($showlist) {
            $count = $show_mod->get_count();
            foreach ($showlist as $k => $v) {
                $ids[] = $v['uid'];
                $showlist[$k]['order_info'] = unserialize($v['order_info']);
                if ($showlist[$k]['order_info']['order_type'] == 1) //积分
                {
                    $showlist[$k]['url'] = url('index', 'gdetails', array('id' => $showlist[$k]['order_info']['goods_id']));
                } elseif ($showlist[$k]['order_info']['order_type'] != 4) //商品
                {
                    $showlist[$k]['url'] = url('index', 'complete', array('id' => $showlist[$k]['order_info']['goods_id']));
                }
            }
        }
        $pagetitle = '我拍我秀,秀奖品奖金币';
        $pageinfo = $this->bidcms_parse_page($count, $showpage);
        include $this->bidcms_template('show');
    }
    //晒单评论
    function comment_action() {
        $sid = intval($this->post('sid'));
        $comment_mod = $this->bidcms_model('comment');
        $checkcode = $this->_checkcode($this->post('checkcode'));
        if ($sid) {
            if (!$checkcode) {
                $this->bidcms_error('验证码不正确');
            }
            $_SESSION['_IMGCODE'] = '';
            $data['uid'] = $GLOBALS['userinfo']['uid'];
            $data['username'] = $GLOBALS['session']->get('username');
            if ($data['uid']) {
                $data['title'] = trim(strip_tags($this->post('title')));
                $data['content'] = str_replace("\r", "", str_replace("\n", "", trim(strip_tags($_POST['content']))));
                $data['updatetime'] = time();
                if (empty($data['title']) || empty($data['content'])) {
                    exit('<SCRIPT LANGUAGE="JavaScript">var dataobj={"datastatus":"nonull"};parent.addComment(dataobj);</SCRIPT>');
                }
                $data['sid'] = $sid;
                $data['ispassed'] = !$GLOBALS['setting']['comment_passed'];
                $insertid = $comment_mod->insert_data($data);
                if ($insertid) {
                    $this->bidcms_model('user_show')->where('id', $sid)->update_add_data('comments');
                    if ($data['ispassed']) {
                       $this->bidcms_success(["datastatus"=>'success',"id"=>$insertid,"uid"=> $data['uid'],"title"=>global_addslashes($data['title']),"content"=>global_addslashes($data['content']),"username"=> $data['username'],"updatetime"=>date('Y-m-d H:i:s', time())]);
                    } else {
                        $this->bidcms_success(["datastatus"=>'needpass'],'提交成功,评论正在审核中....');
                    }
                }
                $this->bidcms_error('发表失败');
            }
        }
        $this->bidcms_error('发表失败');
    }
    //晒单详情
    function showinfo_action() {
        $id = intval($this->get('id'));
        if ($id) {
            $show_mod = $this->bidcms_model('user_show');
            $showinfo = $show_mod->where('id', $id)->get_one();
            $show_mod->where('id', $id)->update_data(array('hits' => $showinfo['hits'] + 1));
            if ($showinfo) {
                //已结束
                $goods_success = $this->bidcms_model('goods_complete')->fields(array('goods_name', 'goods_id', 'thumb', 'currentuser', 'currentuid', 'marketprice', 'nowprice', 'goods_status', 'content'))->where(array('goods_status' => array('lt', 3)))->get_page(5, 'order by lasttime desc');
                $comment_mod = $this->bidcms_model('comment');
                $showpage = array('isshow' => 1, 'current_page' => intval($this->post('page')), 'page_size' => 10, 'url' => SITE_ROOT . '/index.php?con=index&act=showinfo&id' . $id);
                if ($GLOBALS['setting']['comment_passed']) {
                    $comment = $comment_mod->where(array('sid' => $id, 'ispassed' => 1))->get_page($showpage);
                } else {
                    $comment = $comment_mod->where('sid', $id)->get_page($showpage);
                }
                $showinfo['order_info'] = unserialize($showinfo['order_info']);
                if ($showinfo['order_info']['order_type'] == 1) //积分
                {
                    $showinfo['url'] = url('index', 'gdetails', array('id' => $showinfo['order_info']['goods_id']));
                } elseif ($showinfo['order_info']['order_type'] != 4) //商品
                {
                    $showinfo['url'] = url('index', 'complete', array('id' => $showinfo['order_info']['goods_id']));
                }
                $ordertype = $showinfo['order_info']['order_type'];
                $nowprice = $showinfo['order_info']['goods_info']['nowprice'];
                $marketprice = $showinfo['order_info']['goods_info']['marketprice'];
                $gid = $showinfo['order_info']['goods_id'];
                $currentuser = $showinfo['order_info']['goods_info']['currentuser'];
                $thumb = thumb($showinfo['order_info']['goods_info']['thumb']);
                $bidlog_mod = $this->bidcms_model('goods_bidlog');
                $money = $bidlog_mod->fields(array('money'))->where(array('uid' => $showinfo['uid'], 'gid' => $showinfo['order_info']['goods_id']))->get_one();
                $pagetitle = $showinfo['title'];
                $showlist = $this->_likegoods($showinfo['goods_name']);
                $pageinfo = $this->bidcms_parse_page(0,$showpage);
                include $this->bidcms_template('showinfo');
            } else {
                sheader(url('index', 'show'), 3, '不存在这个东东。');
            }
        } else {
            sheader(url('index', 'show'), 3, '参数为空');
        }
    }
    function history_action() {
        $mod = $this->bidcms_model('goods_complete');
        $container = array('goods_status' => array('lt', 3));
        $showpage = array('isshow' => 1, 'current_page' => intval($this->post('page')), 'page_size' => 20, 'url' => SITE_ROOT . '/index.php?con=index&act=history');
        $goods = $mod->where($container)->fields(array('goods_name', 'goods_id', 'thumb', 'marketprice', 'nowprice', 'starttime', 'ishot', 'goods_status', 'content', 'currentuser', 'currentuid', 'lasttime'))->get_page($showpage, 'order by lasttime desc');
        $pagetitle = '竞拍成功列表';
        $count = count($goods)<$showpage['page_size']?0:$mod->where($container)->get_count();
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
        include $this->bidcms_template('history');
    }
    function setguess_action() {
        $uid = $GLOBALS['userinfo']['uid'];
        if ($uid) {
            $gid = intval($this->post('gid'));
            if ($gid) {
                $type = trim($this->post('dotype'));
                $dotype = in_array($type, array('single', 'double')) ? $type : 'single';
                $guess_mod = $this->bidcms_model('guess_base');
                $guessinfo = $guess_mod->where(array('gid' => $gid, 'uid' => $uid))->get_one();
                $data = array();
                if (!$guessinfo) {
                    $data['uid'] = $uid;
                    $data['gid'] = $gid;
                    $data['username'] = $GLOBALS['userinfo']['username'];
                    $data['single_count'] = 0;
                    $data['double_count'] = 0;
                    $data['updatetime'] = time();
                    if ($guess_mod->insert_data($data)) {
                        $guessinfo = $data;
                    }
                }
                
                include $this->bidcms_template('dialog_guess');
            }
        } else {
            include $this->bidcms_template('dialog_login');
        }
    }
    function getguess_action() {
        if ($GLOBALS['userinfo']['uid']) {
            $gid = intval($this->post('gid'));
            if ($gid) {
                $guess_mod = $this->bidcms_model('guess_base');
                $guessinfo = $guess_mod->where('gid', $gid)->get_page();
                $guesslist = array('single' => array(), 'double' => array());
                $doublecount = $singlecount = 0;
                foreach ($guessinfo as $k => $v) {
                    if ($v['single_count'] > 0) {
                        $singlecount+= $v['single_count'];
                        $guesslist['single'][] = $v;
                    }
                    if ($v['double_count'] > 0) {
                        $doublecount+= $v['double_count'];
                        $guesslist['double'][] = $v;
                    }
                }
                include $this->bidcms_template('dialog_guesslist');
            }
        } else {
            include $this->bidcms_template('dialog_login');
        }
    }
    function goods_action() {
        $gid = $this->get('id',0);
        $myip = real_ip();
        $ipfile = ROOT_PATH . '/data/goods/currentcontent_' . $gid . '.txt';
        if (is_file($ipfile)) {
            $ips = explode("\n", file_get_contents($ipfile));
            if (!in_array($myip, $ips)) {
                file_put_contents($ipfile, $myip . "\n", FILE_APPEND | LOCK_EX);
            }
        } else {
            file_put_contents($ipfile, $myip . "\n");
        }
        if ($gid > 0) {
            $info = $this->bidcms_model('goods_base')->where(array('goods_id' => $gid, 'ispassed' => 1))->get_one();
            if ($info) {
                $uid = $GLOBALS['userinfo']['uid'];
                $thumb = thumb($info['thumb']);
                $info['difftime'] = intval($info['difftime'] > 0 ? $info['difftime'] : 10);
                $info['addtime'] = intval($info['addtime'] > 0 ? $info['addtime'] : 10);
                $buycount = '0';
                $havestatus = array();
                if ($uid) {
                    $havestatus = $this->bidcms_model('user_status')->fields('uid')->wherelike(array('title' => '购买'))->get_one();
                    $buylog = $this->bidcms_model('goods_bidlog')->where(['uid' => $uid, 'gid' => $gid])->get_one();
                    $buycount = $buylog['count'];
                }
                $info['button_status'] = $this->checkbutton($info);
                $details = $this->bidcms_model('goods_detail')->where(array('goods_id' => $gid))->get_one();
                
                $goods_list = array(
                  array('id'=>$info['goods_id'],'nowprice'=>$info['nowprice'],'currentuser'=>$info['currentuser'],'lasttime'=>$info['lasttime']-time(),'currentuid'=>$info['currentuid'])
                );
                //历史晒单
                $show_mod = $this->bidcms_model('user_show');
                $showlist = $show_mod->where(array('ispassed' => 1, 'goods_name' => $info['goods_name']))->get_page(array(), 'order by id desc');
                //自动竞拍
                if ($uid > 0) {
                    $auto_mod = $this->bidcms_model('goods_autobuy');
                    $autoinfo = $auto_mod->where(array('uid' => $uid, 'gid' => $gid, 'second' => array('gt', 0)))->get_one();
                }
                //竞猜
                $guess = $this->_guesscount($gid, $uid);
                $pagetitle = $info['goods_name'];
                if ($info['starttime'] > time()) {
                    include $this->bidcms_template('nostart');
                } else {
                    include $this->bidcms_template('details');
                }
            } else {
                sheader(url('index', 'complete', array('id' => $gid)));
            }
        } else {
            sheader(url('index', 'index'), 3, '参数为空,查看更多商品...');
        }
    }
    //商品详情
    function descgoods_action() {
        $gid = intval($this->get('id'));
        if ($gid > 0) {
            $info = $this->bidcms_model('goods_complete')->where(array('goods_id' => $gid))->get_one();
            if (!$info) {
                $info = $this->bidcms_model('goods_base')->where('goods_id', $gid)->get_one();
                if (!$info) {
                    sheader(url('index', 'history'), 3, '此产品已下架或已删除,查看更多秒杀产品...');
                }
            }
            $details = $this->bidcms_model('goods_detail')->where('goods_id', $gid)->get_one();
            
            $showlist = $this->_likegoods($info['goods_name']);
            $pagetitle = $info['goods_name'];
            include $this->bidcms_template('descgoods');
        } else {
            sheader(url('index', 'history'), 3, '此产品已下架或已删除,查看更多秒杀产品...');
        }
    }
    //完成商品
    function complete_action() {
        $gid = intval($this->get('id'));
        if ($gid > 0) {
            $chat_mod = $this->bidcms_model('chat');
            $bidlog_mod = $this->bidcms_model('goods_bidlog');
            $charlist = $chat_mod->where(array('gid' => $gid, 'issystem' => 1))->get_page();
            $bidlog_list = $bidlog_mod->where('gid', $gid)->fields(array('username', 'ip', 'address', 'price','uid','id'))->get_page(11, 'order by price desc');
            $bidlog_count = $bidlog_mod->where('gid', $gid)->fields(array('username', 'ip', 'address', 'price','id','count'))->get_page(11, 'order by count desc');
            $container = ['goods_status' => ['lt', 3], 'goods_id' => $gid];
            $info = $this->bidcms_model('goods_complete')->where($container)->get_one();
            $showlist = $this->_likegoods($info['goods_name']);
            if ($info) {
                $thumb = thumb($info['thumb']);
                $currentcount = $bidlog_mod->where(['uid' => $info['currentuid'], 'gid' => $gid])->get_one();
                $details = $this->bidcms_model('goods_detail')->where('goods_id', $gid)->get_one();
                //竞猜
                $guess = $this->_guesscount($gid, $GLOBALS['userinfo']['uid']);
                $pagetitle = $info['goods_name'];
                include $this->bidcms_template('complete');
            } else {
                sheader(url('index', 'history'), 3, '此产品已下架或已删除,查看更多秒杀产品...');
            }
        } else {
            sheader(url('index', 'index'), 3, '参数为空,查看更多秒杀产品...');
        }
    }
    //积分商城
    function gift_action() {
        $cateid = intval($this->post('cateid'));
        $gift_mode = $this->bidcms_model('goods_gift');
        $showpage = array('isshow' => 1, 'current_page' => intval($this->post('page')), 'page_size' => 20, 'url' => SITE_ROOT . '/index.php?con=admin&act=gift');
        $container = array("starttime" => array("elt", time()), 'lasttime' => array("egt", time()));
        if ($cateid) {
            $container['cateid'] = $cateid;
        }
        $gift = $gift_mode->where($container)->fields(array('thumb', 'subject', 'notice', 'id', 'marketprice', 'needcount', 'giftcount','company'))->get_page($showpage);
        $pagetitle = '积分商城';
        $pageinfo = $this->bidcms_parse_page($gift_mode->get_count(), $showpage);
        include $this->bidcms_template('gift');
    }
    //积分商城详情
    function gdetails_action() {
        $gift_mode = $this->bidcms_model('goods_gift');
        $id = intval($this->get('id'));
        if ($id) {
            $user_mod = $this->bidcms_model('user_base');
            if ($GLOBALS['userinfo']['uid']) {
                $muserinfo = $user_mod->where('uid', $GLOBALS['userinfo']['uid'])->get_one();
            } else {
                $muserinfo = array();
            }
            $info = $gift_mode->where('id', $id)->get_one();
            $thumb = thumb($info['thumb']);
            $othergoods = $gift_mode->where(array('starttime' => array('elt', time())))->get_page(10,'order by id desc');
            $pagetitle = $info['subject'];
            include $this->bidcms_template('gdetails');
        } else {
            sheader(url('index', 'gift'));
        }
    }
    //留言本
    function guestbook_action() {
        if ($this->bidcms_submit_check('commit')) {
            $guestbook_mod = $this->bidcms_model('user_guestbook');
            $code = $this->post('txt_checkcode');
            if ($code) {
                if (!$this->_checkcode($code)) {
                    $this->bidcms_error('验证码不正确');
                }
            } else {
                $this->bidcms_error('验证码不能为空');
                exit;
            }
            $data['title'] = strip_tags($this->post('title'));
            $data['email'] = $this->post('email');
            $data['content'] = htmlspecialchars(strip_tags($this->post('content')));
            $data['ispassed'] = !empty($GLOBALS['setting']['guestbook_passed']) ? 0 : 1;
            if($GLOBALS['userinfo']['uid']>0){
               $data['uid'] = $GLOBALS['userinfo']['uid'];
               $data['username'] = $GLOBALS['userinfo']['username'];
            } else {
               $data['uid'] = 0;
               $data['username'] = '匿名';
            }
            
            $data['updatetime'] = time();
            if ($guestbook_mod->insert_data($data)) {
                $this->bidcms_success('留言成功,感谢您对' . $GLOBALS['setting']['site_title'] . '的关注');
            } else {
                $this->bidcms_error('留言失败，请及时联系管理员');
            }
        } else {
            $showpage = array('isshow' => 1, 'current_page' => intval($this->post('page')), 'page_size' => 10, 'url' => SITE_ROOT . '/index.php?con=index&act=guestbook');
            $guestbook_mod = $this->bidcms_model('user_guestbook');
            $guestbook = $guestbook_mod->where('ispassed', 1)->fields(array('title', 'content', 'reply', 'isgood', 'givemoney', 'uid', 'username', 'updatetime'))->get_page($showpage);
            $pagetitle = '意见反馈';
            $count = $guestbook_mod->where('ispassed', 1)->get_count();
            $pageinfo = $this->bidcms_parse_page($count,$showpage);
            include $this->bidcms_template('guestbook');
        }
    }
    //关闭自动出价
    function closeauto_action() {
        $gid = $this->get('goodsid',0);
        $uid = $GLOBALS['userinfo']['uid'];
        if (!$gid) {
            exit('{"datastatus":"nogoods"}');
        }
        if (!$uid) {
            exit('{"datastatus":"nologin"}');
        }
        if ($gid && $uid) {
            $rows = $this->bidcms_model('goods_autobuy')->where(['uid' => $uid, 'gid' => $gid])->update_data(['second' => 0]);
            if ($rows) {
                exit('{"datastatus":"success"}');
            } else {
                exit('{"datastatus":"failed"}');
            }
        }
    }
    //自动出价
    function autobuy_action() {
        
        $uid = $GLOBALS['userinfo']['uid'];
        if ($this->bidcms_submit_check('commit')) {
            $gid = $this->post('goodsid',0);
            $autocount = intval($this->post('auto_count'));
            if ($autocount < 1) {
                $this->bidcms_error('未设置次数');
            }
            if (!$gid) {
                $this->bidcms_error('不存在此商品');
            } else {
                $goods = $this->bidcms_model('goods_base')->fields('diffmoney,islimit,limitnumber,shutuid')->where('goods_id', $gid)->get_one();
                if (!$goods) {
                    $this->bidcms_error('不存在此商品');
                } else {
                    $gdiffmoney = $goods['diffmoney'] > 0 ? $goods['diffmoney'] : ($GLOBALS['setting']['site_diffmoney'] > 0 ? $GLOBALS['setting']['site_diffmoney'] : 100);
                }
            }
            if (!$uid) {
                $this->bidcms_error(['datastatus'=>'nologin']);
            }
            if (empty($this->post('time'))) {
                $this->bidcms_error('请选择一个时间');
            }
            $automoney = $gdiffmoney * $autocount + floatval($GLOBALS['setting']['site_autobuymoney']);
            $user = $this->bidcms_model('user_base')->fields('money,uid,ip,username,address,verification')->where('uid', $uid)->get_one();
            if ($user['money'] < $automoney) {
                $this->bidcms_error(['datastatus'=>'nomoney','money'=>floatval($user['money']),'needmoney'=>$automoney]);
            }
            $shutuid = explode(',', $goods['shutuid']);
            if ($shutuid && in_array($uid, $shutuid)) {
                $this->bidcms_error('设置失败');
            }
            if ($user['verification'] < 1) {
                $this->bidcms_error('手机未验证');
            }
            if ($goods['islimit'] && $goods['limitnumber'] && $autocount > $goods['limitnumber']) {
                $this->bidcms_error(['datastatus'=>'islimit','number'=>$goods['limitnumber']]);
            }
            $data['uid'] = $uid;
            $data['number'] = intval($autocount);
            $data['gid'] = $gid;
            $data['maxnumber'] = intval($this->post('haved_count'));
            $data['username'] = $GLOBALS['userinfo']['username'];
            $data['ip'] = $GLOBALS['userinfo']['ip'];
            $data['mobile'] = $GLOBALS['userinfo']['mobile'];
            $data['second'] = intval($this->post('time'));
            $data['address'] = $GLOBALS['userinfo']['address'];
            $auto_mod = $this->bidcms_model('goods_autobuy');
            $autoinfo = $auto_mod->where(array('uid' => $uid, 'gid' => $gid))->get_one();
            if (empty($autoinfo)) {
                if ($auto_mod->insert_data($data)) {
                    //扣除自动竞拍手续费
                    if (floatval($GLOBALS['setting']['site_autobuymoney'])) {
                        $now_money = floatval($user['money']-abs($GLOBALS['setting']['site_autobuymoney']));
                        $this->bidcms_model('user_base')->update_cache_data(array('money'=>$now_money));
                        //写入资金变动记录
                        $this->bidcms_model('moneylog')->insert_data(
                          array(
                            'get' => 0, 
                            'put' => $GLOBALS['setting']['site_autobuymoney'],
                            'title' => '自动出价扣除手续费',
                            'uid' => $uid,
                            'updatetime' => time(),
                            'payment' => '系统',
                            'money' => $now_money,
                            'username' => $user['username']
                          )
                        );
                    }
                    $this->bidcms_success('设置成功');
                } else {
                    $this->bidcms_error('设置失败');
                }
            } elseif ($auto_mod->where(['uid' => $uid, 'gid' => $gid])->update_data($data)) {
                $this->bidcms_success('设置更新成功');
            } else {
               $this->bidcms_error('已经设置过了');
            }
        } else {
            $gid = $this->get('goodsid',0);
            if (!$GLOBALS['setting']['auto_buy']) {
                echo '管理员暂未开通此功能';
                exit;
            }
            if ($GLOBALS['userinfo']['uid'] == 0) {
                include $this->bidcms_template('dialog_login');
                exit;
            }
            if ($gid) {
                $goods_mod = $this->bidcms_model('goods_base');
                $goodsinfo = $goods_mod->fields(array('goods_id', 'goods_name'))->where('goods_id', $gid)->get_one();
                if ($goodsinfo) {
                    $auto_mod = $this->bidcms_model('goods_autobuy');
                    $autoinfo = $auto_mod->where(['uid' => $uid, 'gid' => $gid])->get_one();
                    include $this->bidcms_template('dialog_autobuy');
                } else {
                    echo '产品不存在';
                    exit;
                }
            } else {
                echo '产品不存在';
                exit;
            }
        }
    }
    //短信订阅
    function smslist_action() {
        $gid = intval($this->post('goodsid'));
        $uid = $GLOBALS['userinfo']['uid'];
        if ($this->bidcms_submit_check('commit')) {
            if (!$gid) {
                $error = '{"datastatus":"nogoods"}';
                exit($error);
            }
            if (!$uid) {
                $error = '{"datastatus":"nologin"}';
                exit($error);
            }
            $user_mod = $this->bidcms_model('user_base');
            $mobile = $user_mod->fields(array('mobile'))->where('uid', $uid)->get_one();
            $mobile = $mobile['mobile'];
            if (empty($mobile) || strlen($mobile < 11)) {
                $error = '{"datastatus":"nomobile"}';
                exit($error);
            }
            if (empty($this->post('time'))) {
                $error = '{"datastatus":"notime"}';
                exit($error);
            }
            $user = $this->bidcms_model('user_base')->fields('money,uid,mobile')->where('uid', $uid)->get_one();
            $goods = $this->goods->fields(array('goods_name', 'goods_id', 'lasttime'))->where('goods_id', $gid)->get_one();
            if (!$goods) {
                $error = '{"datastatus":"nogoods"}';
                exit($error);
            }
            $data['uid'] = $uid;
            $data['mobile'] = $user['mobile'];
            $data['gid'] = $gid;
            $data['goods_name'] = $goods['goods_name'];
            $data['updatetime'] = time();
            $data['smstime'] = intval($this->post('time'));
            $data['sendtime'] = $goods['lasttime'] - $data['smstime'] * 60;
            $sms_mod = $this->bidcms_model('smslist');
            if (!empty($GLOBALS['setting']['smslist_money']) && $user['money'] < floatval($GLOBALS['setting']['smslist_money'])) {
                $error = '{"datastatus":"nomoney","money":"' . intval($user['money']) . '"}';
                exit($error);
            }
            if ($sms_mod->insert_data($data)) {
                if (floatval($GLOBALS['setting']['smslist_money']) > 0) {
                    $this->bidcms_model('user_base')->where(['uid' => $user['uid']])->update_minus_data('money', floatval($GLOBALS['setting']['smslist_money']));
                    //添加资金操作日志记录代码
                    $this->bidcms_model('moneylog')->insert_data(array('get' => 0, 'put' => intval($GLOBALS['setting']['smslist_money']), 'title' => '短信订阅扣除金币', 'uid' => $uid, 'updatetime' => time(), 'payment' => '系统', 'money' => floatval($user['money'] - $GLOBALS['setting']['smslist_money']), 'username' => $user['username']));
                }
                $error = '{"datastatus":"success","sendtime":"' . date('Y-m-d h:i:s', $data['sendtime']) . '"}';
                exit($error);
            } else {
                $error = '{"datastatus":"failed"}';
                exit($error);
            }
        } else {
            if ($uid == 0) {
                include $this->bidcms_template('dialog_login');
                exit;
            }
            if ($gid) {
                $goods_mod = $this->bidcms_model('goods_base');
                $goodsinfo = $goods_mod->fields(array('goods_id', 'goods_name'))->where('goods_id', $gid)->get_one();
                if ($goodsinfo) {
                    include $this->bidcms_template('dialog_sms');
                } else {
                    echo '产品不存在';
                }
            } else {
                echo '产品不存在';
            }
        }
    }
    function dialog_action() {
        $dialogtype = $this->get('type');
        $charge_mod = $this->bidcms_model('user_charge');
        if ($dialogtype) {
            switch ($dialogtype) {
                case 'login':
                    include $this->bidcms_template('dialog_login');
                break;
                case 'charge':
                    $charge_list = $charge_mod->get_page();
                    include $this->bidcms_template('dialog_charge');
                break;
                case 'chargewait':
                    include $this->bidcms_template('dialog_chargewait');
                break;
            }
        }
    }
    //同名商品
    function _likegoods($goods_name) {
        if ($goods_name) {
            $goods_mod = $this->bidcms_model('goods_complete');
            $show_mod = $this->bidcms_model('user_show');
            $goods = $goods_mod->wherelike(array('goods_name' => $goods_name))->get_page();
            $showlist = $show_mod->wherelike(array('goods_name' => $goods_name))->get_page();
            foreach ($showlist as $k => $v) {
                $showlist[$k]['orderinfo'] = unserialize($v['order_info']);
            }
            return array($goods, $showlist);
        }
        return array();
    }
    //竞猜
    function _guesscount($gid, $uid = 0) {
        global $cache;
        if ($gid) {
            $guess_mod = $this->bidcms_model('guess_count');
            $guess = $guess_mod->where('gid', $gid)->get_one();
            if ($guess) {
                $guess['single_count'] = $guess['single_count'] ? $guess['single_count'] : 0;
                $guess['double_count'] = $guess['double_count'] ? $guess['double_count'] : 0;
                $guess['guesstotal'] = $guess['single_count'] + $guess['double_count'];
                $guess['single_p'] = $guess['guesstotal'] > 0 ? round($guess['single_count'] * 100 / $guess['guesstotal'], 1) : 0;
                $guess['double_p'] = $guess['guesstotal'] > 0 ? round($guess['double_count'] * 100 / $guess['guesstotal'], 1) : 0;
                $guess['double_discount'] = $guess['double_count'] > 0 ? round($guess['single_count'] * 0.9 / $guess['double_count'], 2) + 1 : 1;
                $guess['single_discount'] = $guess['single_count'] > 0 ? round($guess['double_count'] * 0.9 / $guess['single_count'], 2) + 1 : 1;
            } else {
                $data['gid'] = $gid;
                $data['single_count'] = 0;
                $data['double_count'] = 0;
                $guess_mod->insert_data($data);
                $data['guesstotal'] = 0;
                $data['single_p'] = 0;
                $data['double_p'] = 0;
                $data['single_discount'] = 1;
                $data['double_discount'] = 1;
                $guess = $data;
            }
            $guess['win'] = 0;
            $guess['you'] = 0;
            $guess['giveusermoney'] = 0;
            $guess['my_single'] = 0;
            $guess['my_double'] = 0;
            $goods_mod = $this->bidcms_model('goods_complete');
            $goodsinfo = $goods_mod->fields(array('nowprice', 'goods_id'))->where('goods_id', $gid)->get_one();
            $nowprice = 0;
            if ($goodsinfo && $goodsinfo['nowprice']>0) {
                $p = ''.floatval($goodsinfo['nowprice']);
                $nowprice = intval(substr($p,-1));
                $guess['wintype'] = $nowprice % 2 == 0 ? 'double' : 'single';
                $guess['win'] = 1;
            }
            if ($uid > 0) {
                $guessuser_mod = $this->bidcms_model('guess_base');
                $guessuser = $guessuser_mod->where(['uid' => $uid, 'gid' => $gid])->get_one();
                if(!empty($guessuser)){
                  $guess['my_single'] = $guessuser['single_count'];
                  $guess['my_double'] = $guessuser['double_count'];
                  $guess['you'] = 1;
                }
                if ($guess['win'] == 1 && $nowprice>0) {
                    if ($guessuser) {
                        if ($guess['wintype'] == 'single') {
                          $addmoney = $guess['my_single'] * $guess['single_discount'];
                        } elseif ($guess['wintype'] == 'double') {
                          $addmoney = $guess['my_double'] * $guess['double_discount'];
                        }
                        if($guessuser['isgive'] == 0) {
                            $this->bidcms_model('user_base')->where(['uid' => $uid])->update_add_data('money', $addmoney);
                            $cache->update('user'.($uid%10).':'.$uid,array('money'=>floatval($GLOBALS['userinfo']['money']+$addmoney)));
                            $this->bidcms_model('guess_base')->where(['gid' => $gid, 'uid' => $uid])->update_data(['isgive' => 1]);
                            //添加资金操作日志记录代码
                            $this->bidcms_model('moneylog')->insert_data(array('get' => $addmoney, 'put' => 0, 'title' => "第" . $gid . "期，竞猜奖励", 'uid' => $uid, 'updatetime' => time(), 'payment' => '系统', 'money' => floatval($GLOBALS['userinfo']['money'] + abs($addmoney)), 'username' => $GLOBALS['userinfo']['username']));
                        }
                      	$guess['giveusermoney'] = $addmoney;
                    }
                }
            }
            $guess['login'] = $uid;
            return $guess;
        }
        return array();
    }
}
