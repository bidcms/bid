<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: index.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
class article_controller extends front_controller
{
	function index_action()
	{
		$id = $this->get('id',0);
        $container = array();
		$showpage=array('isshow'=>1,'current_page'=>intval($this->bidcms_request('page')),'page_size'=>20,'url'=>'index.php?con=article','example'=>2);
        $pagetitle='文章列表';
		if($id)
		{
            if(isset($GLOBALS['article_cate'][$id])){
				$pagetitle=$GLOBALS['article_cate'][$id]['catename'].'文章列表';
            }
            $container = array('cateid'=>$id);
		}
        $articlelist=$this->bidcms_model('article_base')->where($container)->get_page($showpage,'order by id desc');
        $count = 0;
        if(count($articlelist)>0){
        	$count = $this->bidcms_model('article_base')->where($container)->get_count();
        }
        $pageinfo = $this->bidcms_parse_page($count,$showpage);
		include $this->bidcms_template('article_list');
	}
	function help_action()
	{
		$id = $this->get('id',0);
		$group=$this->_custom_group(2,'help');
        $article = array('title'=>'','content'=>'');
        $pagetitle = '文章列表';
        if($id>0){
          $article=$this->bidcms_model('article_base')->where(array('id'=>$id,'cateid'=>2))->get_one();
          if(!empty($info)){
            $article = $info;
            $pagetitle=!empty($article)?$article['title']:'文章列表';
          }
        }
		include $this->bidcms_template('help');
	}
	function view_action()
	{
		$id = $this->get('id',0);
        $article = array();
        if($id>0){
			$article=$this->bidcms_model('article_base')->where('id',$id)->get_one();
        }
        if($article){
          $pagetitle=$article['title'];
          include $this->bidcms_template('article_view');
        } else {
          sheader('index.php?con=article',3,'内容不存在');
        }
		
	}
	//生成自定义分组
	function _custom_group($cateid,$act='')
	{
		$article=$this->bidcms_model('article_base')->fields(array('custom_group','title','id'))->where(['cateid'=>$cateid])->get_page();
        $group = array();
		foreach($article as $v)
		{
			$group[$v['custom_group']][]=array('title'=>$v['title'],'url'=>url('article',$act,array('id'=>$v['id'])));
		}
		return $group;
	}
		
}