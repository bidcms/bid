<?php
session_start();
ini_set('display_errors',1);            //错误信息
ini_set('display_startup_errors',1);    //php启动错误信息
error_reporting(-1);                    //打印出所有的 错误信息
set_time_limit(1000);
require('inc/common.inc.php');

define("SYSTEM_PATH",dirname(__FILE__).'/');
define("CONTROLS_PATH",SYSTEM_PATH.'controls/admin/');
define("MODELS_PATH",SYSTEM_PATH.'models/');
define("VIEWS_PATH",SYSTEM_PATH.'views/admin/');

define('SITE_ROOT','http://demo.bidcms.com/');
define('STATIC_ROOT','http://demo.bidcms.com/static/');
define('TPL_DIR','admin');
define('UC_API','');

include CONTROLS_PATH.'console.class.php';
$controller=empty($_REQUEST['con'])?'index':$_REQUEST['con'];
$action=empty($_REQUEST['act'])?'index':$_REQUEST['act'];


$file=CONTROLS_PATH.$controller.'.class.php';

if(is_file($file)) {
	include_once($file);
	$conclass=$controller.'_controller';
	$actfunc=$action.'_action';
	if(method_exists($conclass, $actfunc)){
		$views=new $conclass;
		$views->$actfunc();
	}else{
		echo $actfunc.'()方法不存在';
	}
}else{
	echo $controller.".class.php控制器文件不存在";
}
