<?php
/*
	[Bidcms.com!] (C)2009-2011 Bidcms.com.
	This is NOT a freeware, use is subject to license terms
	$author limengqi
	$Id: userclass.php 2016-03-24 10:42 $
*/
if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
class goods_complete_class extends model
{
	public $table='complete';
	public $definition=array(
		'join'=>'goods_id'
	);
    public function get_cache_success(){
      //缓存成功信息
      global $cache;
      $successinfo = $cache->get('successinfo');
      if(empty($successinfo))
      {
          $successinfo = $this->get_page(10,'order by updatetime desc');
		  $cache->set('successinfo',$successinfo);
      }
      return $successinfo;
    }
    public function get_cache_stat(){
      //缓存成功信息
      global $cache;
      $stat = $cache->get('stat');
      if(empty($stat))
      {
          $stat = $this->get_count();
		  $cache->set('stat',$stat);
      }
      return $stat;
    }
}
