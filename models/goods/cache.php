<?php
/*
	[Bidcms.com!] (C)2009-2011 Bidcms.com.
	This is NOT a freeware, use is subject to license terms
	$author limengqi
	$Id: userclass.php 2016-03-24 10:42 $
*/
if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
class goods_cache_class extends model
{
	public $table='goods_cache';
	public $definition=array(
		'primary'=>'goods_id'
	);
	public $fields = array(
		'goods_id'=>'',
        'goods_name'=>'',
        'nowprice'=>'',
        'lasttime'=>'',
        'currentuser'=>'',
        'currentuid'=>'',
        'difftime'=>'',
        'addtime'=>'',
        'autobuy_time'=>'',
        'autobuy_money'=>'',
        'diffmoney'=>'',
        'diffprice'=>'',
        'diffscore'=>'',
        'isfree'=>'',
        'auto_times'=>'',
        'usertype'=>''
	);
}
