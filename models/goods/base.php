<?php
/*
	[Bidcms.com!] (C)2009-2011 Bidcms.com.
	This is NOT a freeware,
 use is subject to license terms
	$author limengqi
	$Id: userclass.php 2016-03-24 10:42 $
*/
if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
class goods_base_class extends model
{
	public $table='goods';
	public $definition=array(
		'primary'=>'id',
		'join'=>'cateid'
	);
	public $fields = array ( 
		'goods_id' => '',
		'goods_name' => '',
		'marketprice' => '',
		'nowprice' => '',
		'starttime' => '',
		'lasttime' => '',
		'currentuser' => '',
		'currentuid' => '',
		'thumb' => '',
		'ishot' => '',
		'content' => '',
		'diffmoney' => '',
		'diffprice' => '',
		'diffscore' => '',
		'updatetime' => '',
		'cateid' => '',
		'shutuid' => '',
		'limitnumber' => '',
		'islimit' => '',
		'ispassed' => '',
		'hostuid' => '',
		'hostuser' => '',
		'isfree' => '',
		'needsign' => '',
		'backmoney' => '',
		'yfeemoney' => '',
		'difftime' => '',
		'addtime' => '',
		'autobuy_time' => '',
		'autobuy_money' => '',
		'auto_times' => ''
	);
}
