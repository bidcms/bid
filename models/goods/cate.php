<?php
/*
	[Bidcms.com!] (C)2009-2011 Bidcms.com.
	This is NOT a freeware, use is subject to license terms
	$author limengqi
	$Id: userclass.php 2016-03-24 10:42 $
*/
if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
class goods_cate_class extends model
{
	public $table='cate';
	public $definition=array(
		'primary'=>'id'
	);
	public $fields = array(
		'id'=>'',
		'catename'=>'required:true:分类名称不能为空',
		'sortorder'=>'',
		'custom_group'=>''
	);
    public function get_cache_list(){
      //缓存成功信息
      global $cache;
      $list = $cache->get('cate');
      if(empty($list))
      {
          $list = $this->get_page(array('index'=>'id'));
		  $cache->set('cate',$list,86400);
      }
      return $list;
    }
}
