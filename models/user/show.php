<?php
/*
	[Bidcms.com!] (C)2009-2011 Bidcms.com.
	This is NOT a freeware, use is subject to license terms
	$author limengqi
	$Id: userclass.php 2016-03-24 10:42 $
*/
if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
class user_show_class extends model
{
	public $table='show';
	public $definition=array(
		'primary'=>'id',
        'join'=>'uid'
	);
    public $fields = array(
    	'id'=>'','uid'=>'','title'=>'','content'=>'','oid'=>'','username'=>'','order_info'=>'','updatetime'=>'','ispassed'=>'','givemoney'=>'','goods_name'=>'','hits'=>'','comments'=>''
    );
    public function get_cache_list(){
      global $cache;
      $show = $cache->get("show_top");
      if(empty($show))
      {
          $list = $this->fields('title,username,uid,order_info,id')->get_page(3,'order by id desc');
          foreach($list as $rows)
          {
              $orderinfo=unserialize($rows['order_info']);
              $goods=$orderinfo['goods_info'];
              $thumb=thumb($goods['thumb']);
              $rows['thumb']=$thumb[0];
              $rows['orderinfo']=$orderinfo;
              $show[]=$rows;
          }
          $cache->set('show_top',$show,3600);
      }
      return $show;
    }
}
