<?php
/*
	[Bidcms.com!] (C)2009-2011 Bidcms.com.
	This is NOT a freeware, use is subject to license terms
	$author limengqi
	$Id: userclass.php 2016-03-24 10:42 $
*/
if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
class system_link_class extends model
{
	public $table='link';
    public $fields = array(
    	'id'=>'','title'=>'','url'=>'','dec'=>'','type'=>'','thumb'=>'','sortorder'=>''
    );
    public function get_cache_list($key = ''){
      global $cache;
      $link = $cache->get('link');
      if(empty($link)){
        $link = $this->get_page(array('index'=>'type','list'=>true),'order by sortorder desc');
        $cache->set('link',$link,86400);
      }
      return $link;
    }
}
