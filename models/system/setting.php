<?php
/*
	[Bidcms.com!] (C)2009-2011 Bidcms.com.
	This is NOT a freeware, use is subject to license terms
	$author limengqi
	$Id: userclass.php 2016-03-24 10:42 $
*/
if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
class system_setting_class extends model
{
	public $table='setting';
    public function get_cache_list($key = ''){
      global $cache;
      $setting = $cache->get('setting');
      if(empty($setting)){
        $list = $this->get_page();
        foreach($list as $k=>$v){
          $setting[$v['variable']] = $v['content'];
        }
        $cache->set('setting',$setting);
      }
      return !empty($key)?$setting[$key]:$setting;
    }
}
