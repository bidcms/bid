<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
header("content-type:text/html;charset=utf-8");
?>
<div style="position: relative; z-index: 99;" class="game_list01">
			            
            <div class="gl_left1">
            <div class="gl_info">
			<div class="gl_center">
            	<div class="gl_icon1 icon1">单</div>
            	<div class="left">共有<span class="yellow66"><?php echo isset($guesslist['single'])?count($guesslist['single']):0;?></span>人，共投入<span class="yellow66"><?php echo $singlecount;?></span><?php echo $GLOBALS['setting']['site_money_name'];?></div>
			</div>
            </div>
            <table width="256" border="1" class="gl_table">
              <tbody><tr bgcolor="#ebebeb">
                <td width="45">投入</td>
                <td width="124">投入者</td>
                <td width="84">投入<?php echo $GLOBALS['setting']['site_money_name'];?></td>
              </tr>
			  <?php if($guesslist['single']){foreach($guesslist['single'] as $v){?>
              <tr bgcolor="#f4f4f4">
                <td><div class="gl_icon1">单</div></td>
                <td><div class="per_name"><img height="13px" width="13px" src="<?php echo UC_API."/avatar.php?uid=".$v['uid']."&size=middle&type=virtual";?>"><span class="blue"><?php echo $v['username'];?></span></div></td>
                <td class="per_pr5"><?php echo $v['single_count'];?></td>
              </tr>
			  <?php }}?>
                          </tbody></table>
            </div>
            <div class="gl_left">
                <div class="gl_info">
					<div class="gl_center">
						<div class="gl_icon2 icon1">双</div><div class="left">共有<span class="yellow66"><?php echo isset($guesslist['double'])?count($guesslist['double']):0;?></span>人，共投入<span class="yellow66"><?php echo $doublecount;?></span><?php echo $GLOBALS['setting']['site_money_name'];?>
					</div>
				</div>
            </div>
            <table width="256" border="1" class="gl_table">
              <tbody><tr bgcolor="#ebebeb">
                <td width="45">投入</td>
           		<td width="124">投入者</td>
                <td width="84">投入<?php echo $GLOBALS['setting']['site_money_name'];?></td>
              </tr>
			  <?php if($guesslist['double']){foreach($guesslist['double'] as $v){?>
                <tr bgcolor="#f4f4f4">
                <td><div class="gl_icon2">双</div></td>
                <td><div class="per_name"><img height="13px" width="13px" src="<?php echo UC_API."/avatar.php?uid=".$v['uid']."&size=middle&type=virtual";?>"><span class="blue"><?php echo $v['username'];?></span></div></td>
                <td class="per_pr5"><?php echo $v['double_count'];?></td>
              </tr>
			  <?php }}?>
                          </tbody></table>
            </div>
                    </div>