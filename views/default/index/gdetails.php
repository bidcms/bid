<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/descgoods.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>

<?php include(VIEWS_PATH."public/header.php");?>

<div class="container980 mar10 border3"> <a name="bid"></a> 
<span class="left pidname"><?php echo $info['subject'];?></span>
  <div class="clear"></div>
</div>
<?php $thumb=thumb($info['thumb']);?>
<!-- 已有往期竞拍开始 -->
<div class="showsheetlist mar10">
  <!--商品图片开始-->
  <div class="bid_left borD6">
    <div class="bid_leftmain">
    <div class="bid_img">
      <div class="bid_bigimg">
      		<img id="pbigimg" src="<?php echo $thumb[0];?>" alt="<?php echo $info['goods_name'];?>" height="308" width="366" >
      </div>
      <ul class="thumb">
	<?php foreach($thumb as $val){?>
     <li class="borD6_img"><img src="<?php echo $val;?>"  width="63" height="53" onclick="$('#pbigimg').attr('src',this.src);"/></li>
	 <?php }?>
		      </ul>
    </div>
	</div>
    <div class="public_corner public_topleft2"></div>
    <div class="public_corner public_topright2"></div>
    <div class="public_corner public_bottomleft"></div>

    <div class="public_corner public_bottomright"></div>
  </div>
  <!--商品图片结束-->
  <div class="bid_center">
    <div>
    <div class="bid_cy1 left">
      <div class="jiao2"></div>
      <div class="cy1_top font24b">
     所需 <?php echo $info['needcount'];?>积分
      </div>

      <div class="cy1_bottom">
        您目前还有：<?php if($muserinfo){?><span class="font12b marr5"><?php echo $muserinfo['score'];?></span>积分
		<?php } else{?>
		<A HREF="javascript:Login_Dialog();">登录</A>
		<?php }?>
      </div>
    </div>
    <div class="bid_cy2 left">
      
       
           
      <!-- 没有正在竞拍中的 -->      
      <div class="left">
        <div class="bid_cy2font">市场价：<span class="overline1 bid_cy2fontprice"><?php echo $info['marketprice'];?></span>元
        </div>

      </div>
          <div class="cy_corner cy_topright"></div>
    <div class="cy_corner cy_bottomright"></div>
    </div>
      <div class="clear"></div>
    </div>
   <div class="newpro">
		每次竞拍出价可获得<?php echo $GLOBALS['setting']['site_diffscore'];?>个积分<br/>
积满相应积分，即可直接换购所需商品，全场包邮费。
<p>
库存：<?php echo $info['giftcount'];?></p>
<p>
供应商：<?php echo $info['company'];?></p>

<p>
有效期：<?php echo date('m-d H:i:s',$info['starttime']);?>-<?php echo date('m-d H:i:s',$info['lasttime']);?></p>
<p>
<?php echo $info['notice'];?></p>
    </div>
	<DIV ID="" style="text-align:center;margin-top:30px;">
	<?php if($muserinfo){?>
	<?php if($muserinfo['score'] >= $info['needcount']){?>
	<INPUT TYPE="button"  class="scorebutton" VALUE="立即换购" ONCLICK="Score_Dialog('<?php echo $info['id'];?>');">
	<?php } else{?>
	<INPUT TYPE="button" class="scorebutton" VALUE="积分不足" ONCLICK="Scorecharge_Dialog('<?php echo $info['id'];?>');">
	<?php }?>
	<?php } else{?>
	<INPUT TYPE="button" class="scorebutton" VALUE="登录" onclick="Login_Dialog();">
	<?php }?>
	</DIV>
  </div>

  <div class="bid_right">
    <div class="bid_rtop">
    
    </div>
  </div>
  <div class="clear"></div>
</div><!-- 已有往期竞拍结束 -->

<!-- 新品结束 -->
<div class="clear"></div>
</div>
</div>
</div>
<div class="details mar10 borD6">
  <dl>
    <dt class="font14b">商家信息</dt>
    <dd class="paddingt0">
	  <?php echo $info['companyinfo'];?>
    </dd>

  </dl>
  <div class="public_corner public_topleft2"></div>
  <div class="public_corner public_topright2"></div>
  <div class="public_corner public_bottomleft"></div>
  <div class="public_corner public_bottomright"></div>
  <div class="clear"></div>
</div>
<div class="details mar10 borD6">
  <dl>
    <dt class="font14b">商品详细介绍</dt>

    <dd>
    <?php echo $info['details'];?>
    </dd>
  </dl>
  <div class="public_corner public_topleft2"></div>

  <div class="public_corner public_topright2"></div>
  <div class="public_corner public_bottomleft"></div>
  <div class="public_corner public_bottomright"></div>
  <div class="clear"></div>
</div>

<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->