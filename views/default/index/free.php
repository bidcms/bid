<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/free.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/global.js"></SCRIPT>

<?php include(VIEWS_PATH."public/header.php");?>
<div class="container980">
	<!--左侧部分开始-->
	<div class="experience_left">
	
		<img src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/experience/experience_03.gif" alt="温馨提示" />
		<!--体验区开始-->

		<div class="experience_box borD6 mar10">
			<dl>
			<dt class="black"><strong>新用户体验列表</strong></dt>
			<dd>
			<?php if($goods){foreach($goods as $v){$thumb=thumb($v['thumb']);?> 
				<!--单个开始-->
				<div class="xibi_box borFC" style="position:relative;">
				<div class="xibi_boxx" id="exp_<?php echo $v['goods_id'];?>">
				<span class="name">

				<a href="<?php echo url('index','goods',array('id'=>$v['goods_id']));?>" title="<?php echo $v['goods_name'];?>">
				<strong><?php echo $v['goods_name'];?></strong></a><span class="grayB4">[第<?php echo $v['goods_id'];?>期]</span></span>
				<span class="img">
				<a href="<?php echo url('index','goods',array('id'=>$v['goods_id']));?>" title="<?php echo $v['goods_name'];?>"><img src="<?php echo $thumb[0];?>" width="180" height="150" border="0" alt="<?php echo $v['goods_name'];?>" /></a>					</span>
				<div class="time"><span  id="lasttime_<?php echo $v['goods_id'];?>" name="lefttime">--:--:--</span></div>
				<span class="price">￥<span id="nowprice_<?php echo $v['goods_id'];?>" name="price">-.-</span></span>

				<span class="user blue" id="currentuser_<?php echo $v['goods_id'];?>" name="username">--</span>
				<div class="button">
					<?php if($v['button_status']=='ok'){?>
					<!--1.可以竞拍开始-->
					<div class="recbidbutton rcbid" id="bid_<?php echo $v['goods_id'];?>" onmouseout="this.className='recbidbutton'" onmousedown="this.className='recbidbuttondown';setInfo('<?php echo $v['goods_id'];?>');">
					<a style="cursor: pointer;" href=""></a>
					</div>
					<!--可以结束-->
					<?php } elseif($v['button_status']=='nologin'){?>
					<!--2.未登录开始-->
					<div id="bid_<?php echo $v['goods_id'];?>" class="recbidlogin rcbid">
					<a href="javascript:Login_Dialog();" style="cursor:pointer;"></a>
					</div>
					<!--未登录结束-->
					<?php } elseif($v['button_status']=='needsign'){?>
					<!--3.需要报名开始-->
					<div id="bid_<?php echo $v['goods_id'];?>" class="recbidapply rcbid">
					<a href="javascript:Login_Dialog();" style="cursor:pointer;"></a>
					</div>
					<!--需要报名结束-->	
					<?php } elseif($v['button_status']=='moneyerror'){?>
					<!--4.不足开始-->
					<div id="bid_<?php echo $v['goods_id'];?>" class="recbidcharge rcbid">
					<a href="javascript:Charge_Dialog();" style="cursor:pointer;"></a>
					</div>
					<!--不足结束-->
					<?php } elseif($v['button_status']=='needconfirm'){?>
					<!--5.手机未验证开始-->
					<div id="bid_<?php echo $v['goods_id'];?>" class="recbidphone rcbid">
					<a href="<?php echo url('user','mobile');?>" style="cursor:pointer;"></a>
					</div>
					<!--手机未验证结束-->
					<?php }?>

				</div>
				</div>

				<div class="public_corner public_topleft6"></div>
				<div class="public_corner public_topright6"></div>
				<div class="public_corner public_bottomleft6"></div>
				<div class="public_corner public_bottomright6"></div>
				</div>
				<!--单个结束-->
			<?php }}?>
						</dd>
			</dl>
			<div class="public_corner public_topleft2"></div>
			<div class="public_corner public_topright2"></div>
			<div class="public_corner public_bottomleft"></div>
			<div class="public_corner public_bottomright"></div>

		</div>
<!--体验区结束-->
<?php if($goods){?>
<INPUT TYPE="hidden" id="ids" value="<?php echo implode('-',$ids);?>">
<INPUT TYPE="hidden" id="oids" value="<?php echo implode('-',$ids);?>">
  <textarea style="display:none;" id="goods_list"><?php echo json_encode($goods_list);?></textarea>
<SCRIPT LANGUAGE="JavaScript">
<!--
getInfo();
window.onload=function(){checkcomplete();}
//-->
</SCRIPT>
<?php }?>	
  </div>
	<!--左侧部分结束-->
	<!--右侧部分开始-->
	<div class="experience_right">
      <div>
              <a href="<?php echo url('user','register');?>"><img src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/head/register.gif" alt="注册即送500<?php echo $GLOBALS['setting']['site_money_name'];?>" /></a>
      </div>

      <!--新手指引开始-->
      <div class="right_box borD6 mar10">
        <h3 class="guideimg">只需三步 轻松免费体验</h3>
        <div class="guide">
          <span class="a">①登录<?php echo $GLOBALS['setting']['site_title'];?><br />进入体验区</span>
          <span class="b">②参与竞拍<br /><strong class="red">1<?php echo $GLOBALS['setting']['site_money_name'];?>体验</strong></span>

          <span class="c">③赢得竞拍<br />支付成交价</span>
        </div>
      </div>
      <!--新手指引结束-->

      <!--本区获胜者开始-->
      <div class="right_box borD6 mar10">
      <dl>
              <dt class="black"><strong>本区获胜者</strong></dt>
              <dd class="tbpad6">
              <ul class="xiyou_do">
              <?php if($win){foreach($win as $v){?>
              <li> <img src="<?php echo UC_API."/avatar.php?uid=".$v['currentuid']."&size=middle&type=virtual";?>" width="13" height="13" class="index_avatar" />
              <div class="index_username"> <span class="blue"><?php echo $v['currentuser'];?>：</span>以&nbsp;<span class="yellow66 font14"><strong>￥<?php echo $v['nowprice'];?></strong></span>&nbsp;
              获得<a href="<?php echo url('index','complete',array('id'=>$v['goods_id']));?>" title="<?php echo $v['goods_name'];?>[第<?php echo $v['goods_id'];?>期]"><?php echo $v['goods_name'];?></a><span class="grayB4">[第<?php echo $v['goods_id'];?>期]</span> </div>

              </li>
              <?php }}?>
              </ul>
              </dd>
        </dl>
          <div class="public_corner public_topleft2"></div>
          <div class="public_corner public_topright2"></div>
          <div class="public_corner public_bottomleft"></div>
          <div class="public_corner public_bottomright"></div>
      </div>


      <div class="left_box borD6 mar10">
      <dl>
              <dt class="black"><strong>常见问题</strong></dt>
              <dd>
              <ul class="problem">

                  <li><a href="/help/register.html" target="_blank">如何注册会员？</a></li>
                  <li><a href="/help/dzpayment.html" target="_blank">如何购买<?php echo $GLOBALS['setting']['site_money_name'];?>参与竞拍？</a></li>
                  <li><a href="/help/my-account.html" target="_blank">如何查看我订单状态？</a></li>
                  <li><a href="/help/faq.html" target="_blank">如何获得免费<?php echo $GLOBALS['setting']['site_money_name'];?>？</a></li>
              </ul>
              </dd>
        </dl>

          <div class="public_corner public_topleft2"></div>
          <div class="public_corner public_topright2"></div>
          <div class="public_corner public_bottomleft"></div>
          <div class="public_corner public_bottomright"></div>
      </div>
	</div>
	<!--右侧部分结束-->
<div class="clear"></div>
</div>
<SCRIPT LANGUAGE="JavaScript">
<!--
	show_alert();
//-->
</SCRIPT>
<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->