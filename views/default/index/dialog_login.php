<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
?>
<form METHOD="POST" ACTION="<?php echo SITE_ROOT;?>/index.php" data-type="ajax">
<input TYPE="hidden" NAME="con" value="user"><input TYPE="hidden" NAME="act" value="login"><input TYPE="hidden" NAME="ajax" value="1"><input TYPE="hidden" NAME="commit" value="1">
<div class="left login_t tmar6"> <span class="left font14 login_line">帐户名：</span> <span class="left">
<input type="text" value="" id="txt_username" name="username" class="login_input" tabindex="3">
</span> </div>
<div class="left login_t"> <span class="left font14 login_line">密　码：</span> <span class="left">
<input type="password" name="password" id="txt_password" class="login_input" tabindex="3">
</span> <a style="line-height: 38px; padding-left: 10px;" target="_blank" href="<?php echo url('user','forget');?>"><span class="blue" tabindex="5">忘记密码?</span></a> </div>
<?php if($GLOBALS['setting']['site_logincode']){?>
<div class="left login_t tmar6"> <span class="left font14 login_line">验证码：</span> <span class="left">
<input class="login_input" type="text" id="txt_verify" name="verify" maxlength="4" tabindex="3" style="ime-mode:disabled;width:100px;"/>
<img src="" id="txt_code" onclick="getCode();"/>
</span> </div>
 <?php }?>
<div class="layer_w333 left mar10">
<input type="button" id="btn_submit" value="登 录"  tabindex="4" onmouseout="this.className='layer_button login_t2 left'" onmousemove="this.className='layer_button_over login_t2 left'" class="layer_button login_t2 left"> <span style="line-height: 30px; margin-left: 10px; display: inline;" class="left red"><span id="spStatus"></span></span> </div>
<div class="layer_msg layer_w333 mar20"> 还没有开通<?php echo $GLOBALS['setting']['site_title'];?>账号？ <strong> <a target="_blank" href="<?php echo url('user','register');?>"> <span class="blue">现在去创建一个</span>。 </a> </strong> </div>
</form>
<script>
  getCode();
  $('#btn_submit').click(function(){
        var cb = $(this).attr('data-cb');
		$("form[data-type='ajax']").ajaxSubmit({
			success:function(data){
                if(cb){
                  eval(cb+'('+res+')');
                } else {
                  if(data.code == 0){
                      if(data.url!=''){
                        window.location.replace(data.url);
                      } else {
                        window.location.replace('/index.php');
                      }
                  } else {
                    alert(data.msg);
                  }
                }
				
			}
		});
	});
</script>