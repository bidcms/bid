<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
?>
<div class="sms_main_div" style="padding:0px;">
<FORM METHOD="POST" ACTION="<?php echo SITE_ROOT;?>/index.php" data-type="ajax">
<INPUT TYPE="hidden" NAME="commit" value="1">
<INPUT TYPE="hidden" NAME="act" value="autobuy">
<INPUT TYPE="hidden" NAME="con" value="index">

<div class="sms_main">
    <div class="sms_main_name blue"> <a target="_blank" href="<?php echo url('index','goods',array('id'=>$goodsinfo['goods_id']));?>">[第<?php echo $goodsinfo['goods_id'];?>期] <?php echo $goodsinfo['goods_name'];?></a></div>

	<?php if($GLOBALS['setting']['site_autobuymoney']>0){?>
	<div class="sms_main_time" style="padding:10px;height:32px;"><span class="smstxt" style="font-size:12px;">同一期竞拍首次开启自动出价仅需：<span class="yellow66"><?php echo $GLOBALS['setting']['site_autobuymoney'];?></span><?php echo $GLOBALS['setting']['site_money_name'];?>
    </span></div>
	<?php }?>
    <div class="sms_main_time" style="padding:5px;"><span class="smstxt">结束前:</span><span class="smstxt">
      <select class="sms_select" id="sel_ftime" name="time">
		<?php if($autoinfo && $autoinfo['second']>0){?><option value="<?php echo $autoinfo['second'];?>"><?php echo $autoinfo['second'];?>秒</option><?php }?>
        <option value="10">10秒</option>
        <option value="9">9秒</option>
        <option value="8">8秒</option>
        <option value="7">7秒</option>
        <option value="6">6秒</option>
		<option value="5">5秒</option>
		<option value="4">4秒</option>
		<option value="3">3秒</option>
		<option value="2">2秒</option>
		<option value="1">1秒</option>
		</select>&nbsp;&nbsp;出价
      </span></div>
	   <div class="sms_main_time" style="padding:5px;"><span class="smstxt">出&nbsp;&nbsp;&nbsp;&nbsp;价:</span><span class="smstxt"><input type="text" id="xib_count" name="auto_count" class="wyt_button" value="<?php echo $autoinfo['number']?$autoinfo['number']:1;?>">&nbsp;&nbsp;次</span></div>

	   <div class="sms_main_time" style="padding:5px;"><span class="smstxt">总出价:</span><span class="smstxt"><input type="text" id="xib_count" name="haved_count" class="wyt_button" value="<?php echo $autoinfo['maxnumber']?$autoinfo['maxnumber']:1;?>">&nbsp;&nbsp;次后参与</span></div>
	
    <div class="clear"></div>
  </div>
 
  <div class="sms_button">
	<input type="hidden" value="<?php echo $goodsinfo['goods_id'];?>" name="goodsid">
    <input type="button"  value="确 定" onmouseout="this.className='layer_button left'" onmousemove="this.className='layer_button_over left'" id="btn_submit" class="layer_button left">
    <span class="yellow14 cancel_txt dialog_close cursor" onclick="Dialog_close();">取 消</span></div>
  <div class="clear"></div>

</form>
</div>
<script>
  function parseAutoError(dataobj) {
		if(dataobj.datastatus=='nomoney') {
			alert('金币不足,当前您的金币为'+dataobj.money+',所需金币为：'+dataobj.needmoney);
		} else if(dataobj.datastatus=='nologin') {
			Login_Dialog();
		} else if(dataobj.datastatus=='islimit') {
			alert('此商品是限次竞拍,限制次数为'+dataobj.number);
		}

  }
  $('#btn_submit').click(function(){
        var cb = $(this).attr('data-cb');
		$("form[data-type='ajax']").ajaxSubmit({
			success:function(res){
                var data =JSON.parse(res);
                if(cb){
                  eval(cb+'('+res+')');
                } else {
                  if(data.code == 0){
                      alert(data.msg);
                      $('#to_start').attr('src',$('#to_start').attr('src').replace('over','down'));
                      $('#to_cancel').attr('src',$('#to_cancel').attr('src').replace('down','over'));
                      Dialog_close();
                  } else {
                     if(data.data){
                    	parseAutoError(data.data);
                     } else {
                       alert(data.msg);
                     }
                  }
                }
				
			}
		});
	});
</script>