<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/cate.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/global.js"></SCRIPT>

<?php include(VIEWS_PATH."public/header.php");?>
<div class="container980">
  <!--左侧部分开始-->
  <div class="class_left">
  
  	    <!--正在竞拍开始-->
    <div class="left_box borFC" style="margin-bottom:10px;">

      <dl>
        <dt class="black"><strong><span class="font16"><?php echo $GLOBALS['cate'][$cateid]['catename'];?></span> 正在竞拍</strong></dt>
        <dd>
           <?php if($goods){foreach($goods as $v){$thumb=thumb($v['thumb']);?>        
          <div class="recommend ret02"  style="position:relative;" id="cat_<?php echo $v['goods_id'];?>">
		    <div id="bidtips_<?php echo $v['goods_id'];?>" style="display:none;position: absolute; top: 280px;background:#ff0000;"></div>
            <div class="name">
            <a href="<?php echo url('index','goods',array('id'=>$v['goods_id']));?>" target="_self" title="<?php echo $v['goods_name'];?>  第<?php echo $v['goods_id'];?>期"><?php echo $v['goods_name'];?> </a>
            <span class="grayB4">[第<?php echo $v['goods_id'];?>期]</span>

            </div>
            <div class="img borEB"> <a href="<?php echo url('index','goods',array('id'=>$v['goods_id']));?>" title="<?php echo $v['goods_name'];?>第<?php echo $v['goods_id'];?>期" target="_blank"> <img src="<?php echo $thumb[0];?>" width="150" height="126" alt="<?php echo $v['goods_name'];?>" /> </a> </div>
			<div class="countdown"><span class="green" name="lefttime" id="lasttime_<?php echo $v['goods_id'];?>">--:--:--</span></div>
            <div class="price"> <span class="gray62">当前价：</span><span class="yellow66 font16"><strong name="price">￥<span name="price" id="nowprice_<?php echo $v['goods_id'];?>">-.-</span></strong> </span><br />
			<span class="gray62">出价人：</span><span class="blue" name="username" id="currentuser_<?php echo $v['goods_id'];?>"><?php echo $v['currentuser'];?></span> </div>
			
            <div class="button">
			<?php if($v['button_status']=='ok'){?>
			<!--1.可以竞拍开始-->
			<div class="recbidbutton rcbid" id="bid_<?php echo $v['goods_id'];?>" onmouseout="this.className='recbidbutton'" onmousedown="this.className='recbidbuttondown';setInfo('<?php echo $v['goods_id'];?>');">
				<a style="cursor: pointer;" href=""></a>
			</div>
			<!--可以结束-->
			<?php } elseif($v['button_status']=='nologin'){?>
			<!--2.未登录开始-->
			<div id="bid_<?php echo $v['goods_id'];?>" class="recbidlogin rcbid">
			<a href="javascript:Login_Dialog();" style="cursor:pointer;"></a>
			</div>
			<!--未登录结束-->
			<?php } elseif($v['button_status']=='needsign'){?>
			<!--3.需要报名开始-->
			<div id="bid_<?php echo $v['goods_id'];?>" class="bidlogin bid">
			<a href="javascript:Login_Dialog();" style="cursor:pointer;"></a>
			</div>
			<!--需要报名结束-->	
			<?php } elseif($v['button_status']=='moneyerror'){?>
			<!--4.<?php echo $GLOBALS['setting']['site_money_name'];?>不足开始-->
			<div id="bid_<?php echo $v['goods_id'];?>" class="recbidcharge rcbid">
			<a href="javascript:Charge_Dialog();" style="cursor:pointer;"></a>
			</div>
			<!--<?php echo $GLOBALS['setting']['site_money_name'];?>不足结束-->
			<?php } elseif($v['button_status']=='needconfirm'){?>
			<!--5.手机未验证开始-->
			<div id="bid_<?php echo $v['goods_id'];?>" class="recbidphone rcbid">
			<a href="<?php echo url('user','mobile');?>" style="cursor:pointer;"></a>
			</div>
			<!--手机未验证结束-->
			<?php }?>
			
            </div>
          </div>
			<?php }}?>
    
                 
        </dd>
      </dl>
      <div class="public_corner public_topleft6"></div>
      <div class="public_corner public_topright6"></div>
      <div class="public_corner public_bottomleft6"></div>
      <div class="public_corner public_bottomright6"></div>
    </div>
    <!--正在竞拍结束-->
<?php if($goods){?>
<INPUT TYPE="hidden" id="ids" value="<?php echo implode('-',$ids);?>">
<INPUT TYPE="hidden" id="oids" value="<?php echo implode('-',$ids);?>">
<textarea style="display:none;" id="goods_list"><?php echo json_encode($goods_list);?></textarea>
<script language="JavaScript">
    <!--
      getInfo();
      window.onload=function(){checkcomplete();}
    //-->
</script>
<?php }?>	    
    
        <!--同类商品开始-->

    <div class="left_box01 borD6">
      <dl>
        <dt class="black"><strong><span class="font16"><?php echo $GLOBALS['cate'][$cateid]['catename'];?></span> 已拍过的商品</strong></dt>
        <dd>
		<?php if($complete){foreach($complete as $v){$thumb=thumb($v['thumb']);?>
		 <!--单个开始-->
		  <div class="rightimg_txt">
			<div class="leftimg borEB"> <a href="<?php echo url('index','complete',array('id'=>$v['goods_id']));?>" target="_blank" title="<?php echo $v['goods_name'];?>" ><img src="<?php echo $thumb[0];?>" alt="<?php echo $v['goods_name'];?>第<?php echo $v['goods_id'];?>期" width="90" height="76" /></a></div>

			<table>
			  <tr>
				<td><div class="leftname"> <a href="<?php echo url('index','complete',array('id'=>$v['goods_id']));?>" target="_blank" title="<?php echo $v['goods_name'];?> 第<?php echo $v['goods_id'];?>期" > <strong><?php echo $v['goods_name'];?></strong> </a> <span style="color:gray;">[第<?php echo $v['goods_id'];?>期]</span> </div>
				  <div class="leftprice"> <span class="gray62"> 市场价：<span class="line-through">￥<?php echo $v['marketprice'];?></span> </span> </div>

				  <div class="left">第<?php echo $v['goods_id'];?>期&nbsp;</div>
				  <img src="<?php echo UC_API."/avatar.php?uid=".$v['currentuid']."&size=middle&type=virtual";?>" width="13" height="13" class="index_avatar" alt="" />
				  <div class="index_username"> <span class="blue"><?php echo $v['currentuser'];?></span>&nbsp;以 <strong class="yellow66"><?php echo $v['nowprice'];?>元</strong> 赢得该商品 节省了 <strong class="yellow66"><?php echo $v['marketprice']-$v['nowprice'];?></strong> 。</div></td>
			  </tr>

			</table>
			<div class="button"> 
								<a href="<?php echo url('index','complete',array('id'=>$v['goods_id']));?>" target="_blank">
					<img src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/category/detail.gif" onmouseout="this.src='<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/category/detail.gif';" onmouseover="this.src='<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/category/detail_over.gif';"  alt="查看详情" style="margin-top:25px;"/>
				</a>
							</div>
		  </div>
		  <!--单个结束-->
		  <?php }}?>
		  </dd>
      </dl>
      <div class="public_corner public_topleft2"></div>
      <div class="public_corner public_topright2"></div>
      <div class="public_corner public_bottomleft"></div>
      <div class="public_corner public_bottomright"></div>
    </div>
    <!--同类商品结束-->
  </div>
  <!--左侧部分结束-->
  <!--右侧部分开始-->
  <div class="class_right">

  <div class="class_right">
			<a href="<?php echo url('user','register');?>"><img src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/head/register.gif" alt="注册即送500<?php echo $GLOBALS['setting']['site_money_name'];?>" /></a>
		</div>

        <!--本区获胜者开始-->
    <div class="right_box borD6 mar10">
      <dl>
        <dt class="black"><strong>本区获胜者</strong></dt>
        <dd class="tbpad6">

          <ul class="xiyou_do">
			<?php if($win){foreach($win as $v){?>
			<li> <img src="<?php echo UC_API."/avatar.php?uid=".$v['currentuid']."&size=middle&type=virtual";?>" width="13" height="13" class="index_avatar" />
			<div class="index_username"> <span class="blue"><?php echo $v['currentuser'];?>：</span>以&nbsp;<span class="yellow66 font14"><strong>￥<?php echo $v['nowprice'];?></strong></span>&nbsp;
			获得<a href="<?php echo url('index','complete',array('id'=>$v['goods_id']));?>" title="<?php echo $v['goods_name'];?>[第<?php echo $v['goods_id'];?>期]"><?php echo $v['goods_name'];?></a><span class="grayB4">[第<?php echo $v['goods_id'];?>期]</span> </div>

			</li>
			<?php }}?>
          </ul>
        </dd>
      </dl>
      <div class="public_corner public_topleft2"></div>
      <div class="public_corner public_topright2"></div>
      <div class="public_corner public_bottomleft"></div>
      <div class="public_corner public_bottomright"></div>
    </div>

    <!--本区获胜者结束-->
        
        <!--拍友晒单开始-->
    <div class="right_box borD6 mar10">
      <dl>
        <dt class="black">
        	<strong class="left">拍友晒单</strong>
        </dt>
        <dd>
		<?php if($GLOBALS['show']){foreach($GLOBALS['show'] as $k=>$v){?>
   		  
          <div class="leftimg_txt">
            <div class="leftimg borEB"><a href="<?php echo url('index','showinfo',array('id'=>$v['id']));?>" title="<?php echo $v['title'];?>" target="_blank"><img src="<?php echo $v['thumb'];?>" alt="<?php echo $v['goods_name'];?>" width="90" height="76" /></a></div>
            <table class="txt">
              <tr>
                <td><span class="leftname"><a href="<?php echo url('index','showinfo',array('id'=>$v['id']));?>" title="<?php echo $v['title'];?>" target="_blank">
			<?php echo $v['title'];?></a> </span> <span class="leftprice gray62">成交价：￥<?php echo $v['orderinfo']['goods_info']['nowprice'];?></span> <img src="<?php echo UC_API."/avatar.php?uid=".$v['uid']."&size=middle&type=virtual";?>" width="13" height="13" class="index_avatar" />
                  <div class="index_username blue"><?php echo $v['username'];?></div></td>

              </tr>
            </table>
          </div>
 
         <?php }}?>         
        </dd>
      </dl>
      <div class="public_corner public_topleft2"></div>
      <div class="public_corner public_topright2"></div>

      <div class="public_corner public_bottomleft"></div>
      <div class="public_corner public_bottomright"></div>
    </div>
    <!--拍友晒单结束-->
        
	<div class="right_box borD6 mar10">
	<dl>
			<dt class="black"><strong>常见问题</strong></dt>
			<dd>

			<ul class="problem">
				<li><a href="<?php echo url('article','help',array('id'=>5));?>" target="_blank">如何注册会员？</a></li>
				<li><a href="<?php echo url('article','help',array('id'=>12));?>" target="_blank">如何购买<?php echo $GLOBALS['setting']['site_money_name'];?>参与竞拍？</a></li>
				<li><a href="<?php echo url('article','help',array('id'=>13));?>" target="_blank">如何查看我订单状态？</a></li>

				<li><a href="<?php echo url('article','help',array('id'=>21));?>" target="_blank">如何获得免费<?php echo $GLOBALS['setting']['site_money_name'];?>？</a></li>
			</ul>
			</dd>

	  </dl>
		<div class="public_corner public_topleft2"></div>
		<div class="public_corner public_topright2"></div>
		<div class="public_corner public_bottomleft"></div>
		<div class="public_corner public_bottomright"></div>
	</div>
  </div>
  <!--右侧部分结束-->
  <div class="clear"></div>

</div>
<SCRIPT LANGUAGE="JavaScript">
<!--
	show_alert();
//-->
</SCRIPT>
<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->