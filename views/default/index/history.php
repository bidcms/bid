<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/bidlog.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/imgscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>


<?php include(VIEWS_PATH."public/header.php");?>
<div class="container980">
  <table align="center" class="bidlog">
    <thead>
      <tr>
        <th>商品缩略图</th>

        <th>商品名称</th>
       
        <th>市场价</th>
        <th>成交价</th>
        <th>开始时间</th>
        <th>获胜者</th>
        <th>成交时间</th>

      </tr>
    </thead>
    <tbody class="txtcenter">
	<?php if($goods){foreach($goods as $k=>$v){
	   $thumb=thumb($v['thumb']);
	   ?>  
         <tr>
        <td>
		<a href="<?php echo url('index','complete',array('id'=>$v['goods_id']));?>" title="<?php echo $v['goods_name'];?>" target="_blank">
        <img  width="90" height="76" src="<?php echo $thumb[0];?>" alt="<?php echo $v['goods_name'];?>[已竞拍]"></a>
		</td>
        <td class="txtleft" width="240"><a href="<?php echo url('index','complete',array('id'=>$v['goods_id']));?>" title="<?php echo $v['goods_name'];?>" target="_blank"><b><?php echo $v['goods_name'];?>[已竞拍]</b></a></td>
        
        <td class="txtright">￥<?php echo $v['marketprice'];?></td>
        <td class="red txtright">￥<?php echo $v['nowprice'];?></td>

        <td><?php echo date('Y-m-d',$v['starttime']);?></td>
        <td><strong><?php echo $v['currentuser'];?></strong></td>
        <td><?php echo date('Y-m-d H:i:s',$v['lasttime']);?></td>
      </tr>
	 <?php }}?>
          </tbody>
    <tfoot>
      <tr>
        <td align="center" colspan="12"><?php echo $pageinfo;?></td>

      </tr>
    </tfoot>
  </table>
</div>

<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->