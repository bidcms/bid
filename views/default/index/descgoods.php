<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/descgoods.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>

<?php include(VIEWS_PATH."public/header.php");?>

<div class="container980 mar10 border3"> <a name="bid"></a> 
<span class="left pidname"><?php echo $info['goods_name'];?></span>
  <div class="clear"></div>
</div>
<?php $thumb=thumb($info['thumb']);?>
<!-- 已有往期竞拍开始 -->
<div class="showsheetlist mar10">
  <!--商品图片开始-->
  <div class="bid_left borD6">
    <div class="bid_leftmain">
    <div class="bid_img">
      <div class="bid_bigimg">
      		<img id="pbigimg" src="<?php echo $thumb[0];?>" alt="<?php echo $info['goods_name'];?>" height="308" width="366" >
      </div>
      <ul class="thumb">
	<?php foreach($thumb as $val){?>
     <li class="borD6_img"><img src="<?php echo $val;?>"  width="63" height="53" onclick="$('#pbigimg').attr('src',this.src);"/></li>
	 <?php }?>
		      </ul>
    </div>
	</div>
    <div class="public_corner public_topleft2"></div>
    <div class="public_corner public_topright2"></div>
    <div class="public_corner public_bottomleft"></div>

    <div class="public_corner public_bottomright"></div>
  </div>
  <!--商品图片结束-->
  <div class="bid_center">
    <div style="border:1px solid #ffd149;background:#fffcd9;">
    <div class="bid_cy1 left">
      <div class="jiao2"></div>
      <div class="cy1_top font24b">
      <?php echo $info['nowprice'];?>元
      </div>
      <div class="cy1_bottom">
        <span class="font12b marr5"><?php echo $info['currentuser'];?></span><a href="<?php echo url('index','complete',array('id'=>$info['goods_id']));?>"  title="第<?php echo $info['goods_id'];?>期">在第<?php echo $info['goods_id'];?>期赢得</a>
      </div>
    </div>
    <div class="bid_cy2 left">
      <!-- 没有正在竞拍中的 -->      
      <div class="left">
        <div class="bid_cy2font">市场价：<span class="overline1 bid_cy2fontprice"><?php echo $info['marketprice'];?></span>元
        </div>

      </div>
          <div class="cy_corner cy_topright"></div>
    <div class="cy_corner cy_bottomright"></div>
    </div>
      <div class="clear"></div>
    </div>
    <ul class="simple grayB4">
	<?php if($showlist[0]){foreach($showlist[0] as $oldgoods){?>
      <li>在<a href="<?php echo url('index','complete',array('id'=>$oldgoods['goods_id']));?>" title="第<?php echo $oldgoods['goods_id'];?>期"><span class="blue1">第<?php echo $oldgoods['goods_id'];?>期</span></a><div><img src="<?php echo UC_API."/avatar.php?uid=".$oldgoods['currentuid']."&size=middle&type=virtual";?>" /></div>

     <?php echo $oldgoods['currentuser'];?>&nbsp;以<span class="red_b font16"> <?php echo $oldgoods['nowprice'];?>元</span>&nbsp;赢得该商品&nbsp;节省了<span class="yellow66"><?php echo $oldgoods['marketprice']-$oldgoods['nowprice'];?></span></li>
	 <?php }}?>
    </ul>
  </div>
  <div class="clear"></div>
</div><!-- 已有往期竞拍结束 -->

<!-- 新品结束 -->
<div class="clear"></div>
</div>
</div>
</div>
<div class="details mar10 borD6">
  <dl>
    <dt class="font14b">该商品晒单列表</dt>
    <dd class="paddingt0">
	<?php if($showlist[1]){foreach($showlist[1] as $goodshow){?>
      <!--单个晒单开始-->
      <div class="single">

        <div class="single_left">
          <div class="qihao font14">
          <a href="<?php echo url('index','complete',array('id'=>$goodshow['orderinfo']['goods_id']));?>" title="第<?php echo $goodshow['orderinfo']['goods_id'];?>期">第<?php echo $goodshow['orderinfo']['goods_id'];?>期</a>
          </div>
          <div class="price font22b">
          <span class="font26"><?php echo $goodshow['orderinfo']['goods_info']['nowprice'];?></span>元
          </div>
          <ul class="grey190 grayB4 mar10">

            <li>市&nbsp;场&nbsp;价：<span class="overline1"><?php echo $goodshow['orderinfo']['goods_info']['marketprice'];?></span>元</li>
            <li>节&nbsp;省&nbsp;了：<?php echo $goodshow['orderinfo']['goods_info']['marketprice']-$goodshow['orderinfo']['goods_info']['nowprice'];?></li>
            <li>结束时间：<?php echo date('Y-m-d H:i:s',$goodshow['orderinfo']['goods_info']['lasttime']);?></li>
            <li class="mar10"><input type="button" class="jing font333 font12" value="竞拍详情" onclick="window.location.href='<?php echo url('index','complete',array('id'=>$goodshow['orderinfo']['goods_info']['goods_id']));?>'"/></li>

          </ul>
        </div>
        <div class="single_right borD6">
          <div class="jiao"></div>
          <div class="single_rtop">
          <div class="single_rtleft">
            <div class="single_rtl1">            </div>
            <div class="single_rtl2">

            <img src="<?php echo UC_API."/avatar.php?uid=".$goodshow['uid']."&size=middle&type=virtual";?>" />            </div>
          </div>
          <div class="single_rtcenter">
            <div class="name">
              <span class="blue_b font14 marr5 left"><?php echo $goodshow['username'];?></span>
              <span class="grayB4 left">竞拍赢得</span>
                          </div>

            <div class="clear"></div>
            <div class="time9 grayB4">
              <span class="marr5">晒单于：<?php echo date('Y-m-d H:i:s',$goodshow['updatetime']);?></span>
              <span class="marr5">奖励：<?php echo $goodshow['givemoney'];?></span>
              <span class="marr5">回复<span class="yellow66"><?php echo $goodshow['comments'];?></span></span>
              <span class="marr5">浏览<span class="yellow66"><?php echo $goodshow['hits'];?></span></span>

            </div>
          </div>
          <div class="clear"></div>
          </div>
          <div class="prolist mar10">
            <strong class="font14"><a href="<?php echo url('index','showinfo',array('id'=>$goodshow['id']));?>" title="<?php echo $goodshow['title'];?>"><?php echo $goodshow['title'];?></a></strong>
            <p class="prolist"><?php echo strip_tags($goodshow['content']);?><a href="<?php echo url('index','showinfo',array('id'=>$goodshow['id']));?>" title="晒单详细"><span class="blue">晒单详细</span></a></p>

          </div>
          <ul class="imglist">
		  <?php $pic=getUploadPic($goodshow['content'],6);
			foreach($pic as $key=>$val){
			?>
            <li><a href="<?php echo url('index','showinfo',array('id'=>$goodshow['id']));?>"><img src="<?php echo $val;?>" width="92" height="76" /></a></li>
			<?php }?>
          </ul>

        <div class="public_corner public_topleft2"></div>
        <div class="public_corner public_topright2"></div>
        <div class="public_corner public_bottomleft"></div>
        <div class="public_corner public_bottomright"></div>
        </div>
        <div class="clear"></div>
      </div>
      <!--单个晒单结束-->
      <?php }}?>   
    </dd>

  </dl>
  <div class="public_corner public_topleft2"></div>
  <div class="public_corner public_topright2"></div>
  <div class="public_corner public_bottomleft"></div>
  <div class="public_corner public_bottomright"></div>
  <div class="clear"></div>
</div>
<div class="details mar10 borD6">
  <dl>
    <dt class="font14b">商品详细介绍</dt>

    <dd><?php echo $details['content'];?></dd>
  </dl>
  <div class="public_corner public_topleft2"></div>

  <div class="public_corner public_topright2"></div>
  <div class="public_corner public_bottomleft"></div>
  <div class="public_corner public_bottomright"></div>
  <div class="clear"></div>
</div>

<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->