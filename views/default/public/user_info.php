<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
?>
<div class="mem_info_index borD6">
  <div class="avatar_index"><img src="<?php echo "/tools/avatar.php?uid=".$GLOBALS['userinfo']['uid']."&size=middle&type=virtual";?>" width="70" height="70" /></div>

  <div class="txt_info">
    <p class="mem_t1"> <strong class="font16"><?php echo $GLOBALS['userinfo']['username'];?></strong> <span class="blue">[<a href="<?php echo url('user','intro');?>">个人信息</a>]</span>
           <?php if($GLOBALS['userinfo']['verification']){?> <span class="mem_t3"><img src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/newmember/tell.gif" class="img2bottom" alt="已验证" /></span> <span class="blue grayB4">[已验证]</span><?php } else{?><A HREF="<?php echo url('user','mobile');?>">未验证</A><?php }?>
          </p>
        <p class="mem_t22 grayB4"> 上次登录:<?php echo date('Y-m-d H:i:s',$GLOBALS['userinfo']['lastlogin']);?> IP:<?php echo $GLOBALS['userinfo']['lastip'];?></p>
    <div class="mem_t22">
    <div class="left"> 新站内信：<strong class="yellow66"><?php echo $GLOBALS['userinfo']['pms']>0?$GLOBALS['userinfo']['pms']:0;?>封</strong> 
      <span class="blue">[<a href="<?php echo url('user','message');?>">查看</a>]</span> 
      <span class="mem_t3">共参与竞拍：</span><strong class="yellow66"><?php echo $GLOBALS['userinfo']['countbuy'];?>次</strong> 
      <span class="mem_t3">剩余积分：</span><strong class="yellow66"><?php echo $GLOBALS['userinfo']['score'];?></strong> 
      <span class="mem_t3">剩余<?php echo $GLOBALS['setting']['site_money_name'];?>：</span><strong class="yellow66"><?php echo $GLOBALS['userinfo']['money'];?></strong> 
      <span class="blue">[<a href="<?php echo url('user','charge');?>">购买<?php echo $GLOBALS['setting']['site_money_name'];?></a>]</span>
    </div>

    </div>
    <div class="txt_img" style="top:16px;width:150px;"><span class="blue">[<a href="<?php echo url('user','recommend');?>">推荐好友送<?php echo $GLOBALS['setting']['site_money_name'];?></a>]</span></div>
  </div>
  <div class="public_corner public_topleft2"></div>
  <div class="public_corner public_topright2"></div>
  <div class="public_corner public_bottomleft"></div>
  <div class="public_corner public_bottomright"></div>
</div>