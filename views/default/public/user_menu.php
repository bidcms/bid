<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
?>
<div class="mem_left">
  <div class="mem_nav borD6">
    <dl>
      <dt> <span class="bg01"><span class="bg03"></span><strong><a href="<?php echo url('user');?>" >个人管理中心</a></strong></span> </dt>

      <dd>
        <div class="mem_main_nav">
          <div class="bg02 left"> <span class="left lmar10">我的竞拍</span> </div>
        </div>
        <ul class="mem_child_nav">
          <li class=""><a href="<?php echo url('user');?>" >我参与的</a></li>
          <li class=""> <a href="<?php echo url('user','gain');?>"> 竞拍成功</a> </li>

          <li class=""><a href="<?php echo url('order','list');?>"><span>我的订单</span></a></li>
		  <li class=""><a href="<?php echo url('order','list',array('type'=>1));?>"><span>积分换购</span></a></li>
		  <li class=""><a href="<?php echo url('order','show');?>"><span>我的晒单</span></a></li>
        </ul>
        <div class="mem_main_nav">
          <div class="bg02 left"> <span class="left lmar10">我的商品</span> </div>
        </div>

        <ul class="mem_child_nav">
          <li class=""><a href="<?php echo url('user','goodsmodify');?>">发布商品</a></li>
          <li class=""><a href="<?php echo url('user','goods');?>">正在竞拍</a></li>
		  <li class=""><a href="<?php echo url('user','complete');?>">竞拍结束</a></li>
        </ul>
        <div class="mem_main_nav">
          <div class="bg02 left"> <span class="left lmar10">我的账户</span> </div>

        </div>
        <ul class="mem_child_nav">
          <li class=""><a href="<?php echo url('user','charge');?>">购买<?php echo $GLOBALS['setting']['site_money_name'];?></a></li>
		  <li class=""><a href="<?php echo url('user','card');?>">代金券验证</a></li>
          <li class=""><a href="<?php echo url('user','chargelist');?>">资金记录</a></li>
		  <li class=""><a href="<?php echo url('user','bidlist');?>">竞拍记录</a></li>
		  <li><a href="<?php echo url('user','recommend');?>">邀请好友</a></li>
        </ul>
        <div class="mem_main_nav">
          <div class="bg02 left"> <span class="left lmar10">我的信息</span> </div>

        </div>
        <ul class="mem_child_nav">
		  <li class=""><a href="<?php echo url('user','address');?>">收货地址</a></li>
          <li class=""><a href="<?php echo url('user','intro');?>">修改个人信息</a></li>
		  <li class=""><a href="<?php echo url('user','feedback');?>">我的意见反馈</a></li>
		  <li class=""><a href="<?php echo url('user','sms');?>">我的短信订阅</a></li>
          <li class=""><a href="<?php echo url('user','message');?>">站内信</a></li>

        </ul>
      </dd>
    </dl>
    <div class="public_corner public_topleft2"></div>
    <div class="public_corner public_topright2"></div>
    <div class="public_corner public_bottomleft"></div>
    <div class="public_corner public_bottomright"></div>
  </div>
</div>