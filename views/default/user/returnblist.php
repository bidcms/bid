<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/newmember.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>
<?php include(VIEWS_PATH."public/header.php");?>

<div class="container980">

<?php include(VIEWS_PATH."public/user_menu.php");?>
<div class="mem_right">
    	
<?php include(VIEWS_PATH."public/user_info.php");?>
<ul class="bidinfo_nav mar10">
	<li class="other"><a href="<?php echo url('user','chargelist');?>" ><span>资金变动</span></a></li>
	<li class="other"><a href="<?php echo url('user','bidlist');?>" ><span>竞拍记录</span></a></li>
	<li class="thisclass"><a href="<?php echo url('user','returnblist');?>" ><span>竞拍返币记录</span></a></li>
	</ul>

	<div class="mem_box bpad10">
	<div class="mem_boxx">
	<div class="bidinfo_nav_txt01">
		<span class="s_w3 txtcenter lmar10">竞拍期数</span>
		<span class="s_w3 txtcenter">使用<?php echo $GLOBALS['setting']['site_money_name'];?></span>
		<span class="s_w3 txtcenter">返币比例</span>
		<span class="s_w3 txtcenter"><?php echo $GLOBALS['setting']['site_money_name'];?>数量</span>
		<span class="s_w1 txtcenter">日期</span>
		
	</div>
	<?php if($returnlog){foreach($returnlog as $key=>$val){?>
		
<div class="mag_main">
		<span class="s_w3 txtcenter"><?php echo $val['gid'];?></span>
		<span class="s_w3 txtcenter"><?php echo $val['money'];?></span>
		<span class="s_w3 txtcenter"><?php echo $val['backmoney'];?>%</span>
		<span class="s_w3 txtcenter"><strong class="yellow66 imgie6"><?php echo $val['resultmoney'];?></strong></span>
		<span class="s_w1 txtcenter"><?php echo date('Y-m-d H:i:s',$val['updatetime']);?></span>
	</div>
<?php }}?>
		
	<div class="pages" >
      <table align="center" class="mar10"  >
        <tbody>
          <tr>
            <td>
			<div class="list_page"><?php echo $pageinfo;?></div>

              </td>
          </tr>
        </tbody>
      </table>
    </div></div>
	<div class="public_corner public_bottomleft"></div>
	<div class="public_corner public_bottomright"></div>
	</div>
	
	</div>

	<div class="clear"></div>
</div>
<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->