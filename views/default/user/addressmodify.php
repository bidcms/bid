<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/newmember.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/pcasunzip.js"></SCRIPT>
<?php include(VIEWS_PATH."public/header.php");?>

<div class="container980">

<?php include(VIEWS_PATH."public/user_menu.php");?>
<div class="mem_right">
    	
<?php include(VIEWS_PATH."public/user_info.php");?>

<ul class="bidinfo_nav mar10">
	<li class="other"><a href="<?php echo url('user','address');?>" ><span>收货地址</span></a></li>
    <li class="thisclass"><a href="<?php echo url('user','addressmodify');?>" ><span>添加地址</span></a></li>
	</ul>
	
	<div class="editdatil mem_borD6">

	  <form name="account" method="post" action="<?php echo SITE_ROOT;?>/index.php">
	  <INPUT TYPE="hidden" NAME="commit" value="1"><INPUT TYPE="hidden" NAME="con" value="user"><INPUT TYPE="hidden" NAME="act" value="addressmodify">
		 <div id="message"><span class="message red"></span></div>
		 <fieldset class="fieldset">
			<INPUT TYPE="hidden" NAME="commit" value="1"><INPUT TYPE="hidden" NAME="updateid" value="<?php echo $address['id'];?>">
			<input type="hidden" name="to" value="<?php echo $referer;?>">
			<p class="p">
				  <label class="left">收&nbsp;货&nbsp;人：</label>
				  <input type="text" name="username" maxlength="10" value="<?php echo $address['username'];?>" class="input">

			</p>
			<p class="p">
				  <label class="left">联系电话：</label>
				  <input type="text" name="telphone" value="<?php echo $address['telphone'];?>" maxlength="32"  class="input">

			</p>
			<p class="p" style="height:100px;">
				  <label class="left">收货地址：</label>
				 <textarea name="address" class="input" style="height:100px;width:200px;"><?php echo $address['address'];?></textarea>

			</p>
			
            <span class="mem_t5 left">
              <input type="submit" value="提交修改" id="btn_submit" class="mem_button01" />

              <input type="reset" value="重 置" id="btn_reset" class="mem_button02 lmar10" />
            </span>
           </fieldset>
		</form>	
			<div class="public_corner public_bottomleft"></div>
			<div class="public_corner public_bottomright"></div>
	</div>
	
	</div>
<div class="clear"></div>
</div>

<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->