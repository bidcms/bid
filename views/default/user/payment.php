<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/newmember.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>
<?php include(VIEWS_PATH."public/header.php");?>

<div class="container980">

<?php include(VIEWS_PATH."public/user_menu.php");?>
<div class="mem_right">
    	
<?php include(VIEWS_PATH."public/user_info.php");?>
<FORM METHOD="POST" ACTION="<?php echo SITE_ROOT;?>/index.php">
<div class="mem_charge mar10 borFC" style="z-index:3">
      <dl>
        <dt><strong>购买<?php echo $GLOBALS['setting']['site_money_name'];?></strong></dt>
		
          <dd style="position:relative;"> <div class="aboutxibi"><?php echo $GLOBALS['setting']['site_money_name'];?>是商品竟拍和竞猜游戏的道具，仅供游戏娱乐使用；<br />

            <?php echo $GLOBALS['setting']['site_money_name'];?>不能够代替法定货币，不能兑换商品，不能替换运费，一旦售出，不能兑换法定货币。
	</div>
	<DIV ID="" CLASS="">
		
		<INPUT TYPE="hidden" NAME="commit" value="1"><INPUT TYPE="hidden" NAME="con" value="user"><INPUT TYPE="hidden" NAME="act" value="pay2">
		<p class="p">
              <label class="left">付款方式：</label>
              <span class="span"> <SELECT NAME="payment">
			<OPTION VALUE="1" SELECTED>信用卡、Master、VISA、美國運通</option>
			<OPTION VALUE="2">遠傳 / 中華 / 台灣大哥大 / 亞太</option>
			<OPTION VALUE="3">ATM轉帳 / WebATM / 臨櫃匯款</option>
			<OPTION VALUE="4">郵局匯款 / 現金袋</option>
		</SELECT> </span> </p>
		<p class="p">
              <label class="left">匯款人姓名:</label>
              <span class="span"><INPUT TYPE="text" NAME="payuser" class="inputaddress" > </span> </p>
		<p class="p">
              <label class="left">付款時間:</label>
              <span class="span"><INPUT TYPE="text" NAME="paytime" class="inputaddress" > </span> </p>
		<p class="p">
              <label class="left">付款金額:</label>
              <span class="span"><INPUT TYPE="text" NAME="paymoney" class="inputaddress" > </span> </p>
		<p class="p">
              <label class="left">付款人帳號:</label>
              <span class="span"><INPUT TYPE="text" NAME="paycard" class="inputaddress" > </span> </p>
		<p class="p">
              <label class="left">付款項目:</label>
              <span class="span">
		<SELECT NAME="paytype">
			<OPTION VALUE="1" SELECTED>儲值</option>
			<OPTION VALUE="2">支付商品</option>
		</SELECT> </span> </p>
		<p class="p">
              <label class="left">聯繫電話:</label>
              <span class="span"><INPUT TYPE="text" NAME="tel" class="inputaddress" > </span> </p>
		<p class="p">
              <label class="left">郵件:</label>
              <span class="span"><INPUT TYPE="text" NAME="email" class="inputaddress" > </span> </p>
		<p class="p">
              <label class="left">备注:</label>
              <span class="span"><TEXTAREA NAME="content" ROWS="" COLS="" class="inputaddress" ></TEXTAREA> </span> </p>
		
	</DIV>

            <span class="txt mar10">
            <input type="submit" id="btnSubmit" value="提交"  class="mem_button03 mem_t8" onmousemove="this.className='mem_button03_over mem_t8'" onmouseout="this.className='mem_button03 mem_t8'" />
            </span> <span class="charge_txt"> 亲爱的用户，为了顺利完成购买，请您务必注意：<br />
            1、为了保障您的权益，请确认您要购买<?php echo $GLOBALS['setting']['site_money_name'];?>的帐号，一旦购买成功，系统将不提供购买修正服务。<br />
            2、如有任何无法解决的问题请拨打免费客服电话：<?php echo $GLOBALS['setting']['site_tel'];?></span> </dd>
      </dl>
      <div class="public_corner public_topleft6"></div>
      <div class="public_corner public_topright6"></div>
      <div class="public_corner public_bottomleft6"></div>
      <div class="public_corner public_bottomright6"></div>
    </div>
  </div>
  </FORM>
  <div class="clear"></div>

</div>
<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->