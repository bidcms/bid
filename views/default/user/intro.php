<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css">
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/newmember.css" rel="stylesheet" type="text/css" />

<script src="//apps.bdimg.com/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//apps.bdimg.com/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/pcasunzip.js"></SCRIPT>
<?php include(VIEWS_PATH."public/header.php");?>
<SCRIPT LANGUAGE="JavaScript">
<!--



//是否全是数字
function is_num(numStr)
{
	var reg = new RegExp("^[0-9]+$");
	return reg.test(numStr);
}
//是否是数值，包含小数也是
function is_number(str)
{
    return !isNaN(str);
}

//是否是邮编
function is_zip(numStr)
{
	var reg = new RegExp("^[0-9]{6}$");
	return reg.test(numStr);
}
//是否为身份证
function is_idcard(numStr)
{
	var reg = new RegExp("^[0-9]{17}(x|X|[0-9]){1}$");
	return reg.test(numStr);
}
//是否是手机
function is_mobile(numstr)
{
	var reg = new RegExp("^1[0-9]{10}$");
	return reg.test(numstr);
}
//只允许数字，字母，下划线
function is_alnum(str)
{
	var reg = new RegExp("^[A-Za-z0-9_]+$");
	return reg.test(str);
}
//省市区连动
var province 	= "<?php echo $userintro['province'];?>";
var city 		= "<?php echo $userintro['city'];?>";
var area 		= "<?php echo $userintro['area'];?>";

window.onload=function(){
//省市区连动
new PCAS("sel_province","sel_city","sel_area",province,city,area);
}
//提交表单
function checksubmit(){
	//获取对象元素
	var $realname = $("#txt_realname");
	var $province = $("#sel_province");
	var $city = $("#sel_city");
	var $area = $("#sel_area");
	var $address = $("#txt_address");
	var $idcard = $("#txt_idcard");

	//错误提示框
	var $msg = $("#message > span");

	if($.trim($realname.val()) == ""){
		$msg.text("请填写真实姓名");
		$realname.focus();
		return false;
	}
	
	if($realname.val().length > 15){
		$msg.text("真实姓名不得超过15个字符");
		$realname.focus();
		return false;
	}

	if($.trim($province.val()) == ""){
		$msg.text("请选择省份");
		return false;
	}

	if($.trim($city.val()) == ""){
		$msg.text("请选择市");
		return false;
	}

	if($.trim($area.val()) == ""){
		$msg.text("请选择区域");
		return false;
	}

	if($.trim($address.val()) == ""){
		$msg.text("请填写地址");
		$address.focus();
		return false;
	}
	
	if($address.val().length > 255){
		$msg.text("地址长度不得超过255字符");
		$address.focus();
		return false;
	}


	if($.trim($idcard.val()) == ""){
		$msg.text("身份证不能为空");
		$idcard.focus();
		return false;
	}

	if(!is_idcard($idcard.val())){
		$msg.text("身份证只能填写18位数字,最后一位可以是x");
		$idcard.focus();
		return false;
	}
}
function uploadavatar(){
	if($('#edit_avatar_div').is(":hidden")){
		$('#edit_avatar_div').show();
	}else{
		$('#edit_avatar_div').hide();
	}
}
function cancelavatar(){
		$('#edit_avatar_div').hide();
};
//上传头像
function upload_result()
{
  	<?php if(isset($GLOBALS["setting"]["avatar_give"]) && $GLOBALS["setting"]["avatar_give"]>0){?>
	$('#dialog').html('上传头像成功，奖励<?php echo $GLOBALS["setting"]["site_money_name"];?>:<?php echo $GLOBALS["setting"]["avatar_give"];?>');
    <?php } else {?>
    $('#dialog').html('上传头像成功');
    <?php }?>
	$('#blurdiv').show();
	$('#blurdiv').height($(document).height());
	$('#dialog').dialog({
		title:'上传头像成功',
		width: 350,
		height:200,
		buttons: {},
		close:function(){$('#blurdiv').hide();$('#dialog').html('');}
	});

}
function updateavatar () {
    var data = new FormData();
    //Append files infos
    jQuery.each($(".file")[0].files, function(i, file) {
      data.append('file-'+i, file);
    });

    $.ajax({  
      url: "?con=user&act=avatar",  
      type: "POST",  
      data: data,  
      cache: false,
      processData: false,  
      contentType: false, 
      context: this,
      success: function (res) {
        upload_result(res.data.name);
      }
    });
}

//-->
</SCRIPT>
<div class="container980">

<?php include(VIEWS_PATH."public/user_menu.php");?>
<div class="mem_right">
    	
<?php include(VIEWS_PATH."public/user_info.php");?>

<ul class="bidinfo_nav mar10">
	<li class="thisclass"><a href="<?php echo url('user','intro');?>" ><span>修改个人信息</span></a></li>
	<li class="other"><a href="<?php echo url('user','password');?>"><span>修改密码</span></a></li>
	</ul>
	
	<div class="editdatil mem_borD6">
      <?php if(!empty($GLOBALS['setting']['avatar_give'])){?>
      <div class="jlxb"><span class="jlxbimg"><img src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/newmember/daka_03.gif" /></span><strong>上传头像奖励<?php echo $GLOBALS['setting']['avatar_give'];?><?php echo $GLOBALS['setting']['site_money_name'];?></strong></div>
      <?php }?>            
      <div>
        <div class="avatar_txt">我的头像：</div>
        <div class="avatar_index01">
          <img id="avatar_img_filepath" src="<?php echo "/tools/avatar.php?uid=".$userintro['uid']."&size=middle&type=virtual";?>" width="70" height="70" />
        </div>
        <div class="avatar_edit">
          <img src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/newmember/i_03.gif" alt="更换头像" name="imgHead" width="63" height="27" class="t1" id="imgHead"  onclick="uploadavatar();"/>
        </div>
        <div id="edit_avatar_div" style="clear:both;display:none;padding-left:130px;">
              <input type="file" class="file" name="favatar"/><button type="button" onclick="updateavatar();">上传</button>
              <img src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/newmember/you_c.gif" onclick="cancelavatar();"  multiple="multiple"/>
		</div>
      </div>
	  <form name="form1" action="<?php echo SITE_ROOT;?>/index.php" method="post"  onsubmit="return checksubmit();"  enctype="multipart/form-data">
	     <INPUT TYPE="hidden" NAME="commit" value="1"><INPUT TYPE="hidden" NAME="con" value="user"><INPUT TYPE="hidden" NAME="act" value="intro">
		 <div id="message"><span class="message red"></span></div>
		 
		   
           <p class="p">
              <label class="left">会&nbsp;员&nbsp;名：</label>
              <span class="span"> <?php echo $userintro['username'];?> </span> 
           </p>

           <p class="p">
             <label class="left">手&nbsp;机&nbsp;号：</label>
              <span class="span" id="newmobile"><?php echo $userintro['mobile'];?></span>
              <span class="green span"><?php echo $userintro['verification']?'已验证':'未验证';?></span>
              <span class="redlink span"><a href="javascript:ChangeMobile_Dialog($('#newmobile').text());" >[修改手机号]</a></span>
           </p>

           <p class="p">
              <label class="left">电子邮箱：</label>
              <span class="span"> <?php echo $userintro['email'];?> </span> </p>
             <p class="p">
              <label class="left">真实姓名：</label>
              <input type="text" name="txt_realname" id="txt_realname"　maxlength="15" value="<?php echo $userintro['realname'];?>"  class="input" />

            </p>
            <p class="p"><label class="left">性　　别：</label>
             <span class="in">
              <label for="rdo_male" style="width:41px; ">
              <input type="radio" name="rdo_sex" id="rdo_male" value="0"  <?php echo $userintro['sex']<1?'checked':'';?>/>
              男</label>
              <label for="rdo_female" style="width:41px;">

              <input type="radio" name="rdo_sex" id="rdo_female" value="1"  <?php echo $userintro['sex']==1?'checked':'';?>  />
              女</label>
			  </span>             
           </p>
             <p class="p">
             <label class="left">地　　区：</label>
              <span class="in">
              <select name="sel_province" id="sel_province" >

              </select>
              <select name="sel_city" id="sel_city"  >
              </select>
              <select name="sel_area" id="sel_area" >
              </select>
              </span> </p>
            <p class="p">
              <label class="left">地　　址：</label>

              <input type="text" name="txt_address" id="txt_address"　maxlength="255" value="<?php echo $userintro['cusaddress'];?>" class="inputaddress" />
            </p>
             <p class="p">
              <label class="left">固　　话：</label>

              <input type="text" id="txt_tel" name="txt_tel" value="<?php echo $userintro['telphone'];?>" class="input" />
            </p>
             <p class="p">
              <label class="left">身份证号：</label>
              <input type="text" name="txt_idcard" id="txt_idcard"　maxlength="18" value="<?php echo $userintro['identity'];?>" class="input" />
            </p>
            <span class="mem_t5 left">
              <input type="submit" value="提交修改" id="btn_submit" class="mem_button01" />

              <input type="reset" value="重 置" id="btn_reset" class="mem_button02 lmar10" />
            </span>
		</form>	
			<div class="public_corner public_bottomleft"></div>
			<div class="public_corner public_bottomright"></div>
	</div>
	
	</div>
<div class="clear"></div>
</div>

<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->