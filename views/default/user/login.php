<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/login.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="<?php echo STATIC_ROOT;?>jquery/jquery.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?>jquery/jquery-ui.js"></SCRIPT>
<script type="text/javascript">

	var strDefTips = "手机号/会员名";

	//提交函数
	function checklogin(){
			var passWord 	= $.trim($("#txt_password").val());
	
			//错误提示
			var $msg = $("#message");
	
			//出错提示信息
			var accountEmpty = '请填写帐户名';
			var passWordEmpty = '请填写密码';
			if($("#txt_account").val() == "") {
				$msg.html(accountEmpty);
				$("#txt_account").focus();
				return false;
			}
			if(passWord == "") {
				//密码为空
				$msg.html(passWordEmpty);
				$("#txt_password").focus();
				return false;
			}
			//清空错误消息
			$msg.html("");
	}

</script>
<?php include(VIEWS_PATH."public/header.php");?>

  <div class="container980">
    <div class="login_main">
      <div class="login_left">
        <div class="login">
          <dl>
            <dt><img src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/login/login_11.gif" width="170" height="38" />
              <div class="corner topleft"></div>
              <div class="corner topright"></div>
            </dt>
            <dd>
              <div id="message" class="login_txt"  >
              </div>
              <fieldset>
			  <form id="form1" data-type="ajax" action="<?php echo SITE_ROOT;?>/index.php" method="post">
			    <input TYPE="hidden" NAME="con" value="user"/><input TYPE="hidden" NAME="act" value="login"/><input TYPE="hidden" NAME="commit" value="1"/><input type="hidden" name="to" value="<?php echo $referer;?>"/>
                 <p>
                  <label for="txt_mobile">账户名：</label>
                  <input type="text" class="inputlogin" tabindex="1" name="username" id="txt_account" />
                 </p>
                  <p>
                    <label for="txt_username"><span>密　码：</span></label>
                    <input type="password" class="inputlogin" tabindex="2" name="password" id="txt_password" />
                  </p>
                  <?php if($GLOBALS['setting']['site_logincode']){?>
                  <p style="clear:both;">
                    <label for="txt_username"><span>验证码：</span></label>
                    <input class="inputlogin" type="text" id="txt_verify" name="verify" maxlength="4" tabindex="3" style="ime-mode:disabled;width:100px;"/><img style="margin-top:10px;" src="" onclick="getCode();" id="txt_code"/>
                  </p>
                  <?php }?>
                  <p>
                    <input type="button" tabindex="3" id="btn_submit" name="btn_submit" value="" class="loginsumbit" />
                  </p>
                  <div class="forget"> <a href="<?php echo url('user','forget');?>"><span class="blue">忘记密码？</span></a>  <A HREF="<?php echo $alipaylogin;?>"><span class="blue">支付宝会员免费登录</span></A></div>
              </form>
              </fieldset>
            </dd>
            <div class="corner bottomleft"></div>
            <div class="corner bottomright"></div>
          </dl>
        </div>
      </div>
      <div class="login_right">
        <span class="a"><strong class="FF4000">便宜有好货：</strong><span class="gray8">用低于市场的价格赢取时尚潮流的正版行货！</span></span> 
        <span class="a t3"><strong class="FF4000">诚信可信赖：</strong><span class="gray8">尊重数据事实，绝不失信与我们的每一个用户！</span></span>
        <span class="a t3"><strong class="FF4000">购物娱乐化：</strong><span class="gray8">变革无趣的购买生活，让您的购物流程娱乐化！</span></span>
        <span class="a t3"><strong class="FF4000">免费得<?php echo $GLOBALS['setting']['site_money_name'];?>：</strong><span class="gray8">手机免费注册即可获得<?php echo $GLOBALS['setting']['site_giverealmoney'];?><?php echo $GLOBALS['setting']['site_money_name'];?>！</span></span>
        <a href="<?php echo url('user','register');?>"><button type="button" class="registersumbit t3"> </button></a>
      </div>
    </div>
    <div class="clear"></div>
  </div>
<script type="text/javascript">
    getCode();
	$('#btn_submit').click(function(){
		$("form[data-type='ajax']").ajaxSubmit({
			success:function(res){
                var data = JSON.parse(res);
                alert(data);
				if(data.code == 0){
					if(data.url!=''){
                      window.location.replace(data.url);
                    } else {
                      window.location.replace('/index.php');
                    }
				} else {
                  alert(data.msg);
                }
				
			}
		}); 
	});
</script>
<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->