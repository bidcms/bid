<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/bidlog.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>

<?php include(VIEWS_PATH."public/header.php");?>
<div class="container980">
   <div class="register_success font16">  <?php if($complete>0){?><span class="font16 red"><?php echo $goodsinfo['goods_name'];?> [ 第<?php echo $goodsinfo['goods_id'];?>期 ]竞拍已成交！　</span>
    
    成交时间：<strong><?php echo date('Y-m-d H:i:s',$goodsinfo['lasttime']);?>        </strong> 　　获胜者：<strong><?php echo $goodsinfo['currentuser'];?>   </strong>
	<?php } else{?><span class="font16 red"><?php echo $goodsinfo['goods_name'];?> [ 第<?php echo $goodsinfo['goods_id'];?>期 ]竞拍正在进行中！　</span><?php }?>
	<div class="clear"></div>
	</div>
    <div class="clear"></div>
  <table align="center" class="bidlog">
    <thead>
      <tr>
        <th>出价人</th>
        <th>手机号</th>

        <th>价格</th>
        <th>时间</th>
        <th>地区</th>
      </tr>
    </thead>
    <tbody>
     <?php if($buylog_list){foreach($buylog_list as $key=>$val){?>
	 <tr>

        <td><?php echo $val['username'];?></td>
        <td><?php echo parseMobile($val['mobile']);?></td>
        <td>￥<?php echo $val['price'];?></td>
        <td><?php echo date('Y-m-d H:i:s',$val['updatetime']);?></td>
        <td><?php echo parseIp($val['ip']);?>【<?php echo $val['address'];?>】</td>
      </tr>
	<?php }}?>
          </tbody>
  </table>
</div>
<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->