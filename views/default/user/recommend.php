<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/newmember.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>
<?php include(VIEWS_PATH."public/header.php");?>
<script type="text/javascript">
function share(sitename){
	//当前网址
	var shareURL = encodeURIComponent("<?php echo url('index','index',array('uid'=>$GLOBALS['session']->get('uid')));?>");	
	var title = encodeURIComponent("<?php echo $GLOBALS['setting']['site_title'];?>");
	
	switch(sitename)
	{
		//QQ空间
		case 'qzone':
			openURL("http://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url=" + shareURL);
		break;
		
		//人人网
		case 'renren':
			openURL("http://share.renren.com/share/buttonshare.do?link=" + shareURL + "&title=" + title);
		break;
		
		//开心网
		case 'kaixin':
			openURL("http://www.kaixin001.com/repaste/share.php?rurl=" + shareURL + "&rtitle=" + title);
		break;
		
		//新浪微博
		case 'sina':
			openURL("http://v.t.sina.com.cn/share/share.php?url=" + shareURL + "&title=" + title);
		break;
		
		//淘江湖
		case 'taojianghu':
			openURL("http://share.jianghu.taobao.com/share/addShare.htm?url=" + shareURL);
		break;
		
		//豆瓣
		case 'douban':
			openURL("http://www.douban.com/recommend/?url=" + shareURL + "&title=" + title);
		break;
	}
}
//打开链接
function openURL(url)
{
	window.open(url);
}
function getData(){
	return document.getElementById("taPromoteURL").value;
}
function copySuccess(){
	alert("复制成功！内容如下:\n\r" + getData());
}
</script>
<div class="container980">

<?php include(VIEWS_PATH."public/user_menu.php");?>
<div class="mem_right">
    	
<?php include(VIEWS_PATH."public/user_info.php");?>

<ul class="bidinfo_nav mar10">
	<li class="thisclass"><a href="/member/charexchange"><span>推荐好友</span></a></li>
	</ul>
	
	<div class="friend_m mem_borD6">
	<div class="friend_mm">
		<span class="friend_main">
			您邀请的好友首次购买<?php echo $GLOBALS['setting']['site_money_name'];?>，系统自动将购买额度的 <span class="red">10％添加到您的账户</span>
		</span>
		<div class="friend_share mar10">
          <span class="left friend_col01 font14 shareline">选择邀请方式：</span> 
		  <span class="sp-share sp-msn" title="QQ/MSN" id="spMSNQQPIC" onclick="share('qzone');"></span>

		  <span class="left font14 shareline sp-msn" id="spMSNQQ" onclick="share('qzone');">QQ/MSN</span> 
		  <span class="sp-share share_t1 sp-tsina" title="新浪微博" onclick="share('sina');"></span>
		  <span class="left font14 shareline sp-tsina" onclick="share('sina');">新浪微博</span> 
		  <span class="sp-share share_t1 sp-kaixin001" title="开心网" onclick="share('kaixin');"></span>
		  <span class="left font14 shareline sp-kaixin001" onclick="share('kaixin');">开心网</span> 
		  <span class="sp-share share_t1 sp-renren" title="人人网" onclick="share('renren');"></span>
		  <span class="left font14 shareline sp-renren" onclick="share('renren');">人人网</span> 
		  <span class="sp-share share_t1 sp-taojianghu" title="淘江湖" onclick="share('taojianghu');"></span>
		  <span class="left font14 shareline sp-taojianghu" onclick="share('taojianghu');">淘江湖</span> 
		  <span class="sp-share share_t1 sp-douban" title="豆瓣" onclick="share('douban');"></span>

		  <span class="left font14 shareline sp-douban" onclick="share('douban');">豆瓣</span>
		</div>
		<div class="friend_share mar10" id="ddMSNQQ">
		<strong>把下面的内容通过QQ、MSN或邮件发送给好友：</strong><br/>
         <div class="left mar10"><textarea id="taPromoteURL" readonly="readonly" class="tbox6" rows="2">
<?php echo url('index','index',array('uid'=>$GLOBALS['session']->get('uid')));?></textarea>
          <object width="111" height="40" border="0" align="middle" id="clipboard" codebase="http://download.macromedia.com/pub/shockwave/cabs
		/flash/swflash.cab#version=10,0,0,0" classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000">
		<param value="sameDomain" name="allowScriptAccess"/>

		<param value="false" name="allowFullScreen"/>
		<param value="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/clipboard.swf" name="movie"/>
		<param value="high" name="quality"/>
			<embed width="111" height="40" align="middle" pluginspage="http://www.adobe.com/go/getflashplayer_cn" type="application/x-shockwave-flash" allowfullscreen="false" allowscriptaccess="always" name="test" quality="high" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/clipboard.swf"/>
		</object>
		</div>
		
		</div>
		<span class="friend_main01">

				<strong>我邀请的好友</strong>
		</span>
		<div class="friend_share">
			<table style="width:736px;background:#ccc;" cellpadding="1" cellspacing="1">
			<tr>
			<th height="25px" style="background:#fff;">注册帐号</th>
			<th style="background:#fff;">注册时间</th>
			</tr>
			<?php foreach($userlist as $v){?>
			<tr>
			<td height="23px" align="center" style="background:#fff;"><?php echo $v['username'];?></td>
			<td align="center" style="background:#fff;"><?php echo date('Y-m-d H:i:s',$v['updatetime']);?></td>
			</tr>
			<?php }?>
			</table>
			
			  <div class="pages" style="width:736px;">
			  <table align="center" class="mar10"  >
				<tbody>
				  <tr>

					<td>
					<div class="list_page"> <?php echo $pageinfo;?></div>
					  </td>
				  </tr>
				</tbody>

			  </table>
			  </div>
		</div>
		</div>
			<div class="public_corner public_bottomleft"></div>
			<div class="public_corner public_bottomright"></div>
</div>

</div>
	</div>

<div class="clear"></div>
</div>
<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->