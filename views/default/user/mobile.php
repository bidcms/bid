<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/register.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/mobile.js"></SCRIPT>
<?php include(VIEWS_PATH."public/header.php");?>
<div class="container980">

<div class="container980">
  <div class="register_left">
    <div class="left_box01 mar10 borD6 su_height4">

      <div class="mobile">
        <form id="form1" action="<?php echo SITE_ROOT;?>/index.php" method="post">
		<INPUT TYPE="hidden" NAME="commit" value="1"><INPUT TYPE="hidden" NAME="con" value="user"><INPUT TYPE="hidden" NAME="act" value="mobile">
          <span class="mobiletips grayB4">
           <span class="mobiletipsimg" id="mobiletipsimg"></span><span id="showtip"><?php echo $GLOBALS['setting']['site_title'];?>已经向您的手机发送免费的验证短信，请注意查收。</span>
                    </span>
          <div class="txt">
            <div class="ll"> 手机号：<strong id="mobile"><?php echo $GLOBALS['userinfo']['mobile'];?></strong>&nbsp;&nbsp;<span id="spChangeMobile" style="cursor:pointer;" class="blue t2">更换号码</span> </div>

            <div class="rr" > <span id="spNewMobile" style="display:none;">
              <div class="martt"> <span class="left label" >新手机号码：</span> <span class="left" >
                <input type="text" id="txt_newMobilePhone" name="txt_newMobilePhone" class="inputmobile" value="<?php echo $GLOBALS['userinfo']['mobile'];?>" />
                <input type="button" id="btnEditMobile" value="" class="btn_validate1" />
                <input type="button" id="btnCancel" value="" class="btn_validate3" />
                </span> </div>

              </span> </div>
          </div>
          <div class="num" id="a"> <span class="l">验证码：</span> <span class="c">
            <input type="text" id="txt_checkcode" name="txt_checkcode" class="inputmobile" />
            </span>
            <!--成功-->
            <span class="tips11"> <span class="img1" id="div_CheckCodeMsg"></span> 请输入您手机中的6位数字验证码 </span>
          </div>
          <div class="sends" id="main_send"><span class="left" id="one_send">如果您在1分钟内没有收到验证短信，请：</span><input type="button" id="btn_send" value="重新发送" class="re-send"/><span id="sp_send" class="red left"></span></div>
		  <div class="sends" id="forbid_send" style="display:none"><span class="red left" >24 小时内发送的验证短信超过次数限制,请稍后再试。</span></div>
		<div class="sendmsg" id="five_send" style="display:none">如果您在5分钟内没有收到验证短信，请编辑短信“bidcms”发送至<?php echo $GLOBALS['setting']['site_tel'];?>，在正常工作时间内，我们会以人工方式将验证码发送至您的手机。</div>
          <div class="button">
            <input type="submit" id="btn_validate" value=""  class="btn_validate2"/>

            <a href="<?php echo url('index','index');?>">
            <div class="btn_tg"></div>
            </a> 
          </div>
          <div class="clear"></div>
          <input type="hidden" id="hd_ChangeMobile" name="hd_ChangeMobile" value="0" />
        </form>
      </div>
      <div class="public_corner public_topleft"></div>

      <div class="public_corner public_topright"></div>
      <div class="public_corner public_bottomleft"></div>
      <div class="public_corner public_bottomright"></div>
    </div>
  </div>
  <div class="register_right">
    <div class="right_box02 borD6">
      <dl>
        <dt class="black">
			<strong class="left">最新成交</strong>
			<div class="publicmore grayB4"><a href="<?php echo url('index','history');?>" target="_blank">更多</a></div>
			</dt>
			<dd class="bpad10">
			<?php foreach($goods_success as $val){$thumb=thumb($val['thumb']);?>
				<!--单个开始-->
				<div class="leftimg_txt">
				<div class="leftimg borEB"><a href="<?php echo url('index','complete',array('id'=>$val['goods_id']));?>" title="<?php echo $val['goods_name'];?>"  target="_blank"><img src="<?php echo $thumb[0];?>" alt="<?php echo $val['goods_name'];?>" width="90" height="76" /></a></div>
				<table>
				<tr>
				<td>
				<span class="leftname">
				<a href="<?php echo url('index','complete',array('id'=>$val['goods_id']));?>" title="<?php echo $val['goods_name'];?>" target="_blank">
				<?php echo $val['goods_name'];?>							</a>
				</span>
				<span class="leftprice">
				<span class="gray62">获胜者：</span><span class="blue"><?php echo $val['currentuser'];?></span>
				</span>
				<span class="leftprice">
				<span class="gray62">成交价：</span><span class="yellow66 font14"><strong>￥<?php echo $val['nowprice'];?></strong></span>
				</span>
				</td></tr></table>
				</div>
				<!--单个结束-->
			<?php }?>
			</dd>
      </dl>
      <div class="public_corner public_topleft2"></div>
      <div class="public_corner public_topright2"></div>
      <div class="public_corner public_bottomleft"></div>
      <div class="public_corner public_bottomright"></div>
    </div>
    <div class="right_box borD6 mar10 bpad10">

      <dl>
        <dt><strong>在线客服</strong></dt>
        <dd>
          <div class="helpbutton mar10"><img border="0" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/index4/tell_03.gif" alt="服务热线" ></div>
          <div class="helpbutton mar10"> <span class="email"></span> <span class="emailtxt">服务邮箱：<?php echo $GLOBALS['setting']['site_email'];?></span></span></div>
        </dd>
      </dl>

      <div class="public_corner public_topleft2"></div>
      <div class="public_corner public_topright2"></div>
      <div class="public_corner public_bottomleft"></div>
      <div class="public_corner public_bottomright"></div>
    </div>
  </div>
</div>
<SCRIPT LANGUAGE="JavaScript">
<!--
	$(document).ready(function(){
	//自动发送验证短信
	$('#btn_send').click();});
//-->
</SCRIPT>
<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->