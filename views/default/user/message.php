
<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/newmember.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>
<?php include(VIEWS_PATH."public/header.php");?>

<script type="text/javascript">
$(function(){
	$("#selectAll").click(function(){
		$(".items").attr("checked",$(this).is(":checked"));
		});
	$("#delete").click(function(){
		if($(".items:checked").length == 0)
		{
			return false;
		}
		var arr = new Array();
		$(".items:checked").each(function(i){
			arr[i] = $(this).val();
			});
		var ids = arr.toString();
		if(!confirm('确认要删除选择的站内信！'))
		{
			return false;
		}
		window.location.href = site_root+"/index.php?con=user&act=pmdelete&id=" + ids;
		});
	$(".del2").click(function(){
		if(!confirm('确认要删除站内信！'))
		{
			return false;
		}
		});
});
</script>
<div class="container980">

<?php include(VIEWS_PATH."public/user_menu.php");?>
<div class="mem_right">
    	
<?php include(VIEWS_PATH."public/user_info.php");?>
<ul class="bidinfo_nav mar10">
	<li class="thisclass"><a href="<?php echo url('user','message');?>" ><span>收件箱</span></a></li>
	</ul>
	<div class="mem_box bpad10">
	<div class="mem_boxx">

	<div class="bidinfo_nav_txt011">
		<span class="x_w1 txtcenter lmar10"> <input type="checkbox" id="selectAll" title="全选"/></span>
		<span class="x_w22 txtcenter ">全选</span>
		<span class="x_w22 txtcenter blue"><a href="#">删除</a></span>
		
		<span class="x_w6">共 <?php echo $count ;?> 封</span>
	</div>
	
		<?php foreach($pmlist as $v){?>
		<div class="mag_main">
		<span class="x_w1 txtcenter"><input type="checkbox" name="id[]" class="items" value="<?php echo $v['id'];?>"></span>
		<span class="x_w2 txtcenter"> <span class="x_w2b txtcenter"  title="未阅读"></span>
       </span>
		<span class="x_w9 txtcenter"><?php echo empty($v['from_uid'])?$GLOBALS['setting']['site_title']:$v['from_uid'];?></span>
		<span class="x_w3 blue"><a href="<?php echo url('user','pmview',array('id'=>$v['id']));?>"><?php echo $v['title'];?></a></span>
		<span class="x_w4"><?php echo date('Y-m-d H:i:s',$v['updatetime']);?></span>

		<span class="x_w5 txtcenter blue"><a href="<?php echo url('user','pmdelete',array('id'=>$v['id']));?>">删除</a></span>
		
	</div>
	<?php }?>
	 	
	 <div class="pages" >
      <table align="center" class="mar10"  >
        <tbody>
          <tr>
            <td>

			<div class="list_page">
                <?php echo $pageinfo;?>
              </div>
            </td>
          </tr>
        </tbody>
      </table>

    </div></div>
	<div class="public_corner public_bottomleft"></div>
	<div class="public_corner public_bottomright"></div>
	</div>
	</div>
	<div class="clear"></div>
</div>
<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->