<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/newmember.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>
<?php include(VIEWS_PATH."public/header.php");?>

<div class="container980">

<?php include(VIEWS_PATH."public/user_menu.php");?>
<div class="mem_right">
    	
<?php include(VIEWS_PATH."public/user_info.php");?>

    <ul class="bidinfo_nav mar10">
    <li class="other"><a href="<?php echo url('user');?>" ><span>我参与的竞拍</span></a></li>
    <li class="thisclass"><a href="<?php echo url('user','gain');?>"><span>成功的竞拍</span></a></li>
    <li class="other"><a href="<?php echo url('order','list');?>"><span>我的订单</span></a></li>
	<li class="other"><a href="<?php echo url('order','list',array('type'=>1));?>"><span>积分换购</span></a></li>
	<li class="other"><a href="<?php echo url('order','show');?>"><span>我的晒单</span></a></li>
  </ul>
  <div class="bidinfo_nav_txt"> <span class="d_w11">商品</span> <span class="d_w22">竞拍价（元）</span> <span class="d_w33">出价人</span> <span class="d_w44">出价/<?php echo $GLOBALS['setting']['site_money_name'];?></span> <span class="d_w55">状态</span>

    <div class="public_corner public_bottomleft"></div>
    <div class="public_corner public_bottomright"></div>
  </div>
        <!--竞拍结束-->
		<?php if($goods_list){foreach($goods_list as $k=>$v){$thumb=thumb($v['thumb'])?>
			<div class="bidinfo_main mar10 borD6">
			<dl>
			<dt> <strong>第<?php echo $v['goods_id'];?>期</strong> <span class="grayB4 lmar10">结束时间：<?php echo date('Y-m-d H:i:s',$v['lasttime']);?></span> <span class="blue lmar10"> 
			<a href="<?php echo url('index','complete',array('id'=>$v['goods_id']));?>" target="_blank">详细</a>
			</span> </dt>
			<dd class="d_w1 l_bor1"> <span class="borEB img">
			<a href="<?php echo url('index','complete',array('id'=>$v['goods_id']));?>" target="_blank">
			<img src="<?php echo $thumb[0];?>" class="imgbor left" width="90" height="76" alt="<?php echo $v['goods_name'];?>" /> </a>
			<!--结束-->
			</span> 
			<table>
			<tr>
			<td><span class="name blue">
			<a href="<?php echo url('index','complete',array('id'=>$v['goods_id']));?>" target="_blank"> <?php echo $v['goods_name'];?> </a> </span> <span class="price">市场价：￥<?php echo $v['marketprice'];?></span> 
			</td>

			</tr>
			</table>
			</dd>
			<dd class="d_w2 l_bor1">￥<?php echo $v['nowprice'];?></dd>
			<dd class="d_w3 l_bor1">
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
			<td><span class="blue"><?php echo $v['currentuser'];?></span>

			<br />
			<!--竞拍结束-->
			赢得
			</td>
			</tr>
			</table>
			</dd>
			<dd class="d_w4 l_bor1"> 
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr height="24">

			<td> 出价：<?php echo $v['count'];?>次</td>
			</tr>
			<tr height="24">
			<td>  <?php echo $GLOBALS['setting']['site_money_name'];?>：<?php echo $v['money'];?></td>
			</tr>
			</table>

			</dd>
			<dd class="d_w5">
			<table border="0" cellpadding="0" cellspacing="0">
			<tr>
			<td>
			<!--竞拍结束-->

			<div class="membutton_f">
			<p style="width:150px;">您可以选择<br /><?php if($v['goods_status']>1){?><input class="membutton_bg" type="button" onclick="javascript:Order_Dialog('<?php echo $v['goods_id'];?>');" value="全价购买"><?php } else{?><input type="button" class="chabtn" onclick="javascript:Order_Dialog('<?php echo $v['goods_id'];?>');" value="下订单"/><?php }?></p>
			</div> 
			</td>

			</tr>
			</table>
			</dd>
			</dl>
			<!--竞拍结束-->
			<div class="public_corner public_topleft"></div>
			<div class="public_corner public_topright"></div>
			<div class="public_corner public_bottomleft"></div>
			<div class="public_corner public_bottomright"></div>
			</div>
	<?php }}?>

        <div class="pages" >
      <table align="center" class="mar10"  >
        <tbody>
          <tr>

            <td>
			<div class="list_page"> <?php echo $pageinfo;?></div>
              </td>
          </tr>
        </tbody>

      </table>
    </div>
  </div>
  <div class="clear"></div>
</div>
<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->