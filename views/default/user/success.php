<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/register.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>


<?php include(VIEWS_PATH."public/header.php");?>
<div class="container980">

<div class="container980">
<div class="left_box02 borFC su_height">

<div class="success_user01 su_t"><span class="su_img"></span>恭喜 <span class="red"><?php echo $GLOBALS['session']->get('username');?></span> ，手机验证成功！</div>
<!--注册引导修改区域开始-->
<div class="xibi">
      <div class=" red_b font14 ">还没有<?php echo $GLOBALS['setting']['site_money_name'];?>吗？您可以 <span class="blue_under"><a class="xibih4" href="<?php echo url('user','intro');?>">上传头像得<?php echo $GLOBALS['setting']['site_money_name'];?></a></span> 或者 <span class="blue_under"><a href="<?php echo url('user','recommend');?>">推荐好友得<?php echo $GLOBALS['setting']['site_money_name'];?></a></span></div>
      <ol class="buy">
        <li>马上 <span class="blue_under"> <a href="<?php echo url('user','charge');?>">购买<?php echo $GLOBALS['setting']['site_money_name'];?></a></span> ，体验竞拍乐趣。</li>
        <li>您已通过手机验证，<?php if($GLOBALS['setting']['site_giverealmoney']){?><?php echo $GLOBALS['setting']['site_giverealmoney'];?><?php echo $GLOBALS['setting']['site_money_name'];?>已经充入您的帐户，<?php }?>马上去 <span class="blue_under"><a href="<?php echo url('index','free');?>">体验竞拍</a></span> 吧!</li>
        <li>您可以到 <span class="blue_under"><a href="<?php echo url('user');?>">用户中心</a></span> 看看，或者 <span class="blue_under"><a target="_self" href="<?php echo url('index');?>">返回首页</a>。</span></li>
      </ol>
    </div>
<!--注册引导修改区域结束-->
  <?php if($status){?>
	<div class="success_charge">
	
	<span class="success_txt">他们刚刚购买<?php echo $GLOBALS['setting']['site_money_name'];?>：</span>
		<?php foreach($status as $v){?>
			<span class="biduser_a">
			<span title="peak" class="biduser_avatar">
			<img height="48" width="48" alt="" src="<?php echo UC_API."/avatar.php?uid=".$v['uid']."&size=middle&type=virtual";?>">
			</span>
			<span class="blue txtcenter biduser_a"><?php echo $v['username'];?></span>
			</span>
		<?php }?>
			</div><?php }?>
	<div class="public_corner public_topleft6"></div>
	<div class="public_corner public_topright6"></div>
	<div class="public_corner public_bottomleft6"></div>
	<div class="public_corner public_bottomright6"></div>
  </div>
  <div class="register_right">
    <div class="right_box02 borD6">
      <dl>
        <dt class="black">
			<strong class="left">最新成交</strong>
			<div class="publicmore grayB4"><a href="<?php echo url('index','history');?>" target="_blank">更多</a></div>
			</dt>
			<dd class="bpad10">
			<?php foreach($GLOBALS['successinfo'] as $val){$thumb=thumb($val['thumb']);?>
				<!--单个开始-->
				<div class="leftimg_txt">
				<div class="leftimg borEB"><a href="<?php echo url('index','complete',array('id'=>$val['goods_id']));?>" title="<?php echo $val['goods_name'];?>"  target="_blank"><img src="<?php echo $thumb[0];?>" alt="<?php echo $val['goods_name'];?>" width="90" height="76" /></a></div>
				<table>
				<tr>
				<td>
				<span class="leftname">
				<a href="<?php echo url('index','complete',array('id'=>$val['goods_id']));?>" title="<?php echo $val['goods_name'];?>" target="_blank">
				<?php echo $val['goods_name'];?>							</a>
				</span>
				<span class="leftprice">
				<span class="gray62">获胜者：</span><span class="blue"><?php echo $val['currentuser'];?></span>
				</span>
				<span class="leftprice">
				<span class="gray62">成交价：</span><span class="yellow66 font14"><strong>￥<?php echo $val['nowprice'];?></strong></span>
				</span>
				</td></tr></table>
				</div>
				<!--单个结束-->
			<?php }?>
			</dd>
      </dl>
      <div class="public_corner public_topleft2"></div>
      <div class="public_corner public_topright2"></div>
      <div class="public_corner public_bottomleft"></div>
      <div class="public_corner public_bottomright"></div>
    </div>
    <div class="right_box borD6 mar10 bpad10">

      <dl>
        <dt><strong>在线客服</strong></dt>
        <dd>
          <div class="helpbutton mar10"><img border="0" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/index4/tell_03.gif" alt="服务热线" ></div>
          <div class="helpbutton mar10"> <span class="email"></span> <span class="emailtxt">服务邮箱：<?php echo $GLOBALS['setting']['site_email'];?></span></span></div>
        </dd>
      </dl>

      <div class="public_corner public_topleft2"></div>
      <div class="public_corner public_topright2"></div>
      <div class="public_corner public_bottomleft"></div>
      <div class="public_corner public_bottomright"></div>
    </div>
  </div>
</div>
<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->