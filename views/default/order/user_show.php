<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $pagetitle;?>-<?php echo $GLOBALS['setting']['seo_title'];?> <?php echo $GLOBALS['setting']['site_title'];?></title>
 <META NAME="Keywords" CONTENT="<?php echo $GLOBALS['setting']['seo_keyword'];?>">
  <META NAME="Description" CONTENT="<?php echo $GLOBALS['setting']['seo_description'];?>">
</head>
<body>
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/newmember.css" rel="stylesheet" type="text/css" />
<link href="<?php echo STATIC_ROOT;?>/jquery/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/textscroll.js"></SCRIPT>
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jqueryui/1.12.1/jquery-ui.min.js"></SCRIPT>
<?php include(VIEWS_PATH."public/header.php");?>

<div class="container980">

<?php include(VIEWS_PATH."public/user_menu.php");?>
<div class="mem_right">
    	
<?php include(VIEWS_PATH."public/user_info.php");?>

    <ul class="bidinfo_nav mar10">
    <li class="other"><a href="<?php echo url('user');?>" ><span>我参与的竞拍</span></a></li>
    <li class="other"><a href="<?php echo url('user','gain');?>"><span>成功的竞拍</span></a></li>
    <li class="other"><a href="<?php echo url('order','list');?>"><span>我的订单</span></a></li>
	<li class="other"><a href="<?php echo url('order','list',array('type'=>1));?>"><span>积分换购</span></a></li>
	<li class="thisclass"><a href="<?php echo url('order','show');?>"><span>我的晒单</span></a></li>
  </ul>
        <!--竞拍结束-->
		<?php if($showlist){foreach($showlist as $k=>$val){?>
			<div class="bidinfo_main mar10 borD6">
			<dl>
			<dt> <strong><?php echo $val['title'];?></strong>  <span class="grayB4 lmar10">晒单时间：<?php echo date('Y-m-d H:i:s',$val['updatetime']);?></span> <span class="blue lmar10"> 
			<a href="<?php echo url('index','show',array('id'=>$val['id']));?>" target="_blank">详细</a>
			</span> </dt>
			<dd class="d_w1 l_bor1" style="width:500px;">
			<?php 
			$pic=getUploadPic($val['content'],5);
			foreach($pic as $v) {
			?> <img src="<?php echo $v;?>" style="width:80px;height:75px;"/>
			<?php } ?></dd>
			<dd class="d_w2 l_bor1" style="width:100px;">
			<?php echo $GLOBALS['show_status'][$val['ispassed']];?>
			</dd>
			<dd class="d_w5" style="width:130px;text-align:center;height:100px;line-height:100px;">
			
			<A HREF="<?php echo url('order','showmodify',array('updateid'=>$val['id']));?>">修改</A>
			<A onclick="return confirm('确认删除？删除后不可恢复');" HREF="<?php echo url('order','show',array('deleteid'=>$val['id']));?>" >删除</A>
			
			</dd>
			</dl>
			<!--竞拍结束-->
			<div class="public_corner public_topleft"></div>
			<div class="public_corner public_topright"></div>
			<div class="public_corner public_bottomleft"></div>
			<div class="public_corner public_bottomright"></div>
			</div>
	<?php }}?>

        <div class="pages" >
      <table align="center" class="mar10"  >
        <tbody>
          <tr>

            <td>
			<div class="list_page"> <?php echo $pageinfo;?></div>
              </td>
          </tr>
        </tbody>

      </table>
    </div>
  </div>
  <div class="clear"></div>
</div>
<!--底部-->
<?php include(VIEWS_PATH."public/footer.php");?>
<!--/底部-->