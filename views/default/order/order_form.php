<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
header("content-type:text/html;charset=utf-8");
?>
<form name="account" method="post" action="<?php echo SITE_ROOT;?>/index.php?con=order&act=confirm">
<INPUT TYPE="hidden" NAME="commit" value="1">
<INPUT TYPE="hidden" NAME="goodsid" value="<?php echo $goods['goods_id'];?>">
<INPUT TYPE="hidden" NAME="order_type" value="<?php echo $order_type;?>">
<table>
<?php if($address){foreach($address as $k=>$v){?>
<tr><td colspan="2"><INPUT TYPE="radio" NAME="addressid" value="<?php echo $v['id'];?>" <?php if($k==0){?>checked<?php }?>  onclick="$('#showaddress').hide();"><?php echo $v['username'];?> <?php echo $v['telphone'];?> <?php echo $v['address'];?></td></tr>
<?php }}?>

<tr><td colspan="2">
<INPUT TYPE="radio" NAME="addressid" value="0" onclick="$('#showaddress').show();">新添加一个收货地址</td></tr>
<tr><td colspan="2" style="display:none;" id="showaddress">
<TABLE>
<TR>
	<TD>收货人：</TD>
	<TD><input type="text" name="username" maxlength="10" value="" class="login_input"></TD>
</TR>
<TR>
	<TD>联系电话：</TD>
	<TD><input type="text" name="telphone" value="" maxlength="32"  class="login_input"></TD>
</TR>
<TR>
	<TD valign="top">收货地址：</TD>
	<TD><textarea name="address" class="login_input" style="height:50px;width:300px;"></textarea></TD>
</TR>

</TABLE>
</td></tr>

<tr><td>
购买商品：</td><td><?php echo $goods['goods_name'];?></td></tr>
<tr><td>应付：</td><td>￥<strong class="red"><?php echo $goods['nowprice'];?></strong>(含运费<?php echo $goods['yfeemoney'];?>)<?php if($price[2]>0){?>,使用代金券<?php echo $price[2];?>元<?php }?></td></tr>
<tr><td valign="top">订单备注：</td><td>
<TEXTAREA NAME="content" class="login_input" style="height:50px;width:300px;"></TEXTAREA></td></tr>
</table>
<div class="layer_w333 left mar10">
<input type="submit" value="确定"  onmouseout="this.className='layer_button'" onmousemove="this.className='layer_button_over'" class="layer_button">&nbsp;&nbsp;
<input type="button" value="取消" onclick="Dialog_close();"  onmouseout="this.className='layer_button'" onmousemove="this.className='layer_button_over'" class="layer_button"/>
</div>

</form>

</div>