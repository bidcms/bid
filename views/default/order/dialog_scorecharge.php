<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
header("content-type:text/html;charset=utf-8");
?>
<div class="dialogdiv01">
<form target="_blank" id="chargeForm" action="<?php echo SITE_ROOT;?>/index.php" method="post" >
<INPUT TYPE="hidden" NAME="con" value="order"/>
<INPUT TYPE="hidden" NAME="act" value="scorepay"/>
<INPUT TYPE="hidden" NAME="commit" value="1"/>
<INPUT TYPE="hidden" NAME="goodsid" value="<?php echo $id;?>"/>
<INPUT TYPE="hidden" NAME="order_type" value="1"/>
  
<div class="dialogdiv01">
<TABLE>
<?php if($address){foreach($address as $k=>$v){?>
<tr><td colspan="2"><INPUT TYPE="radio" NAME="addressid" value="<?php echo $v['id'];?>" <?php if($k==0){?>checked<?php }?>  onclick="$('#showaddress').hide();"><?php echo $v['username'];?> <?php echo $v['telphone'];?> <?php echo $v['address'];?></td></tr>
<?php }}?>

<tr><td colspan="2">
<INPUT TYPE="radio" NAME="addressid" value="0" onclick="$('#showaddress').show();">新添加一个收货地址</td></tr>
<tr><td colspan="2" style="display:none;" id="showaddress">
<TABLE>
<TR>
	<TD>收货人：</TD>
	<TD><input type="text" name="username" maxlength="10" value="" class="login_input"></TD>
</TR>
<TR>
	<TD>联系电话：</TD>
	<TD><input type="text" name="telphone" value="" maxlength="32"  class="login_input"></TD>
</TR>
<TR>
	<TD valign="top">收货地址：</TD>
	<TD><textarea name="address" class="login_input" style="height:50px;width:300px;"></textarea></TD>
</TR>

</TABLE>
</td></tr>
</TABLE>
 <div class="left layer_w22 mar10"><span class="msg_charge_div"></span><strong>需支付金额： <span class="red font16" id="needpay"><?php echo $totalfee;?></span> 元</strong></div>
<div class="left layer_w22 mar20"><strong>选择付款方式</strong></div>
<div class="left mar10">
	<div class="txt mem_t7"><strong>选择付款方式：</strong>(支持全国<span class="blue"><a href="https://www.alipay.com/static/bank/index.htm" target="_blank">65</a></span>家金融机构)</div>
    <div class="txt1 t8">
        <input id='pay_alipay' name="gateway" style="cursor:pointer;" value="alipay" type="radio" checked="true" class="radio1" />
        <label for='pay_alipay' class="label"><img src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/layer/alipay.gif" id="img_zhifubao" style="cursor:pointer;" class="mem_charge_img"  border="0" alt="支付宝" /></label>

        <input id='pay_bank' name="gateway" style="cursor:pointer;" value="bank" type="radio" class="radio1" />
        <label for='pay_bank' class="label"><img src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/layer/alipay.gif" id="img_zhifubao" style="cursor:pointer;" class="mem_charge_img"  border="0" alt="银行转帐" /></label>
    </div>
</div>

<div class="layer_w222 left mar20">
	<input type="button" value="立即购买" onmouseout="this.className='layer_button'" onmousemove="this.className='layer_button_over'" onclick="$('#chargeForm').submit();btnChargeConfirm_click();"  id="btnChargeConfirm" class="layer_button">
</div>
<input type="hidden" value="1" name="auto">
</form>

</div>