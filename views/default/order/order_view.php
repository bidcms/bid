<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
header("content-type:text/html;charset=utf-8");
?>
<iframe src="" style="display:none;" name="order"></iframe>
	<FORM METHOD="POST" target="order" ACTION="<?php echo SITE_ROOT;?>/index.php?con=order&act=view">
	 <INPUT TYPE="hidden" NAME="commit" value="1">
	 <INPUT TYPE="hidden" NAME="ajax" value="1">
	 <INPUT TYPE="hidden" NAME="orderid" value="<?php echo $order['id'];?>">
		<TABLE>
	  <TR>
		<TD>产品名称：</TD>
		<TD><span class="font_bolder red">
		<?php echo $order['goods_name'];?>
		</span></TD>
	 </TR>
	 <TR>
		<TD>应付金额：</TD>
		<TD><span class="font_bolder red">
		￥<?php echo $order['total_fee'];?><?php if($order['order_status']<1){?>
		<a href="javascript:OrderPay_Dialog('<?php echo $order['id'];?>');"><SPAN CLASS="blue">付款</SPAN></A><?php }?>
		</span></TD>
	 </TR>
	 <TR>
		<TD>订单状态：</TD>
		<TD><span class="font_bolder red">
		<?php echo $GLOBALS['order_status'][$order['order_status']];?>
		</span></TD>
	 </TR>
	 <TR>
		<TD>下单日期：</TD>
		<TD><span class="font_bolder red"><?php echo date('Y-m-d H:i:s',$order['updatetime']);?></span></TD>
	 </TR>
	 <?php if($order['sendtime']){?><TR>
		<TD>发货日期：</TD>
		<TD><span class="font_bolder red"><?php echo date('Y-m-d H:i:s',$order['sendtime']);?></span></TD>
	 </TR><?php }?>
	 <?php if($order['paytime']){?><TR>
		<TD>付款日期：</TD>
		<TD><span class="font_bolder red">
		<?php echo date('Y-m-d H:i:s',$order['paytime']);?>
		</span></TD>
	 </TR><?php }?>
	<TR>
		<TD>备注：</TD>
		<TD><TEXTAREA NAME="content" style="height:60px;width:200px;" class="login_input"><?php echo $order['content'];?></TEXTAREA></TD>
	 </TR>
	 <?php if($order['sendcontent']){?>
	 <TR>
		<TD>发货备注：</TD>
		<TD><?php echo $order['sendcontent'];?></TD>
	 </TR><?php }?>
	  <?php if($order['cancelcontent']){?>
	 <TR>
		<TD>取消备注：</TD>
		<TD><?php echo $order['cancelcontent'];?></TD>
	 </TR>
	 <?php }?>
	 <TR>
		<TD>收货地址：</TD>
		<TD><?php if($order['order_status']<2){?><TEXTAREA NAME="address" style="height:60px;width:200px;" class="login_input"><?php echo $order['address'];?></TEXTAREA><?php } else{?><?php echo $order['address'];?><?php }?></TD>
	 </TR>
	 <TR>
		<TD></TD>
		<TD><INPUT TYPE="submit" value="确定"  onmouseout="this.className='layer_button'" onmousemove="this.className='layer_button_over'" class="layer_button"></TD>
	 </TR>
	 </TABLE>
	 </FORM>