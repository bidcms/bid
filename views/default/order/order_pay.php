<?php
/*  
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: order.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_BIDCMS')) {
	exit('Access Denied');
}
header("content-type:text/html;charset=utf-8");
?>
<div class="dialogdiv01">
<form target="_blank" id="chargeForm" action="<?php echo SITE_ROOT;?>/index.php" method="get" >
<INPUT TYPE="hidden" NAME="con" value="order"/>
<INPUT TYPE="hidden" NAME="act" value="pay"/>
 <input type="hidden" name="orderid" value="<?php echo $order['id'];?>"/>
<div class="left layer_w22"><strong><?php echo $order['goods_name'];?></strong></div>
<div class="layer_bg layer_w222 mar10">
<div class="layer_w222 left">
 应付：&nbsp;<span class="cur_menoy">￥<strong class="red"><?php echo $order['total_fee'];?></strong></span>
 
</div>
</div>
<div class="left layer_w22 mar20"><strong>选择付款方式</strong></div>
<div class="layer_w222 left mar10">
	<input type="radio" name="gateway" value="alipay" id="pay_alipay" class="layer_radio1" checked>
	<label for="pay_alipay" class="layer_label1">
		<img border="0" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/i/layer/alipay.gif" onclick="$('#pay_alipay').attr('checked',true);" alt="支付宝">
	</label>
</div>
<div class="layer_w222 left mar20">
	<input type="button" value="立即付款" onmouseout="this.className='layer_button'" onmousemove="this.className='layer_button_over'" onclick="$('#chargeForm').submit();btnOrderConfirm_click();"  id="btnChargeConfirm" class="layer_button">
</div>
<div class="layer_msg layer_w22 mar20">
 	<span class="grayB4">
 	亲爱的用户，购买<?php echo $GLOBALS['setting']['site_money_name'];?>过程中如有任何无法解决的问题。请拨打免费电话：</span>400-666-3036<span class="grayB4">。我们真诚为您服务！
 	</span>
</div>
</form>

</div>