<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/calendar.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>邮件管理</title>
</head>
<body>

<SCRIPT LANGUAGE="JavaScript">
<!--
	function getemail()
	{
		$.ajax({
   				type: "POST",
   				url: "?con=email&act=emaillist",
				ifModified :false,
				cache:true,
				beforeSend: function(XMLHttpRequest){
                   $("#emaillist").val("loading....");
                }, 
   				success:function(msg){
					var e=msg.split(',');
					str="";
					for(i in e)
					{
						str+=e[i]+"\n";
					}
					$("#emaillist").val(str);
					
   				}
			});
	}
	function showtempcontent(tc)
	{
		$.ajax({
   				type: "POST",
   				url: "?con=email&act=emailcontent&file="+$('#'+tc).val(),
				ifModified :false,
				cache:true,
				beforeSend: function(XMLHttpRequest){
                   $("#emailcontent").val("loading....");
                }, 
   				success:function(msg){
					$("#emailcontent").val(msg);
					
   				}
			});
	}
//-->
</SCRIPT>
<div id="man_zone">
   <form action="?con=email" method="post">
  <INPUT TYPE="hidden" NAME="commit" value="1">
  <table width="99%" border="0" align="center"  cellpadding="3" cellspacing="1" class="table_style">
 
    <tr>
      <td width="18%" class="left_title_1"><span class="left-title">邮件主题：</span></td>
      <td width="82%"><INPUT TYPE="text" class="normal_txt"  NAME="subject" id="subject"  size="70" value=""></td>
    </tr>
    <tr>
      <td class="left_title_1">邮件内容：</td>
      <td><SELECT NAME="" id="tc">
		<?php foreach($temp as $v){?>
		<OPTION VALUE="<?php echo $v;?>"><?php echo $v;?></option>
		<?php }?>
      </SELECT><INPUT TYPE="button" VALUE="读取" class="normal_button" ONCLICK="showtempcontent('tc');"><br/>
	  <p>收件人：{email_user} 发件日期：{email_date} 网站地址：{site_root} 网站名：{site_name}</p>
	  <textarea class="normal_txt" style="width:500px;height:300px;" NAME="content" id="emailcontent"></textarea></td>
    </tr>
	<tr>
      <td class="left_title_1">收件人列表：</td>
      <td><INPUT TYPE="button" VALUE="取出会员邮件" class="normal_button" ONCLICK="getemail()"> 多个收件人请换行<br/>
	  <textarea class="normal_txt" style="width:500px;height:300px;" NAME="emaillist" id="emaillist"></textarea></td>
    </tr>
	<tr>
      <td></td>
      <td><INPUT TYPE="submit" class="normal_button" value="提交"></td>
    </tr>
  </table>
  </FORM>
</div>
</body>
</html>
