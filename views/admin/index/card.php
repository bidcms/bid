<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/calendar.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>代金券管理</title>
</head>
<STYLE TYPE="text/css">
	
</STYLE>

<body>
<SCRIPT LANGUAGE="JavaScript">
<!--
	function addcard()
{
	url="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=cardmodify";
	$.get(url, function(data){
		$('#card_area').html(data);
		$('#card_area').show();
	}); 
}
function modifycard(id)
	{
		url="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=cardmodify&updateid="+id;
		$.get(url, function(data){
			$('#card_area').html(data);
			$('#card_area').show();
		}); 
	}
function parseMyCardData(data)
{
	var objdata=data;
	if(objdata.dotype=='update')
	{
		$('#cardnumber-'+objdata.id).html(objdata.cardnumber);
		$('#cardvalue-'+objdata.id).html(objdata.cardvalue);
		$('#cardcode-'+objdata.id).html(objdata.cardcode);
		$('#updatetime-'+objdata.id).html(objdata.updatetime);
		$('#lasttime-'+objdata.id).html(objdata.lasttime);
		$('#username-'+objdata.id).html(objdata.username);

		$('#card_area').hide();
	}
	else
	{
	$('#card0').after('<TR class="tr1" id="card'+objdata.id+'"><TD>'+objdata.cardnumber+'</TD><TD>'+objdata.cardvalue+'</TD><TD align="center">'+objdata.cardcode+'</TD><TD align="center">'+objdata.updatetime+'</TD><TD align="center">'+objdata.lasttime+'</TD><TD align="center">'+objdata.username+'</TD><td>未使用</td><td width="70px" align="center"><A HREF="javascript:deleteVal(\'card\',\''+objdata.id+'\',\'cate'+objdata.id+'\')">删除</A></td></TR>');
	$('#card_area').hide();
	}
}
//-->
</SCRIPT>
<ul class="submenu" id="submenu">

<li class="<?php echo !isset($_GET['status'])?'focus':'normal';?>">
<A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=card">全部</A>
</li>
<li class="<?php echo $_GET['status']==2?'focus':'normal';?>">
<A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=card&status=2">未使用</A>
</li>
<li class="<?php echo $_GET['status']==1?'focus':'normal';?>">
<A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=card&status=1">已使用</A>
</li>
</ul>
<div class="list">
<TABLE cellpadding="1" cellspacing="1">
<TR>
	<th>编号</th><th>面值</th><th>认证码</th><th>生效时间</th><th>失效时间</th><th>使用人</th><th>状态</th><th style="width:80px;">操作</th>
 </tr>
 <TR class="tr1" id="card0">
	<TD></TD>
	<TD></TD>
	<TD></TD>
	<TD></TD>
	<TD></TD>
	<TD></TD>
	<TD></TD>
	<td width="70px" align="center">
	<A HREF="javascript:addcard();">添加</A>
	</td>
</TR>
<?php   foreach($cardlist AS $key => $val){     ?>
<TR class="tr1" id="card<?php echo $val['id'];?>">
		<TD width="150px" align="center">
		<div id="cardnumber-<?php echo $val['id'];?>"><?php echo $val['cardnumber'];?></div>
		</TD>
		<TD width="40px" align="center">
		<div id="cardvalue-<?php echo $val['id'];?>"><?php echo $val['cardvalue'];?></div>
		</TD>
		<TD width="60px" align="center">
		<div id="cardcode-<?php echo $val['id'];?>"><?php echo $val['cardcode'];?></div>
		</TD>
		<TD width="140px" align="center">
		<div id="updatetime-<?php echo $val['id'];?>"><?php echo date('Y-m-d H:i:s',$val['updatetime']);?></div>
		</TD>
		<TD width="140px" align="center">
		<div id="lasttime-<?php echo $val['id'];?>"><?php echo date('Y-m-d H:i:s',$val['lasttime']);?></div>
		</TD>
		<TD width="70px" align="center">
		<div id="username-<?php echo $val['id'];?>"><?php echo $val['username'];?></div>
		</TD>
		
		<td width="60px" align="center">
		<div id="cardstatus-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff0000';" onmouseout="this.style.backgroundColor='';" onclick="updateVal('card','cardstatusinput-<?php echo $val['id'];?>','id','',['已使用','未使用']);">
		<?php echo $val['cardstatus']?'已使用':'未使用';?>
		</div>
		<INPUT TYPE="text" class="hideinput" id="cardstatusinput-<?php echo $val['id'];?>" value="<?php echo intval(!$val['cardstatus']);?>" title='nochange'>
		</td>
		<td align="center"> <A HREF="javascript:modifycard('<?php echo $val['id'];?>');">修改</A> <a href="javascript:deleteVal('card','<?php echo $val['id'];?>','card<?php echo $val['id'];?>')">删除</a></td>
	</tr>
<?php  }    ?>
 </table>

 </div>
 <ul class="page">
	<?php  echo $pageinfo; ?>
</ul>
 <DIV id="card_area" style="display:none;border:2px solid #ccc;width:300px;position:absolute;left:260px;top:60px;background:#fff;z-index:10px;"></DIV>
</body>
</html>