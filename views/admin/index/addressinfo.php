<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>团购管理</title>
</head>
<STYLE TYPE="text/css">
	
</STYLE>
<body>

<div class="list">
<TABLE cellpadding="1" cellspacing="1">
<TR>
	<TH>用户id</TH>
	<TH>收件人</TH>
	<TH>电话</TH>
	<TH>地址</TH>
	<TH>操作</TH>
</TR>

<?php foreach($addressinfo as $key=>$val){?>
<TR class="tr<?php echo $key%2;?>" id="address<?php echo $val['id'];?>">
	<TD align="center" width="40px"><div id="uid-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('uidinput-<?php echo $val['id'];?>');">
	<?php echo $val['uid'];?>
	</div>
	<input class="hideinput" type="text" id="uidinput-<?php echo $val['id'];?>" ondblclick="confirmValue('address',this.value,'uidinput-<?php echo $val['id'];?>','id');"></TD>
	<TD align="center" width="70px"><div id="username-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('usernameinput-<?php echo $val['id'];?>');">
	<?php echo $val['username'];?>
	</div>
	<input class="hideinput" type="text" id="usernameinput-<?php echo $val['id'];?>" ondblclick="confirmValue('address',this.value,'usernameinput-<?php echo $val['id'];?>','id');"></TD>
	<TD  width="70px"><div id="telphone-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('telphoneinput-<?php echo $val['id'];?>');">
	<?php echo $val['telphone'];?>
	</div>
	<input class="hideinput" type="text" id="telphoneinput-<?php echo $val['id'];?>" ondblclick="confirmValue('address',this.value,'telphoneinput-<?php echo $val['id'];?>','id');"></TD>
	<TD width="150px"><div id="address-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('addressinput-<?php echo $val['id'];?>');">
	<?php echo $val['address'];?>
	</div>
	<textarea class="hideinput"  id="addressinput-<?php echo $val['id'];?>" ondblclick="confirmValue('address',this.value,'addressinput-<?php echo $val['id'];?>','id');"></textarea></TD>
	
	<TD align="center" width="80px"><A HREF="javascript:deleteVal('address','<?php echo $val['id'];?>','address<?php echo $val['id'];?>','id')">删除</A></TD>
</TR>
<?php }?>
</TABLE>
</div>
<ul class="page"><?php echo $pageinfo;?></ul>

</body>
</html>
