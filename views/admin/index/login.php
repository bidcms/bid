<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" >
<title><?php echo $GLOBALS['setting']['site_title'];?>-后台管理</title>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />

<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/login.css" type="text/css" />

</head>

<body>
<div class="page">

	<div class="header">
		<div class="logo">
		</div>
		<div class="toplink">
			<a href="<?php echo SITE_ROOT;?>" target="_blank">网站首页</a>

			<a href="http://bidcms.com/bbs" target="_blank">技术论坛</a>
			
			<a href="http://bidcms.com/bbs" target="_blank">帮助</a>
		</div>
	</div>

<div class="content">

		<!--主要内容-->
		<div id="divTheme" class="main">

			<!--介绍-->
			<div id="divText" class="intro intro-noico">				<dl>					<dt><a target="_blank" href="http://www.guwanpai.com">Bid竞拍系统，和你一起站在电子商务最前沿</a></dt>					<dd>国内首家开源竞拍程序</dd><dd>采用流行PHP+Mysql，享受紧张刺激的购物体验</dd>				</dl>			</div>
			
			<!--登录框-->
			<div id="divLogin" class="login login-1">
				<div class="tabs">
					<ul>
						<li  class="tab tab-3" id="tab-3"><i class="ico-focus for for-3"></i>管理员登录</li>
					</ul>
				</div>
				<SCRIPT LANGUAGE="JavaScript">
				<!--
					function checkform(theform)
					{
						if(theform.username.value=='')
						{
							alert('用户名不能为空');
							return false;
						}
						if(theform.password.value=='')
						{
							alert('密码不能为空');
							return false;
						}
					}
				//-->
				</SCRIPT>
				<div class="panel">
					<iframe src="" style="display:none;" name="login"></iframe>
					<form method="post" target="login" action="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=login" onsubmit="return checkform(this);" id="loginform" name="loginform">
						<input type="hidden" value="1" name="commit">
						<div class="fi" id="uName" style="position:relative;">
							<label class="lb for for-1">用户名</label>
							<input type="text" maxlength="50" name="username" class="ipt-t" tabindex="1" id="idInput">
							验证码
							<DIV  id="showcode" CLASS="" style="position:absolute;top:0px;left:235px;display:none;">
								<SCRIPT LANGUAGE="JavaScript">
								<!--
									function changcode(obj)
									{
										obj.src="tools/showimgcode.php?"+Math.random()
									}
								//-->
								</SCRIPT>
								<img src="tools/showimgcode.php" onclick="changcode(this);"/>
							</DIV>
							
						</div>
						<div class="fi">
							<label class="lb">密　码</label>
							<input type="password"  name="password" id="user_pass" class="ipt-t" tabindex="2" id="pwdInput"><input class="ipt-t" size="6" type="text" name="imgcode" maxlength="6" style="ime-mode:disabled;width:60px;" onfocus="document.getElementById('showcode').style.display='';this.select();"  value="" tabindex="3"/>
 						</div>
						
						<div class="fi fi-nolb">
							<button  type="submit" tabindex="6" onmouseup="this.className='btn btn-login'" onmouseout="this.className='btn btn-login'" onmousedown="this.className+=' btn-login-active'" onmouseover="this.className+=' btn-login-hover'" class="btn btn-login">登 录</button>
							<a target="_self" href="" tabindex="8" class="btn btn-reg for for-1">返回首页</a>
						</div>
					</form>

					<div class="ext for for-1">
						<iframe src="http://bidcms.com/notice.php" frameborder="0" scrolling="no" height="60px"></iframe>
					</div>
				</div>

			</div>
		</div>
	</div>


</div>

</body>
</html>