<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<title>站点信息管理</title>
</head>
<body>

<div id="man_zone">
   <form enctype="multipart/form-data" action="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=settingdata" method="post">
	<INPUT TYPE="hidden" NAME="commit" value="1">
	<INPUT TYPE="hidden" NAME="dotype" value="site">
  <table width="99%" border="0" align="center"  cellpadding="3" cellspacing="1" class="table_style">
 
    <tr>
      <td width="18%" class="left_title_1"><span class="left-title">网站名称：</span></td>
      <td width="82%"><INPUT TYPE="text" class="normal_txt" NAME="site_title" id="title"  size="70" value="<?php echo $GLOBALS['setting']['site_title'];?>"></td>
    </tr>
	 <tr>
      <td width="18%" class="left_title_1"><span class="left-title">网站logo：</span></td>
      <td width="82%">
	  <INPUT TYPE="file" NAME="site_logo"  value="">
	  <?php if($GLOBALS['setting']['site_logo']){?><img src="<?php echo $GLOBALS['setting']['site_logo'];?>"/><?php }?></td>
    </tr>
	 <tr>
      <td width="18%" class="left_title_1"><span class="left-title">网站地址：</span></td>
      <td width="82%"><INPUT TYPE="text" class="normal_txt" NAME="site_url" id="charset"  size="50" value="<?php echo SITE_ROOT;?>">(后面不要加 "/")</td>
    </tr>
	<tr>
      <td width="18%" class="left_title_1"><span class="left-title">网站图片地址：</span></td>
      <td width="82%"><INPUT TYPE="text" class="normal_txt" NAME="site_imgurl" id="title"  size="70" value="<?php echo $GLOBALS['setting']['site_imgurl'];?>">用与图片绝对路径</td>
    </tr>
	<tr>
      <td width="18%" class="left_title_1"><span class="left-title">网站后台地址：</span></td>
      <td width="82%"><INPUT TYPE="text" class="normal_txt" NAME="adminpath" id="charset"  size="50" value="<?php echo $GLOBALS['setting']['adminpath'];?>">(默认为admin)</td>
    </tr>
    <tr>
      <td class="left_title_1">缓存目录：</td>
      <td><INPUT TYPE="text" class="normal_txt" name="site_cache_dir" VALUE="<?php echo $GLOBALS['setting']['site_cache_dir'];?>" >
	  <?php if(is_dir(ROOT_PATH.'/'.$GLOBALS['setting']['site_cache_dir']) && is_writable(ROOT_PATH.'/'.$GLOBALS['setting']['site_cache_dir'])){?>可写<?php } else{?>不可写请检查<?php }?>
	 </td>
    </tr>
   <tr>
      <td class="left_title_1">文件上传目录：</td>
      <td><INPUT TYPE="text" class="normal_txt"  name="site_upload_dir" VALUE="<?php echo $GLOBALS['setting']['site_upload_dir'];?>" >
	  <?php if(is_dir(ROOT_PATH.'/'.$GLOBALS['setting']['site_upload_dir']) && is_writable(ROOT_PATH.'/'.$GLOBALS['setting']['site_upload_dir'])){?>可写<?php } else{?>不可写请检查<?php }?></td>
    </tr>
	<tr>
      <td width="18%" class="left_title_1"><span class="left-title">邮箱：</span></td>
      <td width="82%"><INPUT TYPE="text" class="normal_txt" NAME="site_email" id="charset"  size="50" value="<?php echo $GLOBALS['setting']['site_email'];?>"></td>
    </tr>
	<tr>
      <td width="18%" class="left_title_1"><span class="left-title">地址：</span></td>
      <td width="82%"><INPUT TYPE="text" class="normal_txt" NAME="site_address" id="charset"  size="50" value="<?php echo $GLOBALS['setting']['site_address'];?>"></td>
    </tr>
	<tr>
      <td width="18%" class="left_title_1"><span class="left-title">工作时间：</span></td>
      <td width="82%"><INPUT TYPE="text" class="normal_txt" NAME="site_worktime" id="charset"  size="50" value="<?php echo $GLOBALS['setting']['site_worktime'];?>"></td>
    </tr>
	 <tr>
      <td width="18%" class="left_title_1"><span class="left-title">客服QQ：</span></td>
      <td width="82%"><INPUT TYPE="text" class="normal_txt" NAME="site_qq" id="charset"  size="50" value="<?php echo $GLOBALS['setting']['site_qq'];?>"></td>
    </tr>
	 <tr>
      <td width="18%" class="left_title_1"><span class="left-title">备案信息：</span></td>
      <td width="82%"><INPUT TYPE="text" class="normal_txt" NAME="site_benai" id="charset"  size="50" value="<?php echo $GLOBALS['setting']['site_benai'];?>"></td>
    </tr>
	<tr>
      <td width="18%" class="left_title_1"><span class="left-title"> 客服电话：</span></td>
      <td width="82%"><INPUT TYPE="text" class="normal_txt" NAME="site_tel" id="charset"  size="50" value="<?php echo $GLOBALS['setting']['site_tel'];?>"></td>
    </tr>
	<tr>
      <td class="left_title_1">底部版权信息：</td>
      <td><textarea style="width:300px;height:100px;" NAME="site_copyright"><?php echo $GLOBALS['setting']['site_copyright'];?></textarea></td>
    </tr>
	<tr>
      <td class="left_title_1">第三方统计代码：</td>
      <td><textarea style="width:300px;height:100px;" NAME="site_tongji"><?php echo $GLOBALS['setting']['site_tongji'];?></textarea></td>
    </tr>
	
	<tr>
      <td></td>
      <td><INPUT TYPE="submit" class="normal_button" value="提交"></td>
    </tr>
  </table>
  </FORM>
</div>
</body>
</html>
