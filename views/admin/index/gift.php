<?php

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>赠品管理</title>
</head>
<STYLE TYPE="text/css">
	
</STYLE>

<body>
<div class="list">
<TABLE cellpadding="1" cellspacing="1" style="width:600px;margin:auto;">
<TR>
	<th>商品标题</th><th>所需积分</th><th>库存</th><th>市场价</th><th style="width:80px;">操作</th>
 </tr>
<?php   foreach($info AS $key => $val){     ?>
<TR class="tr1" id="gift<?php echo $val['id'];?>">
		<TD>
		<div id="subject-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('subjectinput-<?php echo $val['id'];?>');"><?php echo $val['subject'];?>
		</div>
		<textarea class="hideinput" id="subjectinput-<?php echo $val['id'];?>" ondblclick="confirmValue('goods_gift',this.value,'subjectinput-<?php echo $val['id'];?>','id');"></textarea>
		</TD>
		<TD>
		<div id="needcount-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('needcountinput-<?php echo $val['id'];?>');"><?php echo $val['needcount'];?>
		</div>
		<input class="hideinput" id="needcountinput-<?php echo $val['id'];?>" ondblclick="confirmValue('goods_gift',this.value,'needcountinput-<?php echo $val['id'];?>','id');" />
		</TD>
		<TD>
		<div id="giftcount-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('giftcountinput-<?php echo $val['id'];?>');"><?php echo $val['giftcount'];?>
		</div>
		<input class="hideinput" id="giftcountinput-<?php echo $val['id'];?>" ondblclick="confirmValue('goods_gift',this.value,'giftcountinput-<?php echo $val['id'];?>','id');" />
		</TD>
		<TD><?php echo $val['marketprice'];?></TD>
		<td align="center"><a href="javascript:deleteVal('goods_gift','<?php echo $val['id'];?>','gift<?php echo $val['id'];?>')">删除</a>  <a href="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=giftmodify&updateid=<?php echo $val['id']; ?>">修改</a> </td>
	</tr>
<?php  }    ?>
 </table>
 </div>
<ul class="page">
	<?php  echo $pageinfo; ?>
</ul>
</body>
</html>