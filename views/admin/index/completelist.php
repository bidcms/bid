<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>完成项目管理</title>
</head>

<body>
<div class="list">
<TABLE cellpadding="1" cellspacing="1">
<TR>
    <TH>统计期数</TH>
	<TH>参与人数</TH>
	<TH>实际投币</TH>
	<TH>返币</TH>
	<TH>实收</TH>
	<TH>机器人投币</TH>
	<TH>操作</TH>
</TR>
<?php foreach($goodslist as $key=>$val){?>
<TR class="tr<?php echo $key%2;?>" id="goods<?php echo $val['goods_id'];?>">
	
	<TD><A HREF="<?php echo url('index','complete',array('id'=>$val['goods_id']));?>" target="_blank"b><?php echo $val['goods_name'];?>[第<?php echo $val['goods_id'];?>期]</A></TD>
	
	<TD width="60px" align="center"><?php echo $bidcount[$val['goods_id']];?></TD>
	<TD width="60px" align="center"><?php echo $money[$val['goods_id']]['realbid'];?></TD>
	<TD width="60px" align="center"><?php echo $returnmoney[$val['goods_id']];?></TD>

	<TD width="140px" align="center"><?php echo $money[$val['goods_id']]['realbid']-$returnmoney[$val['goods_id']];?></TD>
	<TD width="90px" align="center"><?php echo $money[$val['goods_id']]['autobid'];?></TD>
	<TD width="90px" align="center"><A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=buylog&gid=<?php echo $val['goods_id'];?>">竞拍明细</A></TD>
</TR>
<?php }?>
</TABLE>
</div>

<ul class="page"><?php echo $pageinfo;?></ul>


</body>
</html>
