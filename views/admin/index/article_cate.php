<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>文章类别管理</title>
</head>
<body>
<SCRIPT LANGUAGE="JavaScript">
<!--
	function addcate()
	{
		url="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=article_catemodify";
		$.get(url, function(data){
			$('#cate_area').html(data);
			$('#cate_area').show();
		}); 
	}
	function parseMyCateData(data)
	{
		var objdata=data;
		$('#cate0').after('<TR class="tr1" id="cate'+objdata.id+'"><TD width="180px">'+objdata.catename+'</TD><td></td><td></td><td width="70px" align="center"><A HREF="javascript:deleteVal(\'article_cate\',\''+objdata.id+'\',\'cate'+objdata.id+'\')">删除</A></td></TR>');
		$('#cate_area').hide();
	}
//-->
</SCRIPT>
<div class="list">
<TABLE cellpadding="1" cellspacing="1" style="width:450px;margin:auto;">
<TR>
	<TH>类别</TH>
	<TH>级别</TH>
	<TH>英文</TH>
	<TH>操作</TH>
</TR>
<TR class="tr1" id="cate0">
	<TD width="180px">
	</TD>
	<TD width="180px">
	</TD>
	<TD width="180px">
	</TD>
	<td width="120px" align="center">
	<A HREF="javascript:addcate();">添加</A>
	</td>
</TR>
<?php 
foreach($list as $key=>$val){?>
<TR class="tr1" id="cate<?php echo $val['id'];?>">
	<TD width="180px">
	<div id="catename-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('catenameinput-<?php echo $val['id'];?>');"><?php echo $val['catename'];?>
	</div>
	<input class="hideinput" id="catenameinput-<?php echo $val['id'];?>" ondblclick="confirmValue('article_cate',this.value,'catenameinput-<?php echo $val['id'];?>','id');"/>
	</TD>
	<td width="60px" align="center">
	<div id="issystem-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff0000';" onmouseout="this.style.backgroundColor='';" onclick="updateVal('article_cate','issysteminput-<?php echo $val['id'];?>','id','',['系统','普通']);">
	<?php echo $val['issystem']?'系统':'普通';?>
	</div>
	<INPUT TYPE="text" class="hideinput" id="issysteminput-<?php echo $val['id'];?>" value="<?php echo intval(!$val['issystem']);?>" title='nochange'>
	</td>
	<TD width="180px">
	<div id="cate_en-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('cate_eninput-<?php echo $val['id'];?>');"><?php echo $val['cate_en']?$val['cate_en']:'--';?>
	</div>
	<input class="hideinput" id="cate_eninput-<?php echo $val['id'];?>" ondblclick="confirmValue('article_cate',this.value,'cate_eninput-<?php echo $val['id'];?>','id');"/>
	</TD>
	<td width="70px" align="center">
	<?php if($val['issystem']){?>
	系统分类
	<?php } else{?>
	<A HREF="javascript:deleteVal('article_cate','<?php echo $val['id'];?>','cate<?php echo $val['id'];?>')">删除</A>
	<?php }?>
	</td>
</TR>
<?php }?>
</TABLE>
</div>
<DIV id="cate_area" style="display:none;border:2px solid #ccc;width:300px;position:absolute;left:260px;top:60px;height:120px;background:#fff;z-index:10px;"></DIV>
</body>
</html>
