<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/calendar.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>商品管理</title>
</head>
<body>


<div id="man_zone">
   <form enctype="multipart/form-data" action="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=completemodify" method="post">
	<INPUT TYPE="hidden" NAME="commit" value="1">
  <INPUT TYPE="hidden" NAME="updateid" value="<?php echo $goods['goods_id'];?>">
  <table width="99%" border="0" align="center"  cellpadding="3" cellspacing="1" class="table_style">
 
    <tr>
      <td width="18%" class="left_title_1"><span class="left-title">商品名称：</span></td>
      <td width="82%"><INPUT TYPE="text" class="normal_txt"  NAME="goods_name" id="subject"  size="70" value="<?php echo $goods['goods_name'];?>"><SPAN CLASS="" id="subjecterror"></SPAN></td>
    </tr>
	 <tr>
      <td class="left_title_2">开始日期：</td>
      <td> <INPUT TYPE="text" class="normal_txt"  NAME="starttime"  value="<?php echo date('Y-m-d H:i:s',$goods['starttime']?$goods['starttime']:time()+86400);?>" onfocus="HS_setDate(this);"></td>
    </tr>
    <tr>
      <td class="left_title_2">结束日期：</td>
      <td> <INPUT TYPE="text" class="normal_txt"  NAME="lasttime"  value="<?php echo date('Y-m-d H:i:s',$goods['lasttime']?$goods['lasttime']:time()+172800);?>" onfocus="HS_setDate(this);"></td>
    </tr>
	
	<tr>
      <td class="left_title_1" valign="top">缩略图：<INPUT TYPE="button" VALUE="+" class="normal_button" ONCLICK="addmthumb();"></td>
      <td>
	  <div id="thumbarea">
	  <div><INPUT TYPE="file" NAME="local_thumb[]" class="normal_button"></div>
	  </div>
	   <?php 
	   if($goods['thumb']){
	   $thumb=explode(',',$goods['thumb']);
	   foreach($thumb as $k=>$v){
	   ?>
	   <div style="float:left;width:100px;height:140px;overflow:hidden;text-align:center;" id="thumb<?php echo $k;?>"><img onclick="window.open(this.src,'','');" src="<?php echo $v;?>" height='100px'/><br/><INPUT TYPE="button" VALUE="删除" ONCLICK="deletethumb('<?php echo $v;?>','thumb<?php echo $k;?>');"></div>
	   <?php }}?>
	  </td>
    </tr>
	<tr>
      <td class="left_title_1">原价：</td>
      <td>￥<INPUT TYPE="text" class="normal_txt"  NAME="marketprice" value="<?php echo $goods['marketprice'];?>"></td>
    </tr>
	<tr>
      <td class="left_title_1">现价：</td>
      <td>￥<INPUT TYPE="text" class="normal_txt"  NAME="nowprice" value="<?php echo $goods['nowprice'];?>"></td>
    </tr>
	<tr>
      <td class="left_title_1">当前竞价会员：</td>
      <td><INPUT TYPE="text" class="normal_txt"  NAME="currentuser" value="<?php echo $goods['currentuser'];?>"></td>
    </tr>
	<tr>
      <td class="left_title_1">当前竞价会员id：</td>
      <td><INPUT TYPE="text" class="normal_txt"  NAME="currentuid" value="<?php echo $goods['currentuid'];?>"></td>
    </tr>
	<tr>
      <td class="left_title_1">价格变动幅度：</td>
      <td><INPUT TYPE="text" class="normal_txt"  NAME="diffprice" value="<?php echo $goods['diffprice'];?>"> 小于0为降价,大于0为升价，针对特殊商品，如果采用项目设置中的，请留空</td>
    </tr>
	<tr>
      <td class="left_title_1">每次扣除金币：</td>
      <td><INPUT TYPE="text" class="normal_txt"  NAME="diffmoney" value="<?php echo $goods['diffmoney'];?>"> 针对特殊商品，如果采用项目设置中的，请留空</td>
    </tr>
	<tr>
      <td class="left_title_1">每次奖励积分：</td>
      <td><INPUT TYPE="text" class="normal_txt"  NAME="diffscore" value="<?php echo $goods['diffscore'];?>"> 针对特殊商品，如果采用项目设置中的，请留空</td>
    </tr>
	<tr>
      <td class="left_title_1">特别提示：</td>
      <td><textarea NAME="content" cols="40" rows="8"><?php echo $goods['content'];?></textarea>
	  </td>
    </tr>
	<tr>
      <td class="left_title_1" valign="top">详情：</td>
      <td>
	 <DIV ID="" CLASS="">
		 <?php echo edit($goods['details'],'details','details');?>
	  </DIV>
	  </td>
    </tr>
	<tr>
      <td></td>
      <td><INPUT TYPE="submit" class="normal_button" value="提交"></td>
    </tr>
  </table>
  </FORM>
</div>
</body>
</html>
