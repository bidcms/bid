<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/calendar.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>商品管理</title>
</head>
<body>

<div id="man_zone">
 <form enctype="multipart/form-data" action="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=goodsmodify" method="post">
  <INPUT TYPE="hidden" NAME="<?php echo $cname;?>" value="1"/>
  <INPUT TYPE="hidden" NAME="updateid" value="<?php echo $goods['goods_id'];?>"/>
  <table width="99%" border="0" align="center"  cellpadding="3" cellspacing="1" class="table_style">
 
    <tr>
      <td width="18%" class="left_title_1"><span class="left-title">商品名称：</span></td>
      <td width="82%" colspan="3"><INPUT TYPE="text" class="normal_txt"  NAME="goods_name" id="subject"  size="50" value="<?php echo $goods['goods_name'];?>"/><INPUT TYPE="checkbox" NAME="isfree" value="1" <?php if($goods['isfree']){?>checked<?php }?>/>新手体验商品？</td>
    </tr>
	<tr>
      <td class="left_title_1">发布人：</td>
      <td colspan="3"><INPUT TYPE="text" class="normal_txt"  NAME="hostuser" value="<?php echo $goods['hostuser'];?>" size="50"/><INPUT TYPE="checkbox" NAME="ispassed" value="1" <?php if($goods['ispassed']){?>checked<?php }?>/>通过审核？</td>
    </tr>
	 <tr>
      <td width="18%" class="left_title_1"><span class="left-title">所属分类:</span></td>
      <td width="82%" colspan="3">
	  <SELECT NAME="cateid">
		<?php if($goods['cateid']){?>
          <option value="<?php echo $goods['cateid'];?>"><?php echo $GLOBALS['cate'][$goods['cateid']]['catename'];?></option>
        <?php }?>
		<?php foreach($GLOBALS['cate'] as $k=>$v){?>
		<option value="<?php echo $v['id'];?>"><?php echo $v['catename'];?></option>
		<?php }?>
	  </SELECT>
	  </td>
    </tr>
	 <tr>
      <td class="left_title_1">开始日期：</td>
      <td style="width:210px;"> <INPUT TYPE="text" class="normal_txt" id="starttime" NAME="starttime"  value="<?php echo date('Y-m-d H:i:s',$goods['starttime']?$goods['starttime']:time()+86400);?>"/><INPUT TYPE="button" VALUE="选择" ONCLICK="HS_setDate(document.getElementById('starttime'))" class="normal_button"/></td>
    
      <td class="left_title_1" style="width:90px;">结束日期：</td>
      <td> <INPUT TYPE="text" class="normal_txt"  NAME="lasttime" id="lasttime" value="<?php echo date('Y-m-d H:i:s',$goods['lasttime']?$goods['lasttime']:time()+172800);?>"/><INPUT TYPE="button" VALUE="选择" ONCLICK="HS_setDate(document.getElementById('lasttime'))" class="normal_button"/></td>
    </tr>
	
	<tr>
      <td class="left_title_1" valign="top">缩略图：<INPUT TYPE="button" VALUE="+" class="normal_button" ONCLICK="addmthumb();"/></td>
      <td colspan="3">
	  <div id="thumbarea">
	  <div><INPUT TYPE="file" NAME="local_thumb[]" class="normal_button"/></div>
	  </div>
	   <?php 
	   if($goods['thumb']){
	   $thumb=explode(',',$goods['thumb']);
	   foreach($thumb as $k=>$v){
	   ?>
	   <div style="float:left;width:100px;height:140px;overflow:hidden;text-align:center;" id="thumb<?php echo $k;?>">
         <img onclick="window.open(this.src,'','');" src="<?php echo $v;?>" height='100px'/>
         <br/>
         <INPUT TYPE="button" VALUE="删除" ONCLICK="deletethumb('<?php echo $v;?>','thumb<?php echo $k;?>');"/>
       </div>
	   <?php }}?>
	  </td>
    </tr>
	<tr>
      <td class="left_title_1">原价：</td>
      <td>￥<INPUT TYPE="text" class="normal_txt"  NAME="marketprice" value="<?php echo $goods['marketprice'];?>"/></td>
    
      <td class="left_title_1">现价：</td>
      <td>￥<INPUT TYPE="text" class="normal_txt"  NAME="nowprice" value="<?php echo $goods['nowprice'];?>"/></td>
    </tr>
	<tr>
      <td class="left_title_1">限次竞拍：</td>
      <td colspan="3">
	  <SCRIPT LANGUAGE="JavaScript">
	  <!--
		function checklimit() {
		    if($('#islimit').is(':checked')) {
				$('#limitnumber').removeAttr('disabled');
			} else {
				$('#limitnumber').attr('disabled','disabled');
			}
		}
	  //-->
	  </SCRIPT>
	  <INPUT TYPE="text" class="normal_txt"  NAME="limitnumber" id="limitnumber" value="<?php echo $goods['limitnumber'];?>" <?php echo $goods['islimit']?'':'disabled="disabled"';?>/>
      <INPUT TYPE="checkbox" NAME="islimit" id="islimit" value="1" <?php echo $goods['islimit']?'checked':'';?> onclick="checklimit();"/> 每个会员最多参加次数，为使用竞拍公平,请选择限次。</td>
    </tr>
	<tr>
      <td class="left_title_1">屏蔽会员id：</td>
      <td colspan="3"><INPUT TYPE="text" class="normal_txt"  NAME="shutuid" value="<?php echo $goods['shutuid'];?>" size="50"/> 防止恶意竞拍,多会会员请和半角逗号隔开。</td>
    </tr>
	<tr>
      <td class="left_title_1">当前竞价会员：</td>
      <td><INPUT TYPE="text" class="normal_txt"  NAME="currentuser" value="<?php echo $goods['currentuser'];?>"/></td>
      <td class="left_title_1">当前竞价会员id：</td>
      <td><INPUT TYPE="text" class="normal_txt"  NAME="currentuid" value="<?php echo $goods['currentuid'];?>"/></td>
    </tr>
	<tr>
      <td class="left_title_1">商品返利：</td>
      <td><INPUT TYPE="text" class="normal_txt"  NAME="backmoney" value="<?php echo $goods['backmoney'];?>"/>% 返利百分比</td>
      <td class="left_title_1">运费：</td>
      <td><INPUT TYPE="text" class="normal_txt"  NAME="yfeemoney" value="<?php echo $goods['yfeemoney'];?>"/> 元</td>
    </tr>
	<tr>
      <td class="left_title_1">倒计时多少秒内：</td>
      <td><INPUT TYPE="text" class="normal_txt"  NAME="difftime" value="<?php echo $goods['difftime'];?>"/>默认10秒 </td>
      <td class="left_title_1">时间增加多少：</td>
      <td><INPUT TYPE="text" class="normal_txt"  NAME="addtime" value="<?php echo $goods['addtime'];?>"/> 默认10秒</td>
    </tr>
	<tr>
      <td class="left_title_1">价格变动幅度：</td>
      <td colspan="3"><INPUT TYPE="text" class="normal_txt"  NAME="diffprice" value="<?php echo $goods['diffprice'];?>"/> 小于0为降价,大于0为升价，针对特殊商品，如果采用项目设置中的，请留空</td>
    </tr>
	<tr>
      <td class="left_title_1">每次扣除金币：</td>
      <td colspan="3"><INPUT TYPE="text" class="normal_txt"  NAME="diffmoney" value="<?php echo $goods['diffmoney'];?>"/> 针对特殊商品，如果采用项目设置中的，请留空</td>
    </tr>
	<tr>
      <td class="left_title_1">每次奖励积分：</td>
      <td colspan="3"><INPUT TYPE="text" class="normal_txt"  NAME="diffscore" value="<?php echo $goods['diffscore'];?>"/> 针对特殊商品，如果采用项目设置中的，请留空</td>
    </tr>
	<tr>
      <td class="left_title_1">机器人设置：</td>
      <td colspan="3"><INPUT TYPE="text" class="normal_txt"  NAME="autobuy_time" value="<?php echo $goods['autobuy_time'];?>"/> 倒计时多少秒参与,为0表示不开启机器人</td>
	</tr>
	<tr>
      <td class="left_title_1">机器人竞争次数：</td>
      <td colspan="3"><INPUT TYPE="text" class="normal_txt"  NAME="auto_times" value="<?php echo $goods['auto_times'];?>"/> 机器人互抢次数</td>
	</tr>
	<tr>
      <td class="left_title_1">机器人最高出价：</td>
      <td colspan="3">
        <INPUT TYPE="text" class="normal_txt"  NAME="autobuy_money" value="<?php echo sprintf('%.2f',$goods['autobuy_money']);?>"/> 
        此商品实际收入总<?php echo !empty($GLOBALS['setting']['site_money_name'])?$GLOBALS['setting']['site_money_name']:'金币';?>数达到多少后自动退出
	  </td>
	</tr>
	<tr>
      <td class="left_title_1">特别提示：</td>
      <td colspan="3"><textarea NAME="content" cols="40" rows="8"><?php echo $goods['content'];?></textarea>
	  </td>
    </tr>
	<tr>
      <td class="left_title_1" valign="top">详情：</td>
      <td colspan="3">
	 <DIV ID="" CLASS="">
		 <?php echo $this->edit($goods['details'],'details','details');?>
	  </DIV>
	  </td>
    </tr>
	<tr>
      <td></td>
      <td colspan="3"><INPUT TYPE="submit" class="normal_button" value="提交"/></td>
    </tr>
  </table>
  </form>
</div>
</body>
</html>
