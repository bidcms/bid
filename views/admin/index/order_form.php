<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/calendar.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>订单管理</title>
</head>
<body>


<div id="man_zone">
   <form action="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=ordermodify" method="post">
	<INPUT TYPE="hidden" NAME="commit" value="1">
  <INPUT TYPE="hidden" NAME="updateid" value="<?php echo $order['id'];?>">
  <table width="99%" border="0" align="center"  cellpadding="3" cellspacing="1" class="table_style">
 
    <tr>
      <td width="18%" class="left_title_1"><span class="left-title">商品名称：</span></td>
      <td width="82%"><?php echo $order['goods_name'];?></td>
    </tr>
	 <tr>
      <td width="18%" class="left_title_1"><span class="left-title">订单号：</span></td>
      <td width="82%"><?php echo $order['order_no'];?></td>
     </tr>
    <tr>
      <td width="18%" class="left_title_1"><span class="left-title">状态：</span></td>
      <td width="82%">
	   <select name="order_status">
	   <option value="<?php echo $order['order_status'];?>"><?php echo $GLOBALS['order_status'][$order['order_status']];?></option>
		<?php foreach($GLOBALS['order_status'] as $k=>$v){?>
		<option value="<?php echo $k;?>"><?php echo $v;?></option>
		<?php }?>
		</select>
	  </td>
     </tr>
	  <tr>
      <td class="left_title_2">下单日期：</td>
      <td><?php echo date('Y-m-d H:i:s',$order['updatetime']);?></td>
    </tr>
	 <tr>
      <td class="left_title_2">付款日期：</td>
      <td><?php echo date('Y-m-d H:i:s',$order['paytime']);?></td>
    </tr>
    <tr>
      <td class="left_title_2">发货日期：</td>
      <td> <INPUT TYPE="text" class="normal_txt"  NAME="sendtime"  value="<?php echo $order['sendtime']?date('Y-m-d H:i:s',$order['sendtime']):'';?>" onfocus="HS_setDate(this);"></td>
    </tr>
	
	<tr>
      <td class="left_title_1">金额：</td>
      <td>￥<INPUT TYPE="text" class="normal_txt"  NAME="total_fee" value="<?php echo $order['total_fee'];?>"></td>
    </tr>
	<tr>
      <td class="left_title_1">类型：</td>
      <td><?php echo $GLOBALS['order_type'][$order['order_type']];?></td>
    </tr>
	<tr>
      <td class="left_title_1">收货地址：</td>
      <td><?php echo $order['address'];?></td>
    </tr>
	 <tr>
      <td class="left_title_2">用户信息：</td>
      <td><?php echo implode(' , ',$userinfo);?></td>
    </tr>
	<tr>
      <td class="left_title_1">用户备注：</td>
      <td><?php echo $order['content'];?></td>
    </tr>
	<tr>
      <td class="left_title_1">发货备注：</td>
      <td><textarea NAME="sendcontent" cols="40" rows="8"><?php echo $order['sendcontent'];?></textarea></td>
    </tr>
	
	<tr>
      <td class="left_title_1">取消备注：</td>
      <td><textarea NAME="cancelcontent" cols="40" rows="8"><?php echo $order['cancelcontent'];?></textarea>
	  </td>
    </tr>
	<tr>
      <td></td>
      <td><INPUT TYPE="submit" class="normal_button" value="提交"></td>
    </tr>
  </table>
  </FORM>
</div>
</body>
</html>
