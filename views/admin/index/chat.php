<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>留言管理</title>
</head>
<STYLE TYPE="text/css">
	
</STYLE>
<body>
<SCRIPT LANGUAGE="JavaScript">
<!--

function addchat()
	{
		url="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=chatmodify";
		$.get(url, function(data){
			$('#chat_area').html(data);
			$('#chat_area').show();
		}); 
	}
function parsechatData(data)
{
	var objdata=data;
	if(objdata.dotype=='add')
	{
		$('#chat0').after('<TR class="tr1" id="chat'+objdata.id+'"><TD width="80px">系统信息</TD><TD>系统信息</TD><TD width="140px" align="center"></TD><td width="60px" align="center">审核</td><td width="60px" align="center">系统</td><TD align="center" width="100px"><A HREF="javascript:deleteVal(\'chat\',\''+objdata.id+'\',\'chat'+objdata.id+'\',\'id\')">删除</A></TD></TR><tr class="tr0"><td colspan="6">'+objdata.content+'</td></tr>');
		$('#chat_area').hide();
	}
}
//-->
</SCRIPT>

<div class="list">
<TABLE cellpadding="1" cellspacing="1">
<TR>
	<TH>发布人</TH>
	<TH>商品名</TH>
	<TH>发表时间</TH>
	<TH>状态</TH>
	<TH>级别</TH>
	<TH>操作</TH>
</TR>
<TR id="chat0" class="tr1">
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td align="center"><A HREF="javascript:addchat();">添加系统信息</A></td>
</TR>
<?php foreach($chatlist as $key=>$val){?>
<TR class="tr1" id="chat<?php echo $val['id'];?>">
	<TD width="80px" ><?php echo $val['username'];?></TD>
	<TD><?php echo $val['goods_name'];?></TD>
	<TD width="140px" align="center"><?php echo date('Y-m-d H:i:s',$val['updatetime']);?></font></TD>
	<td width="60px" align="center">
	<div id="ispassed-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff0000';" onmouseout="this.style.backgroundColor='';" onclick="updateVal('chat','ispassedinput-<?php echo $val['id'];?>','id','',['审核','未审核']);">
	<?php echo $val['ispassed']?'审核':'未审核';?>
	</div>
	<INPUT TYPE="text" class="hideinput" id="ispassedinput-<?php echo $val['id'];?>" value="<?php echo intval(!$val['ispassed']);?>" title='nochange'>
	</td>
	<td width="60px" align="center">
	<div id="issystem-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff0000';" onmouseout="this.style.backgroundColor='';" onclick="updateVal('chat','issysteminput-<?php echo $val['id'];?>','id','',['系统','个人']);">
	<?php echo $val['issystem']?'系统':'个人';?>
	</div>
	<INPUT TYPE="text" class="hideinput" id="issysteminput-<?php echo $val['id'];?>" value="<?php echo intval(!$val['issystem']);?>" title='nochange'>
	</td>
	<TD align="center" width="100px"><A HREF="javascript:deleteVal('chat','<?php echo $val['id'];?>','chat<?php echo $val['id'];?>','id')">删除</A></TD>
</TR>
<tr class="tr0" id="chat<?php echo $val['id'];?>next"><td colspan="6"><?php echo $val['content'];?></td></tr>
<?php }?>
</TABLE>
</div>

<ul class="page"><?php echo $pageinfo;?></ul>
<DIV id="chat_area" style="display:none;border:2px solid #ccc;width:400px;position:absolute;left:260px;top:60px;background:#fff;z-index:10px;"></DIV>
</body>
</html>
