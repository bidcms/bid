<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>团购管理</title>
</head>
<STYLE TYPE="text/css">
	
</STYLE>
<body>
<FORM METHOD=POST ACTION="">
	会员名：<INPUT TYPE="text" NAME="username"><INPUT TYPE="hidden" NAME="usertype" value="<?php echo $_GET['act'];?>"><INPUT TYPE="submit" value="搜索">
</FORM>
<div class="list">
<TABLE cellpadding="1" cellspacing="1">
<TR>
	<TH>用户名</TH>
	<TH>邮箱</TH>
	<TH><?php echo $GLOBALS['setting']['site_money_name'];?></TH>
	<TH>积分</TH>
	<TH>验证</TH>
	<TH>代金券</TH>
	<TH>注册时间</TH>
	<TH>类型</TH>
	<TH>操作</TH>
</TR>

<?php foreach($userlist as $key=>$val){?>
<TR class="tr<?php echo $key%2;?>" id="user<?php echo $val['uid'];?>">
	<TD width="60px">
	<div id="username-<?php echo $val['uid'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('usernameinput-<?php echo $val['uid'];?>');"><?php echo $val['username'];?>
	</div>
	<textarea class="hideinput" id="usernameinput-<?php echo $val['uid'];?>" ondblclick="confirmValue('user',this.value,'usernameinput-<?php echo $val['uid'];?>','uid');"></textarea>
	</TD>

	<TD width="60px">
	<div id="email-<?php echo $val['uid'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('emailinput-<?php echo $val['uid'];?>');"><?php echo $val['email'];?>
	</div>
	<textarea class="hideinput" id="emailinput-<?php echo $val['uid'];?>" ondblclick="confirmValue('user',this.value,'emailinput-<?php echo $val['uid'];?>','uid');"></textarea>
	</TD>

	
	<TD align="center" width="70px"><div id="money-<?php echo $val['uid'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('moneyinput-<?php echo $val['uid'];?>');">
	<?php echo $val['money'];?>
	</div>
	<input class="hideinput" type="text" id="moneyinput-<?php echo $val['uid'];?>" ondblclick="confirmValue('user',this.value,'moneyinput-<?php echo $val['uid'];?>','uid');"></TD>
	
	<TD  align="center" width="70px"><div id="score-<?php echo $val['uid'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('scoreinput-<?php echo $val['uid'];?>');"><?php echo $val['score'];?>
	</div>
	<input TYPE="text" class="hideinput" id="scoreinput-<?php echo $val['uid'];?>" ondblclick="confirmValue('user',this.value,'scoreinput-<?php echo $val['uid'];?>','uid');"/></TD>
	<td width="60px" align="center">
	<div id="verification-<?php echo $val['uid'];?>" onmouseover="this.style.backgroundColor='#ff0000';" onmouseout="this.style.backgroundColor='';" onclick="updateVal('user','verificationinput-<?php echo $val['uid'];?>','uid','',['是','否']);">
	<?php echo $val['verification']?'是':'否';?>
	</div>
	<INPUT TYPE="text" class="hideinput" id="verificationinput-<?php echo $val['uid'];?>" value="<?php echo intval(!$val['verification']);?>" title='nochange'>
	</td>
	<TD  align="center" width="70px"><div id="usercard-<?php echo $val['uid'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('usercardinput-<?php echo $val['uid'];?>');"><?php echo $val['usercard'];?>
	</div>
	<input TYPE="text" class="hideinput" id="usercardinput-<?php echo $val['uid'];?>" ondblclick="confirmValue('user',this.value,'usercardinput-<?php echo $val['uid'];?>','uid');"/></TD>

	

	<TD  align="center" width="100px"><?php echo date('Y-m-d',$val['updatetime']);?></TD>

	<td align="center" width="70px">
	<div id="usertype-<?php echo $val['uid'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('usertypeinput-<?php echo $val['uid'];?>');"><?php echo $val['usertype'];?>
</div>

<select class="hideinput" id="usertypeinput-<?php echo $val['uid'];?>" onchange="confirmValue('user',this.value,'usertypeinput-<?php echo $val['uid'];?>','uid');">
<option value="normaluser"> </option>
<?php foreach($GLOBALS['user_type'] as $k=>$v){?>
<option value="<?php echo $k;?>"><?php echo $v;?></option>
<?php }?>
</select>
	</td>
	<TD align="center" width="130px">
	<A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=usermodify&updateid=<?php echo $val['uid'];?>">修改</A>
	<?php if($GLOBALS['session']->get('adminid')!=$val['uid']){?><A HREF="javascript:deleteVal('user_base','<?php echo $val['uid'];?>','user<?php echo $val['uid'];?>','uid')">删除</A><?php }?>  <A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=buylog&uid=<?php echo $val['uid'];?>">竞拍记录</A>
	</TD>
</TR>
<?php }?>
</TABLE>
</div>
<ul class="page"><?php echo $pageinfo;?></ul>

</body>
</html>
