<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<SCRIPT LANGUAGE="JavaScript" src="https://libs.cdnjs.net/jquery.form/4.2.2/jquery.form.min.js"></SCRIPT>
<title>类别管理</title>
</head>
<body>
<SCRIPT LANGUAGE="JavaScript">
<!--
function addcate()
{
	url="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=catemodify";
	$.get(url, function(data){
		$('#cate_area').html(data);
		$('#cate_area').show();
	}); 
}
//-->
</SCRIPT>
<div class="list">
<TABLE cellpadding="1" cellspacing="1" style="width:450px;margin:auto;">
<TR>
	<TH>类别</TH>
	<TH>自定义分组</TH>
	<TH>排序</TH>
	<TH>操作</TH>
</TR>
<TR class="tr1" id="cate">
	<TD width="180px">
	</TD>
	<TD width="180px">
	</TD><TD width="180px">
	</TD>
	<td width="70px" align="center">
	<A HREF="javascript:addcate();">添加</A>
	</td>
</TR>
<?php 
if(!empty($cate)){
foreach($cate as $key=>$val){?>
<TR class="tr1" id="cate<?php echo $val['id'];?>">
	<TD width="180px">
	<div id="catename-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('catenameinput-<?php echo $val['id'];?>');"><?php echo $val['catename'];?>
	</div>
	<input class="hideinput" id="catenameinput-<?php echo $val['id'];?>" ondblclick="confirmValue('goods_cate',this.value,'catenameinput-<?php echo $val['id'];?>','id');"/>
	</TD>
	<TD width="180px">
	<div id="custom_group-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('custom_groupinput-<?php echo $val['id'];?>');"><?php echo $val['custom_group']?$val['custom_group']:'--';?>
	</div>
	<input class="hideinput" id="custom_groupinput-<?php echo $val['id'];?>" ondblclick="confirmValue('goods_cate',this.value,'custom_groupinput-<?php echo $val['id'];?>','id');"/>
	</TD>
	<TD width="180px">
	<div id="sortorder-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('sortorderinput-<?php echo $val['id'];?>');"><?php echo $val['sortorder'];?>
	</div>
	<input class="hideinput" id="sortorderinput-<?php echo $val['id'];?>" ondblclick="confirmValue('goods_cate',this.value,'sortorderinput-<?php echo $val['id'];?>','id');"/>
	</TD>
	<td width="70px" align="center">
	<A HREF="javascript:deleteVal('goods_cate','<?php echo $val['id'];?>','cate<?php echo $val['id'];?>')">删除</A>
	</td>
</TR>
<?php }}?>
</TABLE>
</div>
<DIV id="cate_area" style="display:none;border:2px solid #ccc;width:300px;position:absolute;left:260px;top:60px;background:#fff;z-index:10px;"></DIV>
<script type="text/javascript">
	$('#cate_area').on('click','#submit',function(){
		$('#cate_area').find('#form1').ajaxSubmit({
			success:function(data){
				if(data.code == 0){
					var objdata=data.data;
					$('#cate').after('<TR class="tr1" id="cate'+objdata.id+'"><TD width="180px">'+objdata.catename+'</TD><TD width="180px">'+objdata.custom+'</TD><TD width="180px">0</TD><td width="70px" align="center"><A HREF="javascript:deleteVal(\'cate\',\''+objdata.id+'\',\'cate'+objdata.id+'\')">删除</A></td></TR>');
					$('#cate_area').hide();
				}
				
			}
		}); 
	});
	 
</script>
</body>
</html>
