<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>评论管理</title>
</head>
<STYLE TYPE="text/css">
	
</STYLE>
<body>

<SCRIPT LANGUAGE="JavaScript">
<!--

function modifyguestbook(id)
	{
		url="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=guestbookmodify&updateid="+id;
		$.get(url, function(data){
			$('#guestbook_area').html(data);
			$('#guestbook_area').show();
		}); 
	}
function parseMyGuestbookData(data)
{
	var objdata=data;
	if(objdata.dotype=='update')
	{
		$('#guestbook_area').hide();
	}
}
//-->
</SCRIPT>
<div class="list">
<TABLE cellpadding="1" cellspacing="1">
<TR>
    <TH>标题</TH>
	<TH>提问人</TH>
	<TH>发表时间</TH>
	<TH>状态</TH>
	<TH>操作</TH>
</TR>
<?php foreach($guestbooklist as $key=>$val){?>
<TR class="tr0" id="guestbook<?php echo $val['id'];?>">
	
	<TD><?php echo $val['title'];?></TD>
	<TD><?php echo $val['username'];?></TD>
	<TD width="140px" align="center"><?php echo date('Y-m-d H:i:s',$val['updatetime']);?></font></TD>
	<td width="60px" align="center">
	<div id="ispassed-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff0000';" onmouseout="this.style.backgroundColor='';" onclick="updateVal('guestbook','ispassedinput-<?php echo $val['id'];?>','id','',['审核','未审核']);">
	<?php echo $val['ispassed']?'审核':'未审核';?>
	</div>
	<INPUT TYPE="text" class="hideinput" id="ispassedinput-<?php echo $val['id'];?>" value="<?php echo intval(!$val['ispassed']);?>" title='nochange'>
	</td>
	<TD align="center" width="100px"><A HREF="javascript:deleteVal('guestbook','<?php echo $val['id'];?>','guestbook<?php echo $val['id'];?>','id')">删除</A> <A HREF="javascript:modifyguestbook('<?php echo $val['id'];?>')">回复</A></TD>
</TR>
<tr class="tr1" id="guestbook<?php echo $val['id'];?>next"><td colspan="6"><?php echo $val['content'];?></td></tr>
<?php }?>
</TABLE>
</div>

<ul class="page"><?php echo $pageinfo;?></ul>
 <DIV id="guestbook_area" style="display:none;border:2px solid #ccc;width:400px;position:absolute;left:260px;top:60px;background:#fff;z-index:10px;"></DIV>
</body>
</html>
