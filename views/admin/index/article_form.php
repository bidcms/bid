<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/calendar.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>文章管理</title>
</head>
<body>


<div id="man_zone">
   <form action="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=articlemodify" method="post">
<INPUT TYPE="hidden" NAME="updateid" value="<?php echo $article['id'];?>"><INPUT TYPE="hidden" NAME="commit" value="1">
  <table width="99%" border="0" align="center"  cellpadding="3" cellspacing="1" class="table_style">
 
    <tr>
      <td width="18%" class="left_title_1"><span class="left-title">标题:</span></td>
      <td width="82%"><INPUT TYPE="text" class="normal_txt"  NAME="title" value="<?php echo $article['title'];?>" size="50" ></td>
    </tr>
	 <tr>
      <td width="18%" class="left_title_1"><span class="left-title">所属分类:</span></td>
      <td width="82%">
	  <SELECT NAME="cateid">
		<?php if($article['cateid']){?><option value="<?php echo $article['cateid'];?>"><?php echo $GLOBALS['article_cate'][$article['cateid']]['catename'];?></option><?php }?>
		<?php foreach($GLOBALS['article_cate'] as $k=>$v){?>
		<option value="<?php echo $v['id'];?>"><?php echo $v['catename'];?></option>
		<?php }?>
	  </SELECT>自定义分组 <INPUT TYPE="text" NAME="custom_group" value="<?php echo $article['custom_group'];?>">
	  </td>
    </tr>
    
	<tr>
	  <td class="left_title_2">详情:</td>
	  <td>
		<?php echo $this->edit($article['content'],'content','content');?>
	  </td>
	</tr>
	
	<tr>
      <td></td>
      <td><INPUT TYPE="submit" class="normal_button" value="提交"></td>
    </tr>
  </table>
  </FORM>
</div>
</body>
</html>
