<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>文章管理</title>
</head>
<STYLE TYPE="text/css">
	
</STYLE>
<body>

<div class="list">
<TABLE cellpadding="1" cellspacing="1">
<TR>
<TH>标题</TH>
	<TH>所属分类</TH>
	<TH>添加日期</TH>
	<TH>操作</TH>
</TR>
<?php foreach($articlelist as $key=>$val){?>
<TR class="tr<?php echo $key%2;?>" id="article<?php echo $val['id'];?>">
	
	<TD>
	<div id="title-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('titleinput-<?php echo $val['id'];?>');"><?php echo $val['title'];?>
	</div>
	<textarea class="hideinput" id="titleinput-<?php echo $val['id'];?>" ondblclick="confirmValue('article',this.value,'titleinput-<?php echo $val['id'];?>','id');"></textarea>
	</TD>
	
	<TD width="60px" align="center"><div id="cateid-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('cateidinput-<?php echo $val['id'];?>');"><?php echo $val['cateid']?$GLOBALS['article_cate'][$val['cateid']]['catename']:'未分类';?>
	</div>
	
	<select class="hideinput" id="cateidinput-<?php echo $val['id'];?>" onchange="confirmValue('article',this.value,'cateidinput-<?php echo $val['id'];?>','id');">
	<option value="0">未分类</option>
	<?php foreach($GLOBALS['article_cate'] as $k=>$v){?>
<option value="<?php echo $v['id'];?>"><?php echo $v['catename'];?></option>
<?php }?>
</select>
	
	</TD>
	<TD width="140px" align="center"><?php echo date('Y-m-d H:i:s',$val['updatetime']);?></TD>
	<TD align="center" width="100px"><A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=articlemodify&updateid=<?php echo $val['id'];?>">修改</A> <A HREF="javascript:deleteVal('article','<?php echo $val['id'];?>','article<?php echo $val['id'];?>','id')">删除</A></TD>
</TR>
<?php }?>
</TABLE>
</div>

<ul class="page"><?php echo $pageinfo;?></ul>


</body>
</html>
