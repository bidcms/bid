<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>定单管理</title>
</head>
<STYLE TYPE="text/css">
	
</STYLE>
<body>
<form act="" method="get">
<input type="hidden" value="order" name="act"/>
<input type="hidden" value="admin" name="con"/>
订单编号：<input type="text" name="orderno"  style="width:100px" value="<?php echo $this->get('orderno');?>"/>
用户id：<input type="text" name="uid" style="width:20px" value="<?php echo $this->get('uid');?>"/>
收件人：<input type="text" name="username" style="width:100px" value="<?php echo $this->get('username');?>"/>
收件人手机：<input type="text" name="mobile" style="width:100px" value="<?php echo $this->get('mobile');?>"/>
<input type="submit" value="搜索"/>
</form>
<form action="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=multigoods" method="post">
<input type="hidden" name="commit" value="1"/>
<ul class="submenu" id="submenu">

<li class="<?php echo !isset($_GET['status'])?'focus':'normal';?>">
<A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=order">全部</A>
</li>
<li class="<?php echo isset($_GET['status']) && $_GET['status']<1?'focus':'normal';?>">
<A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=order&status=0">未付款</A>
</li>
<li class="<?php echo $_GET['status']==1?'focus':'normal';?>">
<A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=order&status=1">已付款</A>
</li>
<li class="<?php echo $_GET['status']==2?'focus':'normal';?>">
<A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=order&status=2">已发货</A>
</li>
<li class="<?php echo $_GET['status']==3?'focus':'normal';?>"><A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=order&status=3">已收货</A></li>
<li class="<?php echo $_GET['status']==4?'focus':'normal';?>"><A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=order&status=4">已取消</A></li>
</ul>
<div class="list">
<TABLE cellpadding="1" cellspacing="1">
<TR>
    <TH>订单号</TH>
	<TH>产品名称</TH>
	<TH>金额</TH>
	<TH>下单时间</TH>
	<TH>状态</TH>
	<TH>操作</TH>
</TR>
<?php foreach($orderlist as $key=>$val){?>
<TR class="tr<?php echo $key%2;?>" id="goods<?php echo $val['id'];?>">
	
	<TD><?php echo $val['order_no'];?><br/><span style="color:#3366FF;"><?php echo $val['address'];?></span></TD>
	<TD><?php echo $val['goods_name'];?></TD>
	<TD width="60px" align="center"><?php echo $val['total_fee'];?></TD>

	<TD width="140px" align="center"><?php echo date('Y-m-d H:i:s',$val['updatetime']);?></font></TD>
	<TD width="90px" align="center"><?php echo $GLOBALS['order_status'][$val['order_status']];?></TD>
	<TD align="center" width="100px"><A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=ordermodify&updateid=<?php echo $val['id'];?>">查看</A> <A HREF="javascript:deleteVal('order','<?php echo $val['id'];?>','goods<?php echo $val['id'];?>','id')">删除</A><br/><A HREF="javascript:setSms('<?php echo $val['id'];?>','<?php echo $val['uid'];?>')">发送短信</A></TD>
</TR>
<?php }?>
</TABLE>
</div>
<SCRIPT LANGUAGE="JavaScript">
<!--
	function setSms(oid,uid)
	{
		$.post("?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=ordersms",{ "oid":oid, "uid":uid,'rand':Math.random() },
		   function(data){
				eval("var dataobj="+data);
				if(dataobj.datastatus=='success')
				{
					alert('短信发送成功');
				}
				else if(dataobj.datastatus=='error')
				{
					alert('发送失败,错误码：'+dataobj.msg);
				}
				else if(dataobj.datastatus=='nomobile')
				{
					alert('手机为空');
				}
			} 
		); 
	}
//-->
</SCRIPT>
<ul class="page"><?php echo $pageinfo;?></ul>
</form>

</body>
</html>
