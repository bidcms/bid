<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/calendar.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>团购管理</title>
</head>
<body>

<div id="man_zone">
   <form action="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=usermodify" method="post">
	<INPUT TYPE="hidden" NAME="commit" value="1">
  <INPUT TYPE="hidden" NAME="updateid" value="<?php echo $user['uid'];?>">
  <table width="99%" border="0" align="center"  cellpadding="3" cellspacing="1" class="table_style">
 
    <tr>
      <td width="18%" class="left_title_1"><span class="left-title">帐号：</span></td>
      <td width="82%"><INPUT TYPE="text" class="normal_txt" NAME="username" id="username"  size="70" value="<?php echo $user['username'];?>"><SPAN CLASS="" id="emailerror"></SPAN></td>
    </tr>
	 <tr>
      <td width="18%" class="left_title_1"><span class="left-title">密码：</span></td>
      <td width="82%"><INPUT TYPE="password" class="normal_txt" NAME="password" id="password"  size="50" value=""><SPAN CLASS="" id="passworderror">如果不修改，请留空</SPAN></td>
    </tr>
   
   <tr>
      <td class="left_title_1">用户类型：</td>
      <td><?php ;?>
	  <SELECT NAME="usertype" id="usertype">
		<?php if($user['usertype']){?><option value="<?php echo $user['usertype'];?>"><?php echo $GLOBALS['user_type'][$user['usertype']];?></option><?php }?>
		<?php foreach($GLOBALS['user_type'] as $k=>$v){?>
		<option value="<?php echo $k;?>"><?php echo $v;?></option>
		<?php }?>
      </SELECT></td>
    </tr>
	<tr>
      <td class="left_title_1">邮箱：</td>
      <td><INPUT TYPE="text" class="normal_txt" NAME="email" value="<?php echo $user['email'];?>"></td>
    </tr>
	<tr>
	  <td class="left_title_1">积分：</td>
      <td><INPUT TYPE="text" class="normal_txt" NAME="score" value="<?php echo $user['score'];?>"></td>
	</tr>
	<tr>
      <td class="left_title_1"><?php echo $GLOBALS['setting']['site_money_name'];?>：</td>
      <td><INPUT TYPE="text" class="normal_txt" NAME="money" value="<?php echo $user['money'];?>"> <A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=buylog&uid=<?php echo $user['uid'];?>">查看竞拍记录</A> <A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=moneylog&uid=<?php echo $user['uid'];?>">查看资金记录</A></td>
    </tr>
  <tr>
      <td class="left_title_1">ip：</td>
      <td><INPUT TYPE="text" class="normal_txt" NAME="ip" value="<?php echo $user['ip'];?>"></td>
    </tr>
	<tr>
      <td class="left_title_1">ip地址：</td>
      <td><INPUT TYPE="text" class="normal_txt" NAME="address" value="<?php echo $user['address'];?>"></td>
    </tr>
	<tr>
      <td class="left_title_1">手机：</td>
      <td><INPUT TYPE="text" class="normal_txt" NAME="mobile" value="<?php echo $user['mobile'];?>"></td>
    </tr>
	<tr>
      <td class="left_title_1">邀请人id：</td>
      <td><INPUT TYPE="text" class="normal_txt" NAME="friendid" value="<?php echo $user['friendid'];?>"><?php if($user['friendid']){?>[<A HREF="javascript:showfriend('<?php echo $user['friendid'];?>');">查看</A>]<?php }?></td>
    </tr>
	<?php if($updateid){?>
	<tr>
      <td class="left_title_1">注册时间：</td>
      <td><input type="text" name="updatetime" value="<?php echo date('Y-m-d H:i:s',$user['updatetime']);?>" onfocus="HS_setDate(this);" class="normal_txt"/></td>
    </tr>
	<?php }?>
	<tr>
      <td></td>
      <td><INPUT TYPE="submit" class="normal_button" value="提交"></td>
    </tr>
  </table>
  </FORM>
</div>
<SCRIPT LANGUAGE="JavaScript">
<!--
	function showfriend(uid)
	{
		url="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=userinfo&uid="+uid+"&rand="+Math.random();
		$.get(url, function(data){
			$('#user_area').html(data);
			$('#user_area').show();
		}); 
	}
//-->
</SCRIPT>
<DIV id="user_area" style="display:none;border:2px solid #ccc;width:300px;position:absolute;left:330px;top:60px;height:220px;background:#fff;z-index:10px;"></DIV>
</body>
</html>
