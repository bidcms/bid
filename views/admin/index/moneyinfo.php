<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>团购管理</title>
</head>
<STYLE TYPE="text/css">
	
</STYLE>
<body>

<div class="list">
<TABLE cellpadding="1" cellspacing="1">
<TR>
	<TH>ID</TH>
	<TH>用户名</TH>
	<TH>原因</TH>
	<TH>收入</TH>
	<TH>支出</TH>
	<TH>余额</TH>
	<TH>积分支出</TH>
	<TH>时间</TH>
	<TH>操作</TH>
</TR>

<?php foreach($moneyinfo as $key=>$val){?>
<TR class="tr<?php echo $key%2;?>" id="moneylog<?php echo $val['id'];?>">
	

	<TD width="60px">
	<?php echo $val['uid'];?>
	</TD>
	<TD align="center" width="70px"><?php echo $val['username'];?></TD>
	<TD align="center" width="70px"><?php echo $val['title'];?></TD>
	
	<TD  align="center" width="70px"><?php echo $val['get'];?></TD>
	<td align="center" width="150px"><?php echo $val['put'];?>
	<TD  align="center" width="70px"><?php echo $val['money'];?></TD>
	<td align="center" width="150px"><?php echo $val['scoreput'];?>

	<TD  align="center" width="40px"><?php echo date('Y-m-d H:i:s',$val['updatetime']);?></TD>
	</td>
	<TD align="center" width="80px"><A HREF="javascript:deleteVal('moneylog','<?php echo $val['id'];?>','moneylog<?php echo $val['id'];?>','id')">删除</A></TD>
</TR>
<?php }?>
</TABLE>
</div>
<ul class="page"><?php echo $pageinfo;?></ul>

</body>
</html>
