<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>完成项目管理</title>
</head>
<STYLE TYPE="text/css">
	
</STYLE>
<body>
<form act="" method="get">
<input type="hidden" value="complete" name="act"/>
<input type="hidden" value="admin" name="con"/>
关键字：<input type="text" name="keyword" value=""/>
成功用户：<input type="text" name="username" value=""/>
<input type="submit" value="搜索"/>
</form>
<form action="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=multigoods" method="post">
<input type="hidden" name="commit" value="1"/>
<div class="list">
<TABLE cellpadding="1" cellspacing="1">
<TR>
    <TH>项目标题</TH>
	<TH>现价</TH>
	<TH>成功用户</TH>
	<TH>用户id</TH>
	<TH>成功时间</TH>
	<TH>状态</TH>
	<TH>操作</TH>
</TR>
<?php foreach($goodslist as $key=>$val){?>
<TR class="tr<?php echo $key%2;?>" id="goods<?php echo $val['goods_id'];?>">
	
	<TD>
	<div id="goods_name-<?php echo $val['goods_id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('goods_nameinput-<?php echo $val['goods_id'];?>');"><?php echo $val['goods_name'];?>
	</div>
	<textarea class="hideinput" id="goods_nameinput-<?php echo $val['goods_id'];?>" ondblclick="confirmValue('complete',this.value,'goods_nameinput-<?php echo $val['goods_id'];?>','goods_id');"></textarea>
	</TD>
	
	<TD width="60px" align="center"><div id="nowprice-<?php echo $val['goods_id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('nowpriceinput-<?php echo $val['goods_id'];?>');">
<?php echo $val['nowprice'];?>
</div>
<input class="hideinput" type="text" id="nowpriceinput-<?php echo $val['goods_id'];?>" ondblclick="confirmValue('complete',this.value,'nowpriceinput-<?php echo $val['goods_id'];?>','goods_id');"/>
</TD>
	<TD width="60px" align="center"><div id="currentuser-<?php echo $val['goods_id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('currentuserinput-<?php echo $val['goods_id'];?>');">
<?php echo $val['currentuser'];?>
</div>
<input class="hideinput" type="text" id="currentuserinput-<?php echo $val['goods_id'];?>" ondblclick="confirmValue('complete',this.value,'currentuserinput-<?php echo $val['goods_id'];?>','goods_id');"/></TD>
	<TD width="60px" align="center"><div id="currentuid-<?php echo $val['goods_id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('currentuidinput-<?php echo $val['goods_id'];?>');">
<?php echo $val['currentuid'];?>
</div>
<input class="hideinput" type="text" id="currentuidinput-<?php echo $val['goods_id'];?>" ondblclick="confirmValue('complete',this.value,'currentuidinput-<?php echo $val['goods_id'];?>','goods_id');"/></TD>

	<TD width="140px" align="center"><?php echo date('Y-m-d H:i:s',$val['lasttime']);?></font></TD>
	<TD width="90px" align="center"><div id="goods_status-<?php echo $val['goods_id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('goods_statusinput-<?php echo $val['goods_id'];?>');"><?php echo $GLOBALS['goods_status'][$val['goods_status']];?>
	</div>
	<select class="hideinput" id="goods_statusinput-<?php echo $val['goods_id'];?>" onchange="confirmValue('complete',this.value,'goods_statusinput-<?php echo $val['goods_id'];?>','goods_id');">
	<?php foreach($GLOBALS['goods_status'] as $k=>$v){?>
<option value="<?php echo $k;?>"><?php echo $v;?></option>
<?php }?>
</select>
	
	</TD>
	<TD align="center" width="130px"><A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=completemodify&updateid=<?php echo $val['goods_id'];?>">修改</A> <A HREF="javascript:deleteVal('complete','<?php echo $val['goods_id'];?>','goods<?php echo $val['goods_id'];?>','goods_id')">删除</A><br/> <A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=buylog&gid=<?php echo $val['goods_id'];?>">竞拍明细</A> <A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=creategoods&gid=<?php echo $val['goods_id'];?>">复制商品</A></TD>
</TR>
<?php }?>
</TABLE>
</div>

<ul class="page"><?php echo $pageinfo;?></ul>
</form>

</body>
</html>
