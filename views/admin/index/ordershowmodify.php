<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/calendar.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>晒单管理</title>
</head>
<body>


<div id="man_zone">
   <form action="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=ordershowmodify" method="post">
<INPUT TYPE="hidden" NAME="updateid" value="<?php echo $show['id'];?>"><INPUT TYPE="hidden" NAME="commit" value="1">
  <table width="99%" border="0" align="center"  cellpadding="3" cellspacing="1" class="table_style">
 
    <tr>
      <td width="18%" class="left_title_1"><span class="left-title">标题:</span></td>
      <td width="82%"><INPUT TYPE="text" class="normal_txt"  NAME="title" value="<?php echo $show['title'];?>" size="50" ></td>
    </tr>
	<tr>
      <td width="18%" class="left_title_1"><span class="left-title">奖励<?php echo $GLOBALS['setting']['site_money_name'];?>:</span></td>
      <td width="82%">
	  <?php if($show['givemoney']>0){?>
	  已奖励<?php echo $GLOBALS['setting']['site_money_name'];?> <?php echo $show['givemoney'];?>
	  <?php } else{?>
	  <INPUT TYPE="text" class="normal_txt"  NAME="givemoney" value="<?php echo $show['givemoney'];?>" size="20" ><INPUT TYPE="checkbox" NAME="confirm" value="1">确认奖励请选择此框，否则为不奖励</td>
	  <?php }?>
    </tr>
	 <tr>
      <td width="18%" class="left_title_1"><span class="left-title">订单信息:</span></td>
      <td width="82%">
	  商品名：<?php echo $show['order_info']['goods_name'];?><br/>
	  订单类型：<?php echo $GLOBALS['order_type'][$show['order_info']['order_type']];?><br/>
	  总价：<?php echo $show['order_info']['total_fee'];?><br/>
	  竞拍成功价：<?php echo $show['order_info']['goods_info']['nowprice'];?><br/>
	  </td>
    </tr>
	<tr>
      <td width="18%" class="left_title_1"><span class="left-title">用户名:</span></td>
      <td width="82%"><INPUT TYPE="text" NAME="username" class="normal_txt" value="<?php echo $show['username'];?>"></td>
    </tr>
	<tr>
      <td width="18%" class="left_title_1"><span class="left-title">用户ID:</span></td>
      <td width="82%"><INPUT TYPE="text" NAME="uid" class="normal_txt" value="<?php echo $show['uid'];?>"></td>
    </tr>
	
	<tr>
	  <td class="left_title_2">详情:</td>
	  <td>
		<?php echo edit($show['content'],'content','content');?>
	  </td>
	</tr>
	
	<tr>
      <td></td>
      <td><INPUT TYPE="submit" class="normal_button" value="提交"></td>
    </tr>
  </table>
  </FORM>
</div>
</body>
</html>
