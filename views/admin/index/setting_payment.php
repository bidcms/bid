<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script language="javascript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/calendar.js"></script>
<title>支付方式管理</title>
</head>
<body>

<div id="man_zone">
   <form action="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=settingdata" method="post">
	<INPUT TYPE="hidden" NAME="commit" value="1">
	<INPUT TYPE="hidden" NAME="dotype" value="payment">
  
  <table width="99%" border="0" align="center"  cellpadding="3" cellspacing="1" class="table_style">
	<tr>
      <td colspan="2" style="font-size:14px;">支付宝</td>
    </tr>
	<tr>
      <td class="left_title_1">商户类型：</td>
      <td>
	    <select name="alipay_service">
		<option value="create_direct_pay_by_user">即时到帐</option>
		
		</select>
		</td>
    </tr>
    <tr>
      <td class="left_title_1">商户ID：</td>
      <td> <INPUT TYPE="text" class="normal_txt" NAME="alipay_partner"  value="<?php echo $GLOBALS['setting']['alipay_partner'];?>">商户申请：签约支付宝</td>
    </tr>
	<tr>
      <td class="left_title_1">交易密钥：</td>
      <td> <INPUT TYPE="password" class="normal_txt" NAME="alipay_sign"  value="<?php echo $GLOBALS['setting']['alipay_sign'];?>">与支付宝签约生成的交易密码(一般在商家服务查看)</td>
    </tr>
	<tr>
      <td class="left_title_1">支付宝账户：</td>
      <td> <INPUT TYPE="text" class="normal_txt" NAME="alipay_account"  value="<?php echo $GLOBALS['setting']['alipay_account'];?>">支付宝登录帐户名 </td>
    </tr>
	<tr>
      <td colspan="2" style="font-size:14px;">银行转帐</td>
    </tr>
	<tr>
      <td class="left_title_1">帐号信息：</td>
      <td>
	    <TEXTAREA NAME="bank_bankinfo" ROWS="10" COLS="60"><?php echo $GLOBALS['setting']['bank_bankinfo'];?></TEXTAREA>
	  </td>
    </tr>
    <tr>
      <td colspan="2" style="font-size:14px;">Paypal</td>
    </tr>
	<tr>
      <td class="left_title_1">帐号信息：</td>
      <td>
	    <TEXTAREA NAME="bank_bankinfo" ROWS="10" COLS="60"><?php echo $GLOBALS['setting']['bank_bankinfo'];?></TEXTAREA>
	  </td>
    </tr>
	<tr>
      <td></td>
      <td><INPUT TYPE="submit" class="normal_button" value="提交"></td>
    </tr>
  </table>
  </FORM>
</div>
</body>
</html>
