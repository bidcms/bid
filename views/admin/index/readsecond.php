<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>秒杀管理</title>
</head>
<STYLE TYPE="text/css">
	
</STYLE>
<body>
<form act="" method="get">
<input type="hidden" value="readsecond" name="act"/>
<input type="hidden" value="admin" name="con"/>
关键字：<input type="text" name="keyword" value=""/>
<input type="submit" value="搜索"/>
</form>
<input type="hidden" name="commit" value="1"/>
<ul class="submenu" id="submenu">

<li class="<?php echo !isset($_GET['start'])?'focus':'normal';?>">
<A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=readsecond">全部</A>
</li>
<li class="<?php echo $_GET['start']==1?'focus':'normal';?>">
<A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=readsecond&start=1">正在进行中</A>
</li>
<li class="<?php echo $_GET['start']==2?'focus':'normal';?>"><A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=readsecond&start=2">未开始</A></li>
</ul>
<div class="list">
<TABLE cellpadding="1" cellspacing="1">
<TR>
    <TH>项目标题</TH>
	<TH>一口价</TH>
	<TH>秒杀价</TH>
	<TH>开始时间</TH>
	<TH>数量</TH>
	<TH>幸运号码</TH>
	<TH>操作</TH>
</TR>
<?php foreach($goodslist as $key=>$val){?>
<TR class="tr<?php echo $key%2;?>" id="goods<?php echo $val['goods_id'];?>">
	
	<TD width="200px">
	<div id="goods_name-<?php echo $val['goods_id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('goods_nameinput-<?php echo $val['goods_id'];?>');"><?php echo $val['goods_name'];?>
	</div>
	<textarea class="hideinput" id="goods_nameinput-<?php echo $val['goods_id'];?>" ondblclick="confirmValue('readsecond',this.value,'goods_nameinput-<?php echo $val['goods_id'];?>','goods_id');"></textarea>
	</TD>
	<TD width="70px" align="center">
	<div id="marketprice-<?php echo $val['goods_id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('marketpriceinput-<?php echo $val['goods_id'];?>');"><?php echo $val['marketprice'];?>
	</div>
	<input class="hideinput" id="marketpriceinput-<?php echo $val['goods_id'];?>" ondblclick="confirmValue('readsecond',this.value,'marketpriceinput-<?php echo $val['goods_id'];?>','goods_id');"/>
	</TD>
	<TD width="70px" align="center">
	<div id="nowprice-<?php echo $val['goods_id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('nowpriceinput-<?php echo $val['goods_id'];?>');"><?php echo $val['nowprice'];?>
	</div>
	<input class="hideinput" id="nowpriceinput-<?php echo $val['goods_id'];?>" ondblclick="confirmValue('readsecond',this.value,'nowpriceinput-<?php echo $val['goods_id'];?>','goods_id');"/>
	</TD>
	<TD width="140px" align="center"><font color="<?php echo $val['starttime']>time()?'red':'';?>"><?php echo date('Y-m-d H:i:s',$val['starttime']);?></font></TD>
	<TD width="70px" align="center">
	<div id="number-<?php echo $val['goods_id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('numberinput-<?php echo $val['goods_id'];?>');"><?php echo $val['number'];?>
	</div>
	<input class="hideinput" id="numberinput-<?php echo $val['goods_id'];?>" ondblclick="confirmValue('readsecond',this.value,'numberinput-<?php echo $val['goods_id'];?>','goods_id');"/>
	</TD>
	<TD width="70px" align="center">
	<div id="goodluck-<?php echo $val['goods_id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('goodluckinput-<?php echo $val['goods_id'];?>');"><?php echo $val['goodluck'];?>
	</div>
	<input class="hideinput" id="goodluckinput-<?php echo $val['goods_id'];?>" ondblclick="confirmValue('readsecond',this.value,'goodluckinput-<?php echo $val['goods_id'];?>','goods_id');"/>
	</TD>
	<TD align="center" width="100px"><A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=readsecondmodify&updateid=<?php echo $val['goods_id'];?>">修改</A> <A HREF="javascript:deleteVal('readsecond','<?php echo $val['goods_id'];?>','goods<?php echo $val['goods_id'];?>','goods_id')">删除</A></TD>
</TR>
<?php }?>
</TABLE>
</div>

<ul class="page"><?php echo $pageinfo;?></ul>


</body>
</html>
