<?php
/*
	[Phpup.Net!] (C)2009-2011 Phpup.net.
	This is NOT a freeware, use is subject to license terms

	$Id: admin.class.php 2010-08-24 10:42 $
*/

if(!defined('IN_PHPUP')) {
	exit('Access Denied');
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTH XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTH/xhtml1-transitional.dTH">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/css/common.css" type="text/css" />
<script src="https://libs.cdnjs.net/jquery/3.4.1/jquery.min.js"></script>
<script language="javascript">var adminpath='<?php echo $GLOBALS['setting']['adminpath'];?>';</script>
<SCRIPT LANGUAGE="JavaScript" src="<?php echo STATIC_ROOT;?><?php echo TPL_DIR;?>/js/admin.js"></SCRIPT>
<title>晒单管理</title>
</head>
<STYLE TYPE="text/css">
	
</STYLE>
<body>

<div class="list">
<TABLE cellpadding="1" cellspacing="1">
<TR>
<TH>标题</TH>
	<TH>商品标题</TH>
	<TH>用户</TH>
	<TH>状态</TH>
	<TH>奖励</TH>
	<TH>日期</TH>
	<TH>操作</TH>
</TR>
<?php foreach($showlist as $key=>$val){?>
<TR class="tr<?php echo $key%2;?>" id="show<?php echo $val['id'];?>">
	
	<TD>
	<div id="title-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff8800';" onmouseout="this.style.backgroundColor='';" onclick="modifyValue('titleinput-<?php echo $val['id'];?>');"><?php echo $val['title'];?>
	</div>
	<textarea class="hideinput" id="titleinput-<?php echo $val['id'];?>" ondblclick="confirmValue('show',this.value,'titleinput-<?php echo $val['id'];?>','id');"></textarea>
	</TD>
	
	<TD width="60px" align="center"><?php echo $val['order_info']['goods_name'];?></TD>
	
	<TD width="60px" align="center"><?php echo $val['username'];?>(<?php echo $val['uid'];?>)</TD>
	
	<td width="60px" align="center">
	<div id="ispassed-<?php echo $val['id'];?>" onmouseover="this.style.backgroundColor='#ff0000';" onmouseout="this.style.backgroundColor='';" onclick="updateVal('show','ispassedinput-<?php echo $val['id'];?>','id','',['审核','不审核']);">
	<?php echo $val['ispassed']?'审核':'不审核';?>
	</div>
	<INPUT TYPE="text" class="hideinput" id="ispassedinput-<?php echo $val['id'];?>" value="<?php echo intval(!$val['ispassed']);?>" title='nochange'>
	</td>
	<TD width="60px" align="center"><?php echo $val['givemoney'];?></TD>
	<TD width="140px" align="center"><?php echo date('Y-m-d H:i:s',$val['updatetime']);?></TD>
	<TD align="center" width="100px"><A HREF="?con=<?php echo $GLOBALS['setting']['adminpath'];?>&act=ordershowmodify&updateid=<?php echo $val['id'];?>">修改</A> <A HREF="javascript:deleteVal('show','<?php echo $val['id'];?>','show<?php echo $val['id'];?>','id')">删除</A></TD>
</TR>
<?php }?>
</TABLE>
</div>

<ul class="page"><?php echo $pageinfo;?></ul>


</body>
</html>
